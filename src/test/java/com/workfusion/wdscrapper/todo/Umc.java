package com.workfusion.wdscrapper.todo;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.List;

import com.workfusion.wdscrapper.BaseTest;
import com.workfusion.wdscrapper.api.Entity;
import com.workfusion.wdscrapper.api.Input;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


public class Umc extends BaseTest {

    private static final String BASE_URL = "http://www.umc.org";

    @Override
    public Collection<String> collectLinks(Input input) {

        //final String[] states = {"CA", "NY", "PA"};
        WebDriverWait wait = new WebDriverWait(getDriver(), 10);
        int numberOfIterations = 0;
        for (int i = 0; i < input.getStates().size() && numberOfIterations < 1000; i++) {
            String state = input.getStates().get(i);
            String baseUrl;
            int pagesSize;
            if ("CA".equals(state)) {
                pagesSize = 9;
                baseUrl = getBaseUrl() + "/find-a-church/search/eyJsb2NhdGlvbktleXdvcmRzIjoiYXNkZmFzZmQiLCJrZXl3b3JkcyI6bnVsbCwiY2VudGVyIjp7ImxhdCI6MzcuNzc5NDg0MSwibG5nIjotMTIyLjQyOTU3OTcsInNob3J0bmFtZSI6IlNGLCBDQSJ9LCJwYWdlX2xpbWl0IjoyNSwiZGlzdGFuY2UiOnsiZnJvbSI6MCwidG8iOjEwMH0sInNpemUiOnsiZnJvbSI6IjIwIiwidG8iOiIyMDAwIn0sImxhbmd1YWdlcyI6WyJlbiJdLCJzZWFyY2hjYXRzIjpbXSwiY2F0ZWdvcmllcyI6W10sImludGVyYWN0aW9ucyI6eyJmaWx0ZXJzIjowLCJhZHZhbmNlZCI6MH0sInNhdmVkVGltZXN0YW1wIjoxNDQ3MzYyOTgzLCJwYWdlbnVtIjoxfQ==";
            } else if ("NY".equals(state)) {
                pagesSize = 27;
                baseUrl = getBaseUrl() + "/find-a-church/search/eyJsb2NhdGlvbktleXdvcmRzIjoiTlkiLCJrZXl3b3JkcyI6bnVsbCwiY2VudGVyIjp7ImxhdCI6NDAuNzEyNzgzNywibG5nIjotNzQuMDA1OTQxMywic2hvcnRuYW1lIjoiTlksIE5ZIn0sInBhZ2VfbGltaXQiOjI1LCJkaXN0YW5jZSI6eyJmcm9tIjowLCJ0byI6MTAwfSwic2l6ZSI6eyJmcm9tIjoiMjAiLCJ0byI6IjIwMDAifSwibGFuZ3VhZ2VzIjpbImVuIl0sInNlYXJjaGNhdHMiOltdLCJjYXRlZ29yaWVzIjpbXSwiaW50ZXJhY3Rpb25zIjp7ImZpbHRlcnMiOjAsImFkdmFuY2VkIjowfSwic2F2ZWRUaW1lc3RhbXAiOjE0NDczNjMzNzksInBhZ2VudW0iOjF9";
            } else {
                pagesSize = 52;
                baseUrl = getBaseUrl() + "/find-a-church/search/eyJsb2NhdGlvbktleXdvcmRzIjoiUEEiLCJrZXl3b3JkcyI6bnVsbCwiY2VudGVyIjp7ImxhdCI6NDEuMjAzMzIxNiwibG5nIjotNzcuMTk0NTI0Nywic2hvcnRuYW1lIjoiUEEifSwicGFnZV9saW1pdCI6MjUsImRpc3RhbmNlIjp7ImZyb20iOjAsInRvIjoxMDB9LCJzaXplIjp7ImZyb20iOiIyMCIsInRvIjoiMjAwMCJ9LCJsYW5ndWFnZXMiOlsiZW4iXSwic2VhcmNoY2F0cyI6W10sImNhdGVnb3JpZXMiOltdLCJpbnRlcmFjdGlvbnMiOnsiZmlsdGVycyI6MCwiYWR2YW5jZWQiOjB9LCJzYXZlZFRpbWVzdGFtcCI6MTQ0NzM3MDU4MywicGFnZW51bSI6MX0=";
            }
            for (int pageNumber = 1; pageNumber <= pagesSize && numberOfIterations < 1000; pageNumber++) {
                getDriver().navigate().to(baseUrl + "/P" + pageNumber);
                wait.until(ExpectedConditions.presenceOfElementLocated(By.id("fac-results")));
                long itemsSize = getDriver().findElements(By.xpath("//a[contains(text(),'Details')]")).size();
                for (int index = 0; index < itemsSize && numberOfIterations < 200; index++) {
                    wait.until(ExpectedConditions.presenceOfElementLocated(By.id("fac-results")));
                    final WebElement webElement = getDriver().findElements(By.xpath("//a[contains(text(),'Details')]")).get(index);
                    webElement.click();
                    wait.until(ExpectedConditions.presenceOfElementLocated(By.id("fac-item-detail")));
                    String html = getDriver().getPageSource();
                    String headTag = "<head>";
                    String websiteUrl = "<website_url>" + getDriver().getCurrentUrl() + "</website_url>";
                    String newHtml = new StringBuilder(html).insert(html.indexOf(headTag) + headTag.length(), websiteUrl).toString();
                    final String filePath = state + "_page_" + pageNumber + "_index_" + index + ".html";
                    try {
                        FileUtils.writeStringToFile(new File(filePath), newHtml, true);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    numberOfIterations++;
                    getDriver().navigate().to(baseUrl + "/P" + pageNumber);
                }
            }
        }

        return null;
    }

    @Override
    public List<Entity> extractEntities(String link) {
        return null;
    }

    @Override
    public String getBaseUrl() {
        return BASE_URL;
    }
}