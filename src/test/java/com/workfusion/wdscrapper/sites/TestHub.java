package com.workfusion.wdscrapper.sites;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.net.URL;
import java.util.concurrent.TimeUnit;

public class TestHub {

    private WebDriver driver;

    @Before
    public void setUp() throws Exception {
        DesiredCapabilities capabilities = DesiredCapabilities.chrome();
        ChromeOptions options = new ChromeOptions();
        options.addArguments("load-extension=/opt/extensions/Block-image_v1.0");
        capabilities.setCapability(ChromeOptions.CAPABILITY, options);
        capabilities.setCapability("name", "Testing Selenium 2");
        //capabilities.setCapability("applicationName", "hub1" );
        String url = "http://se-hub.crowdcomputingsystems.com:5555/wd/hub";
//                       String url = "http://192.168.99.100:32768/wd/hub";
        driver = new RemoteWebDriver(new URL(url), capabilities);
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
    }

    @Test
    public void testSimple() throws Exception {
        driver.get("http://tut.by");
//                       driver.findElement(By.id("search")).sendKeys("Asdf");
        System.out.println(driver.getPageSource());
    }

    //          @Test
    public void testReal() throws Exception {
        driver.get("http://locations.scrubsandbeyond.com/#/map");
//                       driver.findElement(By.id("search")).sendKeys("Asdf");
        System.out.println(driver.getPageSource());
    }


    public void testThread() throws Exception {
        DesiredCapabilities capabilities = DesiredCapabilities.chrome();
        capabilities.setJavascriptEnabled(true);
        capabilities.setCapability("name", "Testing Selenium 2");
        WebDriver driver = new RemoteWebDriver(new URL("http://192.168.99.100:32768/wd/hub"), capabilities);
        driver.get("http://www.google.com");
        Thread.sleep(10000);
        Assert.assertEquals("Google", driver.getTitle());
    }

    @After
    public void tearDown() throws Exception {
        this.driver.quit();
    }

}