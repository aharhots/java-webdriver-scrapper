package com.workfusion.wdscrapper;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletionService;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import com.google.common.base.Joiner;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.workfusion.wdscrapper.api.Entity;
import com.workfusion.wdscrapper.api.IEntityExtractor;
import com.workfusion.wdscrapper.api.Input;
import com.workfusion.wdscrapper.location.provider.LocationEntity;
import com.workfusion.wdscrapper.location.provider.LocationProvider;
import com.workfusion.wdscrapper.s3.S3PublicPut;
import com.workfusion.wdscrapper.utils.CommonUtils;
import com.workfusion.wdscrapper.utils.WebDriverHelper;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import static com.workfusion.wdscrapper.Conf.COMA_CHAR;
import static com.workfusion.wdscrapper.Conf.DEFAULT_STATE;
import static com.workfusion.wdscrapper.Conf.MAX_CONCURRENT_TESTS;
import static com.workfusion.wdscrapper.Conf.MAX_TRY;
import static com.workfusion.wdscrapper.Conf.PROP_IG_COUNTRIES;
import static com.workfusion.wdscrapper.Conf.PROP_IG_STATES;
import static com.workfusion.wdscrapper.Conf.SELENUIM_HUB_URL;
import static java.util.Collections.singletonList;
import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.toList;

public abstract class BaseTest implements IEntityExtractor {

    public static final String BLOCK_IMAGE_EXT = "src\\main\\resources\\extensions\\Block-image_v1.0.crx";

    protected static final String CHURCH_SUFFIX_REGEX = "(Associate|Coordinator|Director of Development & Community Relations|Parish Life Director|Director of Religious Education|Committee Member|Executive Director|Teaching Couple|Office Manager\\/Staff Supervisor|Principal|Director of Communications|Librarian|Vocation Director|Director, Press & Communications|Director)";
    protected static final String CHURCH_TITLE_REGEX = "(Dr\\.?|Fr\\.?|Rev\\.?|Teacher\\.?|Very Rev\\.?|Most Reverend\\.?|Most Rev\\.?|Pastor\\.?|Sr\\.?|Mrs\\.?|Msgr\\.?|Father\\.?|Deacon\\.?|Faithful Admiral\\.?|Bishop\\.?|Past Faithful Navigator)";

    protected static final String LINKEDIN_URL = "linkedinURL";
    protected static final String INSTAGRAM_URL = "instagramURL";
    protected static final String YOUTUBE_URL = "youtubeURL";
    protected static final String PINTEREST_URL = "pinterestURL";
    protected static final String GOOGLE_PLUS_URL = "googlePlusURL";
    protected static final String TWITTER_URL = "twitterURL";
    protected static final String FACEBOOK_URL = "facebookURL";
    protected static final String HAS_ECOMMERCE = "hasEcommerce";
    protected static final String CREDIT_CARDS_ACCEPTED = "creditCardsAccepted";
    protected static final String PAYMENT_METHODS = "paymentMethods";

    protected final Log log = LogFactory.getLog(getClass());
    protected static final ExecutorService EXECUTOR_SERVICE = Executors.newFixedThreadPool(MAX_CONCURRENT_TESTS);
    private static final ThreadLocal<WebDriver> threadLocal = new ThreadLocal<>();
    private final Map<String, String> commonDataCache = Maps.newConcurrentMap();

    private WebDriver newLocalDriver() {
        WebDriverHelper.setDriver();
        ChromeOptions options = new ChromeOptions();
        options.addExtensions(new File(BLOCK_IMAGE_EXT));
        //options.addArguments("--start-maximized"); //doesn't work on unix machines
        WebDriver driver = new ChromeDriver(options);
        WebDriverHelper.setupDriver(driver);
        WebDriverHelper.maximize(driver);
        return driver;
    }

    private WebDriver newRemoteDriver() {
        int count = 0;
        while (true) {
            try {
                return createRemoteDriver();
            } catch (WebDriverException e) {
                if (++count == MAX_TRY)
                    throw e;
                log.info("WebDriver creation retry number: " + count);
                CommonUtils.sleep(60);
            }
        }
    }

    private WebDriver createRemoteDriver() {
        log.info("'newRemoteDriver' starting");
        DesiredCapabilities capability = DesiredCapabilities.chrome();
        ChromeOptions options = new ChromeOptions();
        options.addArguments("load-extension=/opt/extensions/Block-image_v1.0");
        capability.setCapability(ChromeOptions.CAPABILITY, options);
        WebDriver driver = new RemoteWebDriver(SELENUIM_HUB_URL, capability);
        WebDriverHelper.setupDriver(driver);
        //WebDriverHelper.maximize(driver);
        log.info("'newRemoteDriver' has been initialized");
        return driver;
    }

    protected WebDriver getDriver() {
        WebDriver driver = threadLocal.get();
        if (driver == null) {
            driver = SELENUIM_HUB_URL != null ? newRemoteDriver() : newLocalDriver();
            threadLocal.set(driver);
        }
        return driver;
    }

    @Test
    public void execute() {
        final List<Input> inputsByStates = generateTaskInputs();
        final List<Entity> entities = extractEntities(inputsByStates);
        log.info("Generate csv and save it to s3 for entity.length: " + entities.size());
        saveToCsvOnS3(generateCsvContent(entities));
    }

    private List<Entity> extractEntities(List<Input> inputsByStates) {
        try {
            cacheCommonData();
        } finally {
            tearDown();
        }
        return startEntitiesExtractionsByLinks(inputsByStates);
    }

    protected void cacheCommonData() {
    }

    protected List<Entity> startEntitiesExtractionsByLinks(List<Input> inputsByStates) {
        final CompletionService<List<Entity>> completionService = new ExecutorCompletionService<>(EXECUTOR_SERVICE);
        final List<Future<List<Entity>>> tasks = inputsByStates.stream()
                .map(input -> completionService.submit(() -> startTaskAndTearDown(input)))
                .collect(toList());
        return getEntities(completionService, tasks);
    }

    protected List<Entity> getEntities(CompletionService<List<Entity>> completionService, List<Future<List<Entity>>> tasks) {
        final List<Entity> result = Lists.newArrayList();
        for (int i = 0; i < tasks.size(); i++) {
            try {
                List<Entity> entities = completionService.take().get();
                if (CollectionUtils.isNotEmpty(entities)) {
                    result.addAll(entities);
                }
            } catch (InterruptedException | ExecutionException e) {
                log.error(e.getMessage(), e);
            } finally {
                tearDown();
            }
        }
        return result;
    }

    private List<Entity> startTaskAndTearDown(Input input) {
        try {
            log.info("Starting extraction for " + input);
            return startTask(input);
        } finally {
            tearDown();
        }
    }

    protected List<Entity> startTask(Input input) {
        final List<Entity> result = new ArrayList<>();
        final Collection<String> links = retry(input, this::collectLinks);
        if (CollectionUtils.isNotEmpty(links)) {
            for (String link : links) {
                log.info("Starting extraction for link " + link);
                final Collection<Entity> extractedEntities = retry(link, this::extractEntities);
                result.addAll(extractedEntities);
            }
        }
        return result;
    }

    protected String generateCsvContent(List<Entity> result) {
        final StringBuilder content = new StringBuilder();
        final String[] entityFieldNames = getEntityFieldNames();
        try (final CSVPrinter printer = new CSVPrinter(content, CSVFormat.RFC4180.withHeader(underscoreFieldName(entityFieldNames)))) {
            for (Entity entity : result) {
                printer.printRecord(getContent(entity, entityFieldNames));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return content.toString();
    }

    public List getContent(Entity entity, String[] fieldNames) {
        final List<Object> entityValue = new ArrayList<>();
        try {
            for (String fieldName : fieldNames) {
                final Field declaredField = entity.getClass().getDeclaredField(fieldName);
                declaredField.setAccessible(true);
                entityValue.add(declaredField.get(entity));
            }
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }

        return entityValue;
    }

    private String[] getEntityFieldNames() {
        final Field[] entityFields = Entity.class.getDeclaredFields();
        final String[] headers = new String[entityFields.length];
        for (int i = 0; i < entityFields.length; i++) {
            headers[i] = entityFields[i].getName();
        }
        return headers;
    }

    private String[] underscoreFieldName(String[] fieldNames) {
        final String[] result = new String[fieldNames.length];
        for (int i = 0; i < fieldNames.length; i++) {
            result[i] = underscoreFieldName(fieldNames[i]);
        }
        return result;
    }

    private String underscoreFieldName(String header) {
        return header.replaceAll("([A-Z]+)", "_$1").toLowerCase();
    }

    protected void tearDown() {
        WebDriver webDriver = threadLocal.get();
        if (webDriver != null) {
            try {
                getDriver().quit();
            } catch (Exception ignored) {
            }
            threadLocal.set(null);
        }
    }

    protected String getElementAttrByXPath(String xPath, String attributeName) {
        return getElementAttr(By.xpath(xPath), attributeName);
    }

    protected String getElementAttr(By by, String attributeName) {
        final WebElement element = getWebElement(by);
        return element == null ? null : element.getAttribute(attributeName);
    }

    protected String getElementText(By by) {
        return getElementText(by, 0);
    }

    /**
     * Returns element text.
     *
     * @param by      Locator.
     * @param timeout Timeout to find element.
     * @return Element text.
     */
    protected String getElementText(By by, int timeout) {
        try {
            final List<String> texts = WebDriverHelper.getElementTexts(getDriver(), by, timeout);
            return texts.size() > 0 ? texts.get(0) : StringUtils.EMPTY;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return StringUtils.EMPTY;
        }
    }

    /**
     * Returns element texts.
     *
     * @param by      Locator.
     * @param timeout Timeout to find element.
     * @return Element text.
     */
    protected List<String> getElementTexts(By by, int timeout) {
        return WebDriverHelper.getElementTexts(getDriver(), by, timeout);
    }

    protected String getElementTextByXpath(String xpathExpression) {
        try {
            return getText(getElementByXpath(xpathExpression)).trim();
        } catch (Exception e) {
            log.error("Url with error:" + getDriver().getCurrentUrl() + "\n" + e.getMessage(), e);
            return StringUtils.EMPTY;
        }
    }

    protected List<WebElement> getElementsByXpath(String xpathExpression) {
        try {
            return (List<WebElement>) CommonUtils.jse(getDriver()).executeScript("var nodes = []; var nodesSnapshot = document.evaluate(arguments[0], document, null, XPathResult.ORDERED_NODE_SNAPSHOT_TYPE, null);for ( var i=0 ; i < nodesSnapshot.snapshotLength; i++ ){nodes.push(nodesSnapshot.snapshotItem(i));}return nodes;", xpathExpression);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return Lists.newArrayList();
        }
    }

    protected WebElement getElementByXpath(String xpathExpression) {
        return ((WebElement) CommonUtils.jse(getDriver()).executeScript("return document.evaluate(arguments[0], document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue;", xpathExpression));
    }

    protected void executeJs(String js) {
        CommonUtils.jse(getDriver()).executeScript(js);
    }

    private String getText(WebElement element) {
        try {
            return ((String) CommonUtils.jse(getDriver()).executeScript("return arguments[0].innerText", element)).trim();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return StringUtils.EMPTY;
        }
    }

    protected String getElementsTextByXpath(String xpathExpression, String separator) {
        final List<String> result = Lists.newArrayList();
        final List<WebElement> elementsByXpath = getElementsByXpath(xpathExpression);
        result.addAll(elementsByXpath.stream().map(webElement -> getText(webElement).trim()).collect(Collectors.toList()));
        return Joiner.on(separator).join(result);
    }

    private WebElement getWebElement(String xPath) {
        return getWebElement(By.xpath(xPath));
    }

    protected WebElement getWebElement(By by) {
        WebElement element = null;
        try {
            element = getDriver().findElement(by);
        } catch (Exception ignore) {
        }
        return element;
    }

    private List<String> getListFromSystemProperty(String propertyName) {
        final String countriesProperty = System.getProperties().getProperty(propertyName);
        //final String countriesProperty = "US";
        return StringUtils.isNotBlank(countriesProperty) ? Lists.newArrayList(countriesProperty.split(COMA_CHAR)) : Lists.newArrayList();
    }

    protected List<Input> generateTaskInputs() {
        final Collection<LocationEntity> locations = getLocations();
        final Map<String, List<LocationEntity>> byState = locations.stream()
                .collect(
                        groupingBy(
                                LocationEntity::getState
                        ));
        List<Input> inputs = byState.values().stream()
                .map(list -> new Input(
                                singletonList(list.get(0).getCountry()),
                                singletonList(list.get(0).getState()),
                                list.stream()
                                        .map(LocationEntity::getZipCode)
                                        .collect(toList())
                        )
                ).collect(toList());
        return inputs;
    }

    protected Collection<LocationEntity> getLocations() {
        final LocationProvider locationProvider = LocationProvider.getLocationProvider();
        final List<String> countries = getListFromSystemProperty(PROP_IG_COUNTRIES);
        if (countries.isEmpty()) {
            final List<String> states = getListFromSystemProperty(PROP_IG_STATES);
            if (states.isEmpty()) {
                states.add(DEFAULT_STATE);
            }
            return locationProvider.getLocations(locationEntity -> states.contains(locationEntity.getState()));
        }
        Collection<LocationEntity> locations = locationProvider.getLocations(locationEntity -> countries.contains(locationEntity.getCountry()));
        return locations;
    }

    protected void saveToCsvOnS3(String content) {
        final String fileName = getWebsiteUrl() + "-" + System.currentTimeMillis() + ".csv";
        final String path = "Selenium/Scrub/selenium_run_results/" + getWebsiteUrl() + "/" + fileName;
        log.info("s3 path: " + path);
        S3PublicPut.getInstance().putDataToAmazon(path, content);
    }

    protected String getWebsiteUrl() {
        String url = getBaseUrl();
        try {
            URI uri = new URI(getBaseUrl());
            String domain = uri.getHost();
            url = domain.startsWith("www.") ? domain.replace("www.", "") : domain;
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        return url;
    }

    /**
     * Sets text to text field type of "Input".
     *
     * @param locator Element locator.
     * @param text    Text to set.
     * @param timeout Timeout to wait text field visible.
     */
    protected void setText(By locator, String text, int timeout) {
        WebDriverHelper.setText(getDriver(), locator, text, timeout);
    }

    /**
     * Verifies if an element is present in HTML (DOM).
     *
     * @param locator Element locator.
     * @param timeout Timeout.
     * @return true if element present, otherwise false.
     */
    protected boolean isPresent(By locator, int timeout) {
        try {
            return WebDriverHelper.isPresent(getDriver(), locator, timeout);
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Verifies if an element invisible.
     *
     * @param locator Element locator.
     * @param timeout Timeout for wait element invisible.
     * @return true if element invisible, otherwise false.
     */
    protected boolean isInvisible(By locator, int timeout) {
        return WebDriverHelper.isInvisible(getDriver(), locator, timeout);
    }

    /**
     * Verifies if an element visible.
     *
     * @param locator Element locator.
     * @param timeout Timeout for wait element visible.
     * @return true if element invisible, otherwise false.
     */
    protected boolean isVisible(By locator, int timeout) {
        return WebDriverHelper.isVisible(getDriver(), locator, timeout);
    }

    /**
     * Clicks on element.
     *
     * @param locator locator.
     * @param timeout Timeout for wait element visible.
     */
    protected void click(By locator, int timeout) {
        WebDriverHelper.click(getDriver(), locator, timeout);
    }

    /**
     * Returns list of URLs from all found elements.
     *
     * @param locator locator.
     * @param timeout Timeout to find elements.
     * @return List of URLs.
     */
    protected List<String> getUrls(By locator, int timeout) {
        return WebDriverHelper.getUrls(getDriver(), locator, timeout);
    }

    /**
     * Returns first URL from all found elements.
     *
     * @param locator locator.
     * @return List of URLs.
     */
    protected String getUrl(By locator) {
        return getUrl(locator, 0);
    }

    /**
     * Returns first URL from all founded elements.
     *
     * @param locator locator.
     * @param timeout Timeout to find elements.
     * @return List of URLs.
     */
    protected String getUrl(By locator, int timeout) {
        try {
            List<String> urls = WebDriverHelper.getUrls(getDriver(), locator, timeout);
            return CollectionUtils.isNotEmpty(urls) ? urls.get(0) : StringUtils.EMPTY;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return StringUtils.EMPTY;
        }
    }

    /**
     * Extracts text from text by regular expression.
     *
     * @param text  Text to search.
     * @param regex Regular expression.
     * @return Returns part of text from input text.
     */
    protected String getTextByRegex(String text, String regex) {
        return CommonUtils.getTextByRegex(text, regex);
    }

    protected String getFullAddress(String address, String state, String city, String zip) {
        return StringUtils.join(Arrays.asList(address, state, city, zip), " ");
    }

    protected Map<String, String> getCommonDataCache() {
        return commonDataCache;
    }

    protected String convertBoolean(boolean flag) {
        return flag ? "Y" : "N";
    }

    /**
     * Returns list of elements.
     *
     * @param locator locator.
     * @param timeout Timeout to find elements.
     * @return List of Web Elements.
     */
    protected List<WebElement> getElements(By locator, int timeout) {
        return WebDriverHelper.getElements(getDriver(), locator, timeout);
    }

    protected String getAlertMessage() {
        return WebDriverHelper.getAlertMessage(getDriver());
    }

    protected void setCommonData(Entity entity) {
        entity.setCreditCardsAccepted(commonDataCache.get(CREDIT_CARDS_ACCEPTED));
        entity.setHasEcommerce(commonDataCache.get(HAS_ECOMMERCE));
        entity.setPaymentMethods(commonDataCache.get(PAYMENT_METHODS));

        entity.setLinkedinURL(commonDataCache.get(LINKEDIN_URL));
        entity.setInstagramURL(commonDataCache.get(INSTAGRAM_URL));
        entity.setYoutubeURL(commonDataCache.get(YOUTUBE_URL));
        entity.setGoogleplusURL(commonDataCache.get(GOOGLE_PLUS_URL));
        entity.setFacebookURL(commonDataCache.get(FACEBOOK_URL));
        entity.setTwitterURL(commonDataCache.get(TWITTER_URL));
        entity.setPinterestURL(commonDataCache.get(PINTEREST_URL));
    }

    protected <T, E> Collection<E> retry(T arg, Function<T, Collection<E>> func) {
        int count = 0;
        while (true) {
            try {
                return func.apply(arg);
            } catch (Exception e) {
                tearDown();
                openStoreLocatorPage();
                if (++count == MAX_TRY) {
                    log.error("Error happened for arg: " + arg);
                    log.error(e.getMessage(), e);
                    break;
                } else {
                    log.info("retry number: " + count);
                }
            }
        }
        return new ArrayList<>();
    }

    protected void openStoreLocatorPage() {
    }

    protected String removeToken(String input, String regex, String token) {
        if (token != null && !token.isEmpty()) {
            input = input.replaceAll(regex, "").trim();
        }
        return input;
    }

    protected String getToken(String regex, String line) {
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(line);
        if (matcher.find()) {
            return matcher.group();
        }
        return null;
    }

    protected void waitForAjaxDone() {
        while (true) {
            Boolean ajaxIsComplete = (Boolean) CommonUtils.jse(getDriver()).executeScript("return jQuery.active == 0");
            if (ajaxIsComplete)
                break;
            CommonUtils.sleepMillis(100);
        }
    }
}
