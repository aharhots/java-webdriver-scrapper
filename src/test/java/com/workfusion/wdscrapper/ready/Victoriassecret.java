package com.workfusion.wdscrapper.ready;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.workfusion.wdscrapper.BaseTest;
import com.workfusion.wdscrapper.api.Entity;
import com.workfusion.wdscrapper.api.Input;
import org.openqa.selenium.By;


public class Victoriassecret extends BaseTest {
    final String BASE_URL = "https://www.victoriassecret.com";

    @Override
    public Collection<String> collectLinks(Input input) {
        return null;
    }

    @Override
    public List<Entity> extractEntities(String link) {
        return null;
    }

    @Override
    public String getBaseUrl() {
        return BASE_URL;
    }

    @Override
    public List<Entity> startTask(Input input) {
        try {
            log.info("Starting extraction for " + input);
            List<String> inputZips = input.getZipCodes();
            List<Entity> entities = new ArrayList<>();
            for (int i = 0; i < inputZips.size(); i++) {
                String entityLink = BASE_URL + "/store-locator#/stores/" + inputZips.get(i);
                getDriver().get(entityLink);
                entities.addAll(getPageEntities(entityLink));
                log.info("Entities found: " + entities.size() + "\tZip: " + (i + 1) + "/" + inputZips.size());
            }
            return entities;
        } finally {
            tearDown();
        }
    }

    private List<Entity> getPageEntities(String entityLink) {
        String listItemContainerXpath = "//*[@class='storeList']";
        int numberOfStores = getElements(By.xpath(listItemContainerXpath), 0).size();
        List<Entity> entities = new ArrayList<>();
        for (int i = 1; i <= numberOfStores; i++) {
            Entity entity = new Entity(getWebsiteUrl(), entityLink);
            entity.setBusinessName(getElementText(By.xpath(listItemContainerXpath + "[" + i + "]//*[@class='mallName']"), 0));
            entity.setAddress(getAddress(By.xpath(listItemContainerXpath + "[" + i + "]//address")));
            entity.setPhone(getPhone(By.xpath(listItemContainerXpath + "[" + i + "]//address")));
            entity.setOperationHours(getHours(By.xpath(listItemContainerXpath + "[" + i + "]//*[@class='hours-block']//*")));
            entity.setFacebookURL("https://www.facebook.com/victoriassecret");
            entity.setTwitterURL("https://twitter.com/victoriassecret");
            entity.setYoutubeURL("https://www.youtube.com/user/VICTORIASSECRET");
            entity.setPinterestURL("https://www.pinterest.com/victoriassecret");
            entity.setInstagramURL("https://www.instagram.com/victoriassecret/");
            entities.add(entity);
        }
        return entities;
    }

    private String getAddress(By addressContainerBy) {
        String addressWithPhone = getElementText(addressContainerBy, 0);
        if (addressWithPhone != null && addressWithPhone.split("\n").length > 1)
            return addressWithPhone.split("\n")[0] + ", " + addressWithPhone.split("\n")[1];
        return "";
    }

    private String getPhone(By addressContainerBy) {
        String addressWithPhone = getElementText(addressContainerBy, 0);
        if (addressWithPhone != null && addressWithPhone.split("\n").length > 2)
            return addressWithPhone.split("\n")[2];
        return "";
    }

    private String getHours(By hoursContainerBy) {
        List<String> hoursList = getElementTexts(hoursContainerBy, 0);
        String hours = "";
        for (String hourDay : hoursList)
            hours += hourDay + "\n";
        return hours;
    }
}
