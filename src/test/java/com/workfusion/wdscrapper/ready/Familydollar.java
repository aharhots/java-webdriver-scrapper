package com.workfusion.wdscrapper.ready;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.workfusion.wdscrapper.BaseTest;
import com.workfusion.wdscrapper.Conf;
import com.workfusion.wdscrapper.api.Entity;
import com.workfusion.wdscrapper.api.Input;
import com.workfusion.wdscrapper.utils.CommonUtils;
import com.workfusion.wdscrapper.utils.WebDriverHelper;
import org.apache.commons.collections.CollectionUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.UnhandledAlertException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Familydollar extends BaseTest {

    @Override
    public Collection<String> collectLinks(Input input) {
        return null;
    }

    @Override
    public List<Entity> extractEntities(String link) {
        return null;
    }

    @Override
    protected List<Entity> startTask(Input input) {
        return extractEntities(input);
    }

    private List<Entity> extractEntities(Input input) {
        List<Entity> entities = new ArrayList<>();
        Set<String> addresses = new HashSet<>();

        getDriver().get("http://www.familydollar.com/content/familydollar/en/store-results.html");
        List<WebElement> radios = getDriver().findElements(By.name("distRadios"));
        if (CollectionUtils.isNotEmpty(radios)) {
            radios.get(radios.size() - 1).click();
        }
        for (String zip : input.getZipCodes()) {
            WebElement inputPostalCode = getDriver().findElement(By.xpath("//*[@id='storeLocatorForm']/input[@type='text']"));
            inputPostalCode.clear();
            inputPostalCode.sendKeys(zip);
            click(By.xpath("//*[@id='storeLocatorForm']/input[@type='button']"), Conf.TIMEOUT);

            entities.addAll(extractEntitiesForZip(addresses));
        }

        return entities;
    }

    private List<Entity> extractEntitiesForZip(Set<String> addresses) {
        List<Entity> entities = new ArrayList<>();

        String alertText = getAlertMessage();
        if (alertText.contains("Please enter a valid address or zip code")) {
            return entities;
        }

        CommonUtils.sleep(1);
        try {
            WebDriverWait wait = new WebDriverWait(getDriver(), 120);
            wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//div[@id='storelocatorloader']")));
        } catch (UnhandledAlertException e) {
            log.info("UnhandledAlertException error");
            WebDriverHelper.closeAlertIfPresent(getDriver());
        }

        String messageBar = getElementText(By.id("retryMessageBar"));
        if (!messageBar.startsWith("We couldnt find any stores") && !messageBar.startsWith("The address you entered was not found")) {
            int size = getDriver().findElements(By.xpath("//*[@id='storelisting']/div/div[2]/div[@class = 'name']/strong")).size();
            for (int i = 1; i <= size; i++) {
                String address = getElementTextByXpath("(//*[@id='storelisting']/div/div[2]/div[@class = 'address'])[" + i + "]");
                if (!addresses.contains(address)) {
                    addresses.add(address);
                    entities.add(extractEntity(i, address));
                }
            }
        }
        return entities;
    }

    private Entity extractEntity(int i, String address) {
        Entity entity = new Entity(getWebsiteUrl(), getBaseUrl());
        entity.setBusinessName("FAMILY DOLLAR STORE");
        entity.setAddress(address);
        entity.setPhone(getElementTextByXpath("(//*[@id='storelisting']/div/div[2]/span[@class = 'phone'])[" + i + "]"));
        entity.setOperationHours(getElementTextByXpath("(//*[@id='storelisting']/div/div[2]/div[@class = 'moreInfo']/p[@class = 'hours'])[" + i + "]"));
        entity.setCompanyWebsite(getBaseUrl());
        entity.setFacebookURL(getCommonDataCache().get(FACEBOOK_URL));
        entity.setTwitterURL(getCommonDataCache().get(TWITTER_URL));
        entity.setGoogleplusURL(getCommonDataCache().get(GOOGLE_PLUS_URL));
        entity.setPinterestURL(getCommonDataCache().get(PINTEREST_URL));
        return entity;
    }

    @Override
    protected void cacheCommonData() {
        getDriver().get(getBaseUrl());
        getCommonDataCache().put(FACEBOOK_URL, getUrl(By.xpath("//a[contains(@href, 'www.facebook.com')]"), 0));
        getCommonDataCache().put(TWITTER_URL, getUrl(By.xpath("//a[contains(@href, 'twitter.com')]"), 0));
        getCommonDataCache().put(GOOGLE_PLUS_URL, getUrl(By.xpath("//a[contains(@href, 'youtube.com')]"), 0));
        getCommonDataCache().put(PINTEREST_URL, getUrl(By.xpath("//a[contains(@href, 'pinterest.com')]"), 0));
    }

    @Override
    public String getBaseUrl() {
        return "http://www.familydollar.com";
    }
}
