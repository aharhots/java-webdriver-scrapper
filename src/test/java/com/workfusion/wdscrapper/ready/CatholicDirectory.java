package com.workfusion.wdscrapper.ready;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.workfusion.wdscrapper.BaseTest;
import com.workfusion.wdscrapper.Conf;
import com.workfusion.wdscrapper.api.Entity;
import com.workfusion.wdscrapper.api.Input;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import static java.util.Collections.singletonList;


public class CatholicDirectory extends BaseTest {

    private static final String BASE_URL = "http://www.thecatholicdirectory.com";

    @Override
    public Collection<String> collectLinks(Input input) {
        List<String> links = new ArrayList<>();
        final String baseUrl = getBaseUrl();
        final String startUrl = baseUrl + "/directory.cfm?fuseaction=search_directory&country=US&state=%s&pagename=dsp_adv_search_form&viewall=true&sort=alpha";
        //final String[] states = {"NY", "CA"};
        for (final String state : input.getStates()) {
            getDriver().navigate().to(String.format(startUrl, state));
            final List<WebElement> elements = getDriver().findElements(By.xpath("//span[@class='search_titledark']/a"));
            for (WebElement element : elements) {
                links.add(element.getAttribute("href"));
            }
            getDriver().navigate().to(String.format(startUrl, state));
        }
        return links;
    }

    @Override
    public List<Entity> extractEntities(String link) {
        WebDriver driver = getDriver();
        driver.navigate().to(link);
        final Entity entity = new Entity(getWebsiteUrl(), driver.getCurrentUrl());
        entity.setAddress(getElementText(By.xpath("//*[@itemprop='address']"), Conf.TIMEOUT));
        entity.setBusinessName(getElementText(By.xpath("//*[@class='PageTitleListing']"), Conf.TIMEOUT));
        entity.setPrimaryContactName(getElementText(By.xpath("//*[@itemprop='contactPoints']"), Conf.TIMEOUT));
        entity.setPhone(getElementText(By.xpath("//*[@itemprop='telephone']"), Conf.TIMEOUT));
        entity.setFax(getElementText(By.xpath("//*[@itemprop='faxNumber']"), Conf.TIMEOUT));
        entity.setCompanyWebsite(getElementText(By.xpath("//*[@itemprop='url']"), Conf.TIMEOUT));
        String district = getElementText(By.xpath("//*[@id='parish_info']/a"), Conf.TIMEOUT);
        String worshipType = getElementText(By.xpath("//*[@itemprop='rite']"), Conf.TIMEOUT);
        String language = getElementText(By.xpath("//*[@itemprop='language']"), Conf.TIMEOUT);
        entity.setLanguagesSpoken(language);
        System.out.println(entity);
        return singletonList(entity);
    }

    @Override
    public String getBaseUrl() {
        return BASE_URL;
    }
}