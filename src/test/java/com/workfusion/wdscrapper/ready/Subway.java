package com.workfusion.wdscrapper.ready;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.workfusion.wdscrapper.BaseTest;
import com.workfusion.wdscrapper.Conf;
import com.workfusion.wdscrapper.Data;
import com.workfusion.wdscrapper.api.Entity;
import com.workfusion.wdscrapper.api.Input;
import com.workfusion.wdscrapper.utils.StatesUtils;
import org.openqa.selenium.By;

public class Subway extends BaseTest {
    final By WORKING_INDICATOR_BY = By.xpath("//*[@id='FWHmap_spinner']");
    final String BASE_URL = "http://www.subway.com";
    final String LIST_ITEM_CONTAINER_WITHOUT_WEBSITE_LINK_XPATH = "//*[contains(@id,'resultsHolderArea')][not(.//a[@class='storeWebPageField'])]";
    final String LIST_ITEM_CONTAINER_WITH_WEBSITE_LINK_XPATH = "//*[contains(@id,'resultsHolderArea')][.//a[@class='storeWebPageField']]";
    static boolean ARE_CANADA_ENTITIES_COLLECTED = false;

    @Override
    public Collection<String> collectLinks(Input input) {
        return null;
    }

    @Override
    public List<Entity> extractEntities(String link) {
        return null;
    }

    @Override
    public String getBaseUrl() {
        return BASE_URL;
    }

    /**
     * Most of entities collects from search results list.
     * Some entity details collects from page opened by direct URL "Store Web Page". But not all items have this link.
     * @return
     */
    @Override
    public List<Entity> startTask(Input input) {
        List<Entity> entities = new ArrayList<>();
            String baseUrl = BASE_URL + "/storelocator/default.aspx";
            getDriver().get(baseUrl);
            closeInitialPopup();
            entities.addAll(getEntities(baseUrl, input.getStates()));
        if (!ARE_CANADA_ENTITIES_COLLECTED) {
            baseUrl = "http://w.subway.com/en-ca/findastore";
            getDriver().get(baseUrl);
            entities.addAll(getEntities(baseUrl, Arrays.asList(Data.CANADA_PROVINCES))); //TODO. Use data from csv.
            ARE_CANADA_ENTITIES_COLLECTED = true;
        }
        return entities;
    }

    // <editor-fold desc="Site utils">
    /**
     * The method implements main logic to collect all entities.
     * @param baseUrl   URL.
     * @param states    List of states.
     * @return          All collected entities.
     */
    private List<Entity> getEntities(String baseUrl, List<String> states) {
        List<Entity> entities = new ArrayList<>();
        List<Map> entityNamesWithWebStoreLink = new ArrayList<>();
        String region;
        for (String state : states) {
            do {
                region = StatesUtils.isValidAbbreviation(state) ? StatesUtils.getStateNameFromAbbreviation(state) : state;
                if (searchState(region)) {
                    entities.addAll(getListItemEntities(getDriver().getCurrentUrl()));
                    entityNamesWithWebStoreLink.addAll(getEntityValuesWithWebSiteLink());
                    if (!getDriver().getCurrentUrl().equals(baseUrl))
                        getDriver().get(baseUrl);
                }
                else
                    getDriver().get(baseUrl);
            } while (clickOnNextPage());
            log.info("Entities found from list: " + entities.size() + ". Region: " + region);
        }
        entities.addAll(collectEntitiesFromPage(entityNamesWithWebStoreLink));
        return entities;
    }

    /**
     * @return  Map of entity values from list items which have "Store Web Page" link.
     */
    private List<Map> getEntityValuesWithWebSiteLink() {
        List<Map> entityNamesWithWebStoreLink = new ArrayList<>();
        Map<String, String> entityValues;
        if (isPresent(By.xpath(LIST_ITEM_CONTAINER_WITH_WEBSITE_LINK_XPATH), 0)){
            int numberOfListItems = getDriver().findElements(By.xpath(LIST_ITEM_CONTAINER_WITH_WEBSITE_LINK_XPATH)).size();
            for (int i = 1; i < numberOfListItems + 1; i++) {
                entityValues = getEntityValues(LIST_ITEM_CONTAINER_WITH_WEBSITE_LINK_XPATH + "[" + i + "]");
                entityValues.put("url", getUrls(By.xpath(LIST_ITEM_CONTAINER_WITH_WEBSITE_LINK_XPATH + "[" + i + "]//a[contains(@class,'storeWebPageField')]"), 0).get(0));
                entityNamesWithWebStoreLink.add(entityValues);
            }
        }
        return entityNamesWithWebStoreLink;
    }

    /**
     * @param link  Entity page link.
     * @return      Entities with values collected from list without "Store Web Page" link.
     */
    private List<Entity> getListItemEntities(String link) {
        List<Entity> entities = new ArrayList<>();
        if (isPresent(By.xpath(LIST_ITEM_CONTAINER_WITHOUT_WEBSITE_LINK_XPATH), 0)) {
            int numberOfListItems = getDriver().findElements(By.xpath(LIST_ITEM_CONTAINER_WITHOUT_WEBSITE_LINK_XPATH)).size();
            for (int i = 1; i < numberOfListItems + 1; i++) {
                Map entityValues = getEntityValues(LIST_ITEM_CONTAINER_WITHOUT_WEBSITE_LINK_XPATH + "[" + i + "]");
                Entity entity = new Entity(getWebsiteUrl(), link);
                entity.setBusinessName((String) entityValues.get("businessName"));
                entity.setAddress((String) entityValues.get("address"));
                entity.setPhone((String) entityValues.get("phone"));
                entity.setOperationHours((String) entityValues.get("hoursOfOperations"));
                //entity.setFax(""); // Required but doesn't exist on the list.
                //entity.setCompanyWebsite(""); // Required but is not found anywhere.
                //entity.setCreditCardsAccepted(""); // Required but doesn't exist on the list.
                //entity.setHasEcommerce(""); // Required but is not found anywhere.
                entity.setFacebookURL("https://www.facebook.com/subway?_rdr=p");
                entity.setTwitterURL("https://twitter.com/subway");
                entities.add(entity);
                //DataStoreServiceClient.getInstance().insertEntities(Conf.getDatastoreName(), Lists.newArrayList(entity));
            }
        }
        return entities;
    }

    /**
     * @param listItemContainerXpath    List item container xpath.
     * @return                          Returns entity values collected from list.
     */
    private Map<String, String> getEntityValues(String listItemContainerXpath) {
        Map<String, String> entityValues = new HashMap<>();
        entityValues.put("businessName", getElementText(By.xpath(listItemContainerXpath + "//*[@class='address3Field']"), 0));
        String address = getElementText(By.xpath(listItemContainerXpath + "//*[@class='addressDetail']"), 0);
        String state = getTextByRegex(getElementText(By.xpath(listItemContainerXpath + "//*[@class='cityStateField']"), 0), "(?<=, ).\\w(?= )");
        String city = getTextByRegex(getElementText(By.xpath(listItemContainerXpath + "//*[@class='cityStateField']"), 0), ".*?(?=,)");
        String zip = getTextByRegex(getElementText(By.xpath(listItemContainerXpath + "//*[@class='cityStateField']"), 0), "(?<= \\w\\w ).*(?=,)");
        entityValues.put("address", getFullAddress(address, state, city, zip));
        entityValues.put("city", city);
        entityValues.put("state", state);
        entityValues.put("zip", zip);
        entityValues.put("phone", getElementText(By.xpath(listItemContainerXpath + "//*[@class='phoneField']"), 0));
        entityValues.put("hoursOfOperations", getElementText(By.xpath(listItemContainerXpath + "//*[@class='dailyHours']"), 0));
        return entityValues;
    }

    /**
     * Returns entity from page opened by direct URL.
     * @param entityValues  Entity values collected from list. Absent info will be collected from the page. Such as Fax, Credit Card...
     * @return              Entity with all found values.
     */
    public List<Entity> collectEntitiesFromPage(List<Map> entityValues) {
        List<Entity> entities = new ArrayList<>();
        int numberOfEntityPages = entityValues.size();
        for (int i = 0; i < numberOfEntityPages; i++) {
            Entity entity = new Entity(getWebsiteUrl(), (String)entityValues.get(i).get("url"));
            getDriver().get((String)entityValues.get(i).get("url"));
            entity.setBusinessName((String)entityValues.get(i).get("businessName"));
            entity.setAddress((String) entityValues.get(i).get("address"));
            entity.setPhone((String)entityValues.get(i).get("phone"));
            entity.setOperationHours((String) entityValues.get(i).get("hoursOfOperations"));
            entity.setFax(getTextByRegex(getElementText(By.xpath("//*[contains(@id,'StoreFax')][2]"), 0), "\\d+.*\\d"));
            //entity.setCompanyWebsite(""); // Required but is not found anywhere.
            entity.setCreditCardsAccepted(isVisible(By.xpath("//*[contains(text(),'Credit card')]"), 0) ? "Y" : "N");
            //entity.setHasEcommerce(""); // Required but is not found anywhere.
            //entity.setFacebookURL(""); // Required but is not found anywhere.
            //entity.setTwitterURL(""); // Required but is not found anywhere.
            entities.add(entity);
            //DataStoreServiceClient.getInstance().insertEntities(Conf.getDatastoreName(), Lists.newArrayList(entity));
            log.info("Entity page: " + (i + 1) + "/" + numberOfEntityPages + ". Entities found from page: " + entities.size() + ". Region: " + entityValues.get(i).get("state"));
        }
        return entities;
    }

    /**
     * Searches for state.
     *
     * @param state     State to search.
     * @return          true if popup "no results found" is not displayed, otherwise false.
     */
    private boolean searchState(String state) {
        By oopsCloseButton = By.id("btnCloseDialog");
        By searchField = By.xpath("//*[@id='searchLocationInput']");
        if (!state.equals("") && !getDriver().findElement(searchField).getAttribute("value").toLowerCase().trim().equals(state.trim().toLowerCase())) {
            getDriver().findElement(searchField).clear();
            getDriver().findElement(searchField).sendKeys(state);
            click(By.id("btnDoSearch"), Conf.TIMEOUT);
            isInvisible(WORKING_INDICATOR_BY, 10);
        }
        return isInvisible(oopsCloseButton, 0);
    }

    private boolean clickOnNextPage() {
        By buttonNext = By.xpath("//*[contains(@id,'imgNextPage')][@onmouseover]");
        String currentPageNumber = getElementText(By.xpath("//*[@class='inactivePageLink']"), 10);
        By inactiveLinkNumber = By.xpath("//*[@class='inactivePageLink'][text()='" + currentPageNumber + "']");
        if (isPresent(buttonNext, 0)) {
            click(buttonNext, Conf.TIMEOUT);
            isInvisible(inactiveLinkNumber, Conf.TIMEOUT);
            return true;
        }
        return false;
    }

    private void closeInitialPopup() {
        By popup = By.className("gno_closeIcon_sweepstakes");
        if (isVisible(popup, 0))
            click(popup, Conf.TIMEOUT);
    }
    // </editor-fold>
}