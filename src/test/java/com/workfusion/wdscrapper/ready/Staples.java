package com.workfusion.wdscrapper.ready;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.workfusion.wdscrapper.BaseTest;
import com.workfusion.wdscrapper.Conf;
import com.workfusion.wdscrapper.api.Entity;
import com.workfusion.wdscrapper.api.Input;
import com.workfusion.wdscrapper.utils.CommonUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class Staples extends BaseTest {

    @Override
    protected List<Entity> startEntitiesExtractionsByLinks(List<Input> inputsByStates) {
        List<Entity> entities = super.startEntitiesExtractionsByLinks(inputsByStates);

        try {
            cacheCaCommonData();
            String url = "http://storelocator.staples.ca/?locale=en_CA";
            Collection<String> links = retry(url, (u) -> collectLinks(u, "MB", 5000));
            log.info(String.format("number of collected links: %s for Canada", links.size()));
            for (String link : links) {
                entities.addAll(retry(link, (lnk) -> extractEntities(lnk)));
            }
        } finally {
            tearDown();
        }
        return entities;
    }

    @Override
    public Collection<String> collectLinks(Input input) {
        Set<String> links = collectLinks("http://storelocator.staples.com", input.getStates().get(0), 300);
        log.info(String.format("collecteLinks size: %s for state: %s", links.size(), input.getStates().get(0)));
        return links;
    }

    private Set<String> collectLinks(String url, String state, int radius) {
        Set<String> links = new HashSet<>();

        getDriver().get(url);
        WebElement inputAddress = getWebElement(By.id("addressInput"));
        inputAddress.clear();
        inputAddress.sendKeys(state);
        click(By.id("searchButton"), 0);
        CommonUtils.sleep(2);

        executeJs(String.format("window.jQuery('#radius').find('option[value=100]').val(%s)", radius));
        new Select(getWebElement(By.id("radius"))).selectByValue(String.valueOf(radius));

        click(By.id("searchButton"), 0);
        By moreResultsLnk = By.linkText("more results");
        if (isVisible(moreResultsLnk, 5)) {
            do {
                click(moreResultsLnk, 0);
                waitForAjaxDone();
            } while (isVisible(moreResultsLnk, 0));
        }

        links.addAll(getUrls(By.linkText("Store Info"), Conf.TIMEOUT));
        return links;
    }

    @Override
    public Collection<Entity> extractEntities(String link) {
        getDriver().get(link);
        Entity entity = new Entity(getWebsiteUrl(), link);
        entity.setBusinessName("STAPLES");
        String phone = getElementText(By.xpath("//div[@class='storeInfo']/div/span[4]"));
        if (phone.contains("P.")) {
            entity.setPhone(phone.replace("P.", ""));
        }
        String fax = getElementText(By.xpath("//div[@class='storeInfo']/div/span[5]"));
        if (fax.contains("F.")) {
            entity.setFax(fax.replace("F.", ""));
        }
        String storeNum = getElementText(By.xpath("//div[@class='storeInfo']/div/span[contains(., 'Store')]"));
        String details = getElementText(By.xpath("//div[@class='storeInfo']/div"));
        entity.setAddress(details.replace(phone, "").replace(fax, "").replace(storeNum, "").trim());
        entity.setOperationHours(getElementText(By.xpath("//div[@class='storeTime']/div")));

        setCommonData(entity);

        return Collections.singletonList(entity);
    }

    private void cacheCaCommonData() {
        getDriver().get("http://www.staples.ca");
        getCommonDataCache().put(GOOGLE_PLUS_URL, "https://plus.google.com/110604096289965306897");
        getCommonDataCache().put(YOUTUBE_URL, "https://www.youtube.com/user/StaplesTechTV");
        getCommonDataCache().put(FACEBOOK_URL, "https://www.facebook.com/StaplesCanada");
        getCommonDataCache().put(TWITTER_URL, "https://twitter.com/staplescanada");
        getCommonDataCache().put(LINKEDIN_URL, "https://www.linkedin.com/company/staples-canada");

        getCommonDataCache().put(HAS_ECOMMERCE, convertBoolean(isPresent(By.id("cartNav"), 0)));
        getCommonDataCache().put(CREDIT_CARDS_ACCEPTED, convertBoolean(true));
    }

    @Override
    protected void cacheCommonData() {
        getDriver().get(getBaseUrl());
        getCommonDataCache().put(GOOGLE_PLUS_URL, getUrl(By.xpath("//a[contains(@href, 'plus.google.com')]")));
        getCommonDataCache().put(YOUTUBE_URL, getUrl(By.xpath("//a[contains(@href, 'youtube.com')]")));
        getCommonDataCache().put(FACEBOOK_URL, getUrl(By.xpath("//a[contains(@href, 'facebook.com')]")));
        getCommonDataCache().put(TWITTER_URL, getUrl(By.xpath("//a[contains(@href, 'twitter.com')]")));
        getCommonDataCache().put(LINKEDIN_URL, getUrl(By.xpath("//a[contains(@href, 'linkedin.com')]")));


        getCommonDataCache().put(HAS_ECOMMERCE, convertBoolean(isPresent(By.xpath("//li[contains(@class, 'cart')]"), 0)));
        getCommonDataCache().put(CREDIT_CARDS_ACCEPTED, convertBoolean(true));
    }

    @Override
    public String getBaseUrl() {
        return "http://www.staples.com";
    }
}
