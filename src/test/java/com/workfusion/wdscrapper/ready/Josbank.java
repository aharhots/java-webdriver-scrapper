package com.workfusion.wdscrapper.ready;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.google.common.collect.Lists;
import com.workfusion.wdscrapper.BaseTest;
import com.workfusion.wdscrapper.api.Entity;
import com.workfusion.wdscrapper.api.Input;
import com.workfusion.wdscrapper.utils.CommonUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;


public class Josbank extends BaseTest {

    private static final String BASE_URL = "http://www.josbank.com";

    @Override
    public Collection<String> collectLinks(Input input) {
        return null;
    }

    @Override
    protected List<Entity> startTask(Input input) {
        final Set<Entity> entities = new HashSet<>();
        for (String zipCode : input.getZipCodes()) {
            log.info("Zip code processing " + zipCode);
            entities.addAll(retry(zipCode, this::extractEntitiesByZip));
        }
        return new ArrayList<>(entities);
    }

    private Set<Entity> extractEntitiesByZip(String zipCode) {
        Set<Entity> entities = new HashSet<>();
        getDriver().get(getBaseUrl() + "/AjaxStoreLocatorDisplayView?catalogId=14052&langId=-1&storeId=13452");
        final WebElement postalСode = getDriver().findElement(By.id("postalcode"));
        postalСode.clear();
        postalСode.sendKeys(zipCode);
        postalСode.submit();

        waitForAjaxDone();
        final boolean isStoresFound = getElementsByXpath("//div[@class='error-message' and text()='Stores not found.']").isEmpty();
        if (isStoresFound) {
            boolean isNextButtonDisabled = true;
            do {
                entities.addAll(extractEntities());
                WebElement nextButton = getElementByXpath("//div[@class=\"pagination\"]/a[text()='Next']");
                isNextButtonDisabled = nextButton.getAttribute("class").contains("inactive");
                if (!isNextButtonDisabled) {
                    CommonUtils.jse(getDriver()).executeScript("arguments[0].click()", nextButton);
                    waitForAjaxDone();
                }
            } while (!isNextButtonDisabled);
        }
        return entities;
    }

    private List<Entity> extractEntities() {
        final List<Entity> entities = Lists.newArrayList();
        if (isPresent(By.xpath("//div[@class='details']"), 5)) {
            List<WebElement> detailsBlock = getElementsByXpath("//div[@class='details']");
            for (int i = 1; i <= detailsBlock.size(); i++) {
                entities.add(extractEntity(i));
            }
        }
        return entities;
    }

    private Entity extractEntity(int i) {
        final Entity entity = new Entity(getWebsiteUrl(), getBaseUrl());
        final String addressLocality = getElementText(By.xpath("//ol/li[" + i + "]/div[@class='details']/span[@class='addr']"));
        final String addressRegion = getElementText(By.xpath("//ol/li[" + i + "]/div[@class='details']/span[@class='region']"));
        final String phoneNumber = getElementText(By.xpath("//ol/li[" + i + "]/div[@class='details']/span[@class='tel']"));
        final String hours = getElementText(By.xpath("//ol/li[" + i + "]/div[@class='details']/span[@class='store-hours']"));
        entity.setAddress(addressLocality + ", " + addressRegion);
        entity.setBusinessName("JOS A BANK");
        entity.setCompanyWebsite(getBaseUrl());
        entity.setOperationHours(hours);
        entity.setPhone(phoneNumber);

        setCommonData(entity);
        return entity;
    }

    @Override
    protected void cacheCommonData() {
        getDriver().get(getBaseUrl());
        getCommonDataCache().put(INSTAGRAM_URL, getUrl(By.xpath("//a[contains(@href, 'instagram.com')]")));
        getCommonDataCache().put(YOUTUBE_URL, getUrl(By.xpath("//a[contains(@href, 'youtube.com')]")));
        getCommonDataCache().put(FACEBOOK_URL, getUrl(By.xpath("//a[contains(@href, 'facebook.com')]")));
        getCommonDataCache().put(PINTEREST_URL, getUrl(By.xpath("//a[contains(@href, 'pinterest.com')]")));
        getCommonDataCache().put(TWITTER_URL, getUrl(By.xpath("//a[contains(@href, 'twitter.com')]")));
        getCommonDataCache().put(GOOGLE_PLUS_URL, getUrl(By.xpath("//a[contains(@href, 'plus.google.com')]")));

        getCommonDataCache().put(HAS_ECOMMERCE, convertBoolean(isPresent(By.xpath("//span[@class='minicartbag']"), 5)));
        getCommonDataCache().put(CREDIT_CARDS_ACCEPTED, convertBoolean(true));
    }

    @Override
    public List<Entity> extractEntities(String link) {
        return null;
    }

    @Override
    public String getBaseUrl() {
        return BASE_URL;
    }

}