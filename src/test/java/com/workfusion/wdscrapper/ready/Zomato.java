package com.workfusion.wdscrapper.ready;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.google.common.collect.Lists;
import com.workfusion.wdscrapper.BaseTest;
import com.workfusion.wdscrapper.Conf;
import com.workfusion.wdscrapper.api.Entity;
import com.workfusion.wdscrapper.api.Input;
import com.workfusion.wdscrapper.utils.CommonUtils;
import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class Zomato extends BaseTest {

    private static final String BASE_URL = "www.zomato.com";
    private static final String US_LINK = "/united-states";

    @Override
    public Collection<String> collectLinks(Input input) {
        final String startUrl = "https://" + getBaseUrl() + US_LINK;
        WebDriver driver = getDriver();
        driver.get(startUrl);
        List<String> cities = getUrls(By.cssSelector(".l2-list-item-l2-text"), Conf.TIMEOUT);
        final List<String> links = new ArrayList<>();
        for (String city : cities) {
            driver.get(city + "/restaurants");
            int totalPages = 1;
            try {
                totalPages = Integer.parseInt(driver.findElement(By.xpath("//*[contains(@class,'pagination-number')]//*")).getText().split("\\s")[3]);
            } catch (Exception ignore) {
            }
            for (int page = 0; page < totalPages; page++) {
                driver.get(city + "/restaurants?page=" + (page + 1));
                try {
                    List<String> restaurantUrls = getUrls(By.xpath("//*[contains(@class,'result-title')]"), Conf.TIMEOUT);
                    links.addAll(restaurantUrls);
                } catch (Exception ignore) {
                }
            }
        }
        return links;
    }

    @Override
    public List<Entity> extractEntities(String link) {
        final Entity entity = new Entity(getWebsiteUrl(), getBaseUrl());
        try {
            getDriver().get(link);
            final WebDriver driver = getDriver();
            entity.setBusinessName(getElementTextByXpath(driver, "//div[contains(@class, 'res-name-wrapper')]//span[@itemprop='name']"));
            String address = getElementTextByXpath(driver, "//*[@id=\"mainframe\"]/div[1]/div[1]/div[2]/div/div[4]/span[1]").split(",")[0].trim();
            String state = getElementTextByXpath(driver, "//*[@id=\"mainframe\"]/div[1]/div[1]/div[2]/div/div[4]/span[2]/a").split("\\s")[0];
            String city = getElementTextByXpath(driver, "//*[@id=\"mainframe\"]/div[1]/div[1]/div[2]/div/div[4]/span[1]").split(",")[1].trim();
            String zip = getElementTextByXpath(driver, "//*[@id=\"mainframe\"]/div[1]/div[1]/div[2]/div/div[4]/span[2]/a").split("\\s")[1];
            entity.setAddress(getFullAddress(address, state, city, zip));
            entity.setPhone(getElementTextByXpath(driver, "//span[@itemprop='telephone']"));
            entity.setCompanyWebsite(driver.getCurrentUrl());
            entity.setOperationHours(getElementTextByXpath(driver, "//*[@id=\"res-week-timetable\"]"));
            entity.setFacebookURL("https://www.facebook.com/zomato/");
            entity.setInstagramURL("https://www.instagram.com/zomato/");
            entity.setTwitterURL("https://twitter.com/zomato");
            entity.setPriceRange(getElementTextByXpath(driver, "//span[@itemprop='priceRange']"));
            entity.setCuisineDescription(getElementTextByXpath(driver, "//*[@id=\"mainframe\"]/div[1]/div[1]/div[5]/div[3]"));
            entity.setPaymentMethods("-");
            entity.setCreditCardsAccepted("N");
        } catch (Exception e){
            log.error("Zomato extraction exception: ", e);
        }
        return Lists.newArrayList(entity);
    }

    @Override
    public String getBaseUrl() {
        return BASE_URL;
    }

    private String getElementTextByXpath(WebDriver driver, String xpathExpression) {
        try {
            return getText(driver, driver.findElement(By.xpath(xpathExpression))).trim();
        } catch (Exception e) {
            return StringUtils.EMPTY;
        }
    }

    private String getText(WebDriver driver, WebElement element) {
        return (String) CommonUtils.jse(driver).executeScript("return arguments[0].innerText", element);
    }
}
