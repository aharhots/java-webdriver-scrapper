package com.workfusion.wdscrapper.ready;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.workfusion.wdscrapper.BaseTest;
import com.workfusion.wdscrapper.Conf;
import com.workfusion.wdscrapper.api.Entity;
import com.workfusion.wdscrapper.api.Input;
import com.workfusion.wdscrapper.utils.WebDriverHelper;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.text.WordUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class Zagat extends BaseTest {
    final String BASE_URL = "https://www.zagat.com";
    final By CATEGORIES_DROPDOWN_EXPAND_LINK = By.xpath("//*[contains(@class,'search-form top-form')]//a[@class='s-link']");
    final By CATEGORIES_DROPDOWN = By.xpath("//*[contains(@class,'js-vertical-select')]//ul");
    final String CATEGORY_NAME_LINK_XPATH = "//*[contains(@class,'search-form top-form')]//*[@class='js-models']//a[text()='CATEGORY_NAME']";
    final By SEARCH_LOADING = By.xpath("//*[@id='content']//*[contains(@class,'search-loading')]");

    @Override
    protected List<Input> generateTaskInputs() {
        List<Input> inputs;
        try {
            inputs = new ArrayList<>();

            getDriver().get(BASE_URL + "/locations");
            List<String> cityUrls = getUrls(By.xpath("//*[contains(@class,'list-usa')]//a"), Conf.TIMEOUT);
            for (String cityUrl : cityUrls) {
                Input input = new Input();
                input.setLinks(Collections.singletonList(cityUrl.replace("set-city", "p")));
                inputs.add(input);
            }
        } finally {
            tearDown();
        }
        return inputs;
    }

    @Override
    public Collection<String> collectLinks(Input input) {
        Set<String> links = new HashSet<>();
        for (String cityLnk : input.getLinks()) {
            String cityName = FilenameUtils.getBaseName(cityLnk);

            getDriver().get(cityLnk);
            if (isVisible(CATEGORIES_DROPDOWN_EXPAND_LINK, 10)) {
                List<String> categories = getCategories();
                for (String category : categories) {
                    selectCategory(category);
                    do {
                        List<String> pageLinks = getUrls(By.xpath("//*[@id='content']//h3/a"), 0);
                        links.addAll(pageLinks);
                        log.info("Links found: " + links.size() + ".\tCity: " + cityName);
                    } while (clickOnNextPage());
                }
            }
        }
        return links;
    }

    @Override
    public List<Entity> extractEntities(String link) {
        WebDriver driver = getDriver();
        List<Entity> entities = new ArrayList<>();
        driver.get(link);
        Entity entity = new Entity(getWebsiteUrl(), link);
        entity.setBusinessName(getElementText(By.id("main-content-title"), 0));
        String address = getElementText(By.xpath("//*[@itemprop='streetAddress']"), 0);
        String state = getElementText(By.xpath("//*[@itemprop='addressRegion']"), 0);
        String city = getElementText(By.xpath("//*[@itemprop='addressLocality']"), 0);
        String zip = getElementText(By.xpath("//*[@itemprop='postalCode']"), 0);
        entity.setAddress(getFullAddress(address, state, city, zip));
        String phone = getTextByRegex(getElementText(By.xpath("//*[@itemprop='address']"), 0), "(?<=\\n)\\d.*");
        entity.setPhone(phone);
        List<String> webSiteUrls = getUrls(By.xpath("//*[@itemprop='address']//a[@class='website']"), 0);
        entity.setCompanyWebsite(!webSiteUrls.isEmpty() ? webSiteUrls.get(0) : "");
        entity.setOperationHours(getHours());
        entities.add(entity);
        log.info("Entities found for link: " + link);
        return entities;
    }

    @Override
    public String getBaseUrl() {
        return BASE_URL;
    }

    private String getHours() {
        By hoursContainerBy = By.xpath("//*[contains(@class,'all-hours')]");
        if (isPresent(hoursContainerBy, 0)) {
            WebDriverHelper.makeElementVisible(getDriver(), hoursContainerBy);
            return getElementText(hoursContainerBy, 0);
        }
        return "";
    }

    /**
     * Returns list of categories from categories dropdown.
     *
     * @return Categories. E.g: shopping, hotels, nightlife, restaurants, attractions.
     */
    private List<String> getCategories() {
        WebDriverHelper.makeElementVisible(getDriver(), CATEGORIES_DROPDOWN);
        By category = By.xpath("//*[contains(@class,'search-form top-form')]//*[@class='js-models']//a");
        List<String> availableCategories = getElementTexts(category, 5);
        WebDriverHelper.makeElementInvisible(getDriver(), CATEGORIES_DROPDOWN);
        return availableCategories;
    }

    /**
     * Selects category from dropdown. E.g: shopping, hotels, nightlife, restaurants, attractions.
     *
     * @param categoryName Category name to select
     */
    private void selectCategory(String categoryName) {
        WebDriverHelper.makeElementVisible(getDriver(), CATEGORIES_DROPDOWN);
        By categoryBy = By.xpath(CATEGORY_NAME_LINK_XPATH.replace("CATEGORY_NAME", WordUtils.capitalize(categoryName)));
        click(categoryBy, Conf.TIMEOUT);
        isInvisible(SEARCH_LOADING, Conf.TIMEOUT);
    }

    /**
     * Clicks on next page button.
     *
     * @return true if next page button was found and clicked, otherwise false.
     */
    private boolean clickOnNextPage() {
        By nextLinkBy = By.xpath("//*[@class='paging hide-mob paging-bottom']//*[@class='next ']//a");
        String inactivePageLinkXpath = "//*[@class='paging hide-mob paging-bottom']//strong";
        if (isVisible(nextLinkBy, 0)) {
            String inactivePageLinkText = getDriver().findElement(By.xpath(inactivePageLinkXpath)).getText();
            click(nextLinkBy, Conf.TIMEOUT);
            isInvisible(By.xpath(inactivePageLinkXpath + "[text()='" + inactivePageLinkText + "']"), Conf.TIMEOUT);
            return true;
        }
        return false;
    }
}
