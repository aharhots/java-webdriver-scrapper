package com.workfusion.wdscrapper.ready;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.workfusion.wdscrapper.BaseTest;
import com.workfusion.wdscrapper.Conf;
import com.workfusion.wdscrapper.api.Entity;
import com.workfusion.wdscrapper.api.Input;
import com.workfusion.wdscrapper.utils.CommonUtils;
import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Ulta extends BaseTest {

    @Override
    protected List<Entity> startTask(Input input) {
        return extractEntities(input);
    }

    private List<Entity> extractEntities(Input input) {
        List<Entity> entities = new ArrayList<>();
        Set<String> addresses = new HashSet<>();

        openStoreLocatorPage();
        for (String zip : input.getZipCodes()) {
            entities.addAll(retry(zip, (zipCode) -> extractEntities(zipCode, addresses)));
        }
        return entities;
    }

    @Override
    protected void openStoreLocatorPage() {
        getDriver().get("http://www.ulta.com/ulta/stores/storelocator.jsp");
        CommonUtils.sleep(3);
    }

    private List<Entity> extractEntities(String zip, Set<String> addresses) {
        List<Entity> entities = new ArrayList<>();
        if (isPresent(By.id("zipCode"), Conf.TIMEOUT)) {
            WebElement inputPostalCode = getDriver().findElement(By.id("zipCode"));
            inputPostalCode.clear();
            inputPostalCode.sendKeys(zip);
            click(By.xpath("//input[@class='storelocator-search-btn']"), Conf.TIMEOUT);
            CommonUtils.sleep(2);

            String errorMsg = getElementText(By.id("errorMessageDisplay"), 0);
            if (StringUtils.isBlank(errorMsg)) {
                WebDriverWait wait = new WebDriverWait(getDriver(), Conf.TIMEOUT);
                wait.until(ExpectedConditions.presenceOfElementLocated(By.id("storedetails-container")));
                entities.addAll(extractEntities(addresses));
            }
        }
        return entities;
    }

    private List<Entity> extractEntities(Set<String> addresses) {
        List<Entity> entities = new ArrayList<>();
        int itemSize = getElements(By.xpath("//div[@class='store-details']"), 1).size();
        for (int i = 1; i <= itemSize; i++) {
            String address = getElementTextByXpath("//div[@class='store-details'][" + i + "]/div/p[@class = 'address']");
            if (!addresses.contains(address)) {
                addresses.add(address);
                entities.add(extractEntity(i, address));
            }
        }
        return entities;
    }

    private Entity extractEntity(int i, String address) {
        Entity entity = new Entity(getWebsiteUrl(), getBaseUrl());
        entity.setBusinessName("ULTA BEAUTY");
        entity.setAddress(address);
        entity.setPhone(getElementTextByXpath("//div[@class='store-details'][" + i + "]/div/p[@class = 'phone']").replace("Get Directions", "").trim());
        entity.setOperationHours(getElementTextByXpath("//div[@class='store-details'][" + i + "]/div[2]/p"));
        entity.setCompanyWebsite(getBaseUrl());
        setCommonData(entity);

        return entity;
    }

    @Override
    protected void cacheCommonData() {
        getDriver().get(getBaseUrl());
        getCommonDataCache().put(HAS_ECOMMERCE, convertBoolean(isVisible(By.id("hdrCartCnt"), 5)));
        getCommonDataCache().put(CREDIT_CARDS_ACCEPTED, convertBoolean(true));
        getCommonDataCache().put(INSTAGRAM_URL, getUrl(By.xpath("//a[contains(@href, 'instagram.com')]")));
        getCommonDataCache().put(YOUTUBE_URL, getUrl(By.xpath("//a[contains(@href, 'youtube.com')]")));
        getCommonDataCache().put(FACEBOOK_URL, getUrl(By.xpath("//a[contains(@href, 'facebook.com')]")));
        getCommonDataCache().put(PINTEREST_URL, getUrl(By.xpath("//a[contains(@href, 'pinterest.com')]")));
        getCommonDataCache().put(TWITTER_URL, getUrl(By.xpath("//a[contains(@href, 'twitter.com')]")));
    }

    @Override
    public Collection<String> collectLinks(Input input) {
        return new ArrayList<>();
    }

    @Override
    public List<Entity> extractEntities(String link) {
        return new ArrayList<>();
    }

    @Override
    public String getBaseUrl() {
        return "http://www.ulta.com";
    }
}
