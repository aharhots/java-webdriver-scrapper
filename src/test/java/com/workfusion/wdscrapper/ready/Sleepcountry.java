package com.workfusion.wdscrapper.ready;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.workfusion.wdscrapper.BaseTest;
import com.workfusion.wdscrapper.Conf;
import com.workfusion.wdscrapper.api.Entity;
import com.workfusion.wdscrapper.api.Input;
import com.workfusion.wdscrapper.utils.CommonUtils;
import org.openqa.selenium.By;

public class Sleepcountry extends BaseTest {

    @Override
    protected List<Input> generateTaskInputs() {
        List<Input> inputs;
        try {
            inputs = new ArrayList<>();
            getDriver().get("http://stores.sleeptrain.com/");
            List<String> stateLnks = getUrls(By.xpath("//div[@class='tlsmap_list']/li/a"), Conf.TIMEOUT);
            for (String stateLnk : stateLnks) {
                Input input = new Input();
                input.setLinks(Collections.singletonList(stateLnk));
                inputs.add(input);
            }
        } finally {
            tearDown();
        }
        return inputs;
    }

    @Override
    public Collection<String> collectLinks(Input input) {
        Set<String> links = new HashSet<>();
        for (String stateLnk : input.getLinks()) {
            getDriver().get(stateLnk);
            Set<String> cityLnks = new HashSet<>();
            cityLnks.addAll(getUrls(By.xpath("//div[@class='tlsmap_list']/li/a"), Conf.TIMEOUT));
            for (String cityLnk : cityLnks) {
                getDriver().get(cityLnk);
                links.addAll(getUrls(By.linkText("Store Info"), Conf.TIMEOUT));
            }
        }
        return links;
    }

    @Override
    public List<Entity> extractEntities(String link) {
        getDriver().get(link);
        CommonUtils.sleep(1);

        List<Entity> entities = new ArrayList<>();
        Entity entity = new Entity(getWebsiteUrl(), link);
        entity.setBusinessName("SLEEP COUNTRY USA");
        entity.setAddress(getElementText(By.id("rls-address")));
        entity.setPhone(getElementText(By.xpath("//div[@class='rio-store-phone']/span")));
        entity.setOperationHours(getElementText(By.xpath("//div[@class='hours']")));

        setCommonData(entity);

        entities.add(entity);
        return entities;
    }

    @Override
    protected void cacheCommonData() {
        getDriver().get(getBaseUrl());
        getCommonDataCache().put(FACEBOOK_URL, getUrl(By.xpath("//a[contains(@href, 'facebook.com')]")));
        getCommonDataCache().put(TWITTER_URL, getUrl(By.xpath("//a[contains(@href, 'twitter.com')]")));
        getCommonDataCache().put(YOUTUBE_URL, getUrl(By.xpath("//a[contains(@href, 'youtube.com')]")));

        getCommonDataCache().put(HAS_ECOMMERCE, convertBoolean(isPresent(By.id("mini-cart"), 0)));
        getCommonDataCache().put(CREDIT_CARDS_ACCEPTED, convertBoolean(true));
    }

    @Override
    public String getBaseUrl() {
        return "http://www.sleepcountry.com";
    }
}
