package com.workfusion.wdscrapper.ready;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.workfusion.wdscrapper.BaseTest;
import com.workfusion.wdscrapper.Conf;
import com.workfusion.wdscrapper.api.Entity;
import com.workfusion.wdscrapper.api.Input;
import com.workfusion.wdscrapper.utils.CommonUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class Jambajuice extends BaseTest {

    @Override
    protected List<Entity> startTask(Input input) {
        List<Entity> entities = new ArrayList<>();
        Set<String> addresses = new HashSet<>();

        openStoreLocatorPage();
        for (String zip : input.getZipCodes()) {
            entities.addAll(retry(zip, (zipCode) -> extractEntities(zipCode, addresses)));
        }
        return entities;
    }

    @Override
    protected void openStoreLocatorPage() {
        getDriver().get("http://www.jambajuice.com/find-a-store");
        CommonUtils.sleep(3);
    }

    private List<Entity> extractEntities(String zip, Set<String> addresses) {
        List<Entity> entities = new ArrayList<>();
        if (isPresent(By.id("zipCode"), Conf.TIMEOUT)) {
            WebElement inputPostalCode = getDriver().findElement(By.id("zipCode"));
            inputPostalCode.clear();
            inputPostalCode.sendKeys(zip);
            click(By.xpath("//input[@class='findStore']"), Conf.TIMEOUT);
            CommonUtils.sleep(5);

            if (isVisible(By.cssSelector("div.results"), 5)) {
                entities.addAll(extractEntities(addresses));
            }
        }
        return entities;
    }

    private List<Entity> extractEntities(Set<String> addresses) {
        List<Entity> entities = new ArrayList<>();
        int itemSize = getElements(By.xpath("//tr[@class='line']/td/address"), 1).size();
        for (int i = 1; i <= itemSize; i++) {
            String phone = getElementText(By.xpath("(//tr[@class='line']/td/address/a)[" + i + "]"));
            String address = getElementText(By.xpath("(//tr[@class='line']/td/address)[" + i + "]")).replace(phone, "").trim();
            if (!addresses.contains(address)) {
                entities.add(extractEntity(i, phone, address));
                addresses.add(address);
            }
        }
        return entities;
    }

    private Entity extractEntity(int i, String phone, String address) {
        Entity entity = new Entity(getWebsiteUrl(), getBaseUrl());
        entity.setBusinessName("JAMBA JUICE");
        entity.setPhone(phone);
        entity.setAddress(address);
        entity.setOperationHours(getElementText(By.xpath("(//tr[@class='line']/td/div[@class = 'hours'][1])[" + i + "]")));
        entity.setCompanyWebsite(getBaseUrl());

        setCommonData(entity);

        return entity;
    }

    @Override
    protected void cacheCommonData() {
        getDriver().get(getBaseUrl());
        getCommonDataCache().put(YOUTUBE_URL, getUrl(By.xpath("//a[contains(@href, 'youtube.com')]")));
        getCommonDataCache().put(FACEBOOK_URL, getUrl(By.xpath("//a[contains(@href, 'facebook.com')]")));
        getCommonDataCache().put(TWITTER_URL, getUrl(By.xpath("//a[contains(@href, 'twitter.com')]")));
    }

    @Override
    public Collection<String> collectLinks(Input input) {
        return new ArrayList<>();
    }

    @Override
    public List<Entity> extractEntities(String link) {
        return new ArrayList<>();
    }

    @Override
    public String getBaseUrl() {
        return "http://www.jambajuice.com";
    }
}
