package com.workfusion.wdscrapper.ready;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.workfusion.wdscrapper.BaseTest;
import com.workfusion.wdscrapper.Conf;
import com.workfusion.wdscrapper.api.Entity;
import com.workfusion.wdscrapper.api.Input;
import com.workfusion.wdscrapper.utils.CommonUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class Coffeebeanery extends BaseTest {

    public static final String BASE_URL = "http://www.coffeebeanery.com/store-locator";

    @Override
    public Collection<String> collectLinks(Input input) {
        Set<String> links = new HashSet<>();

        log.info("Start collecting links for zipCode.length: " + input.getZipCodes().size());
        getDriver().get(BASE_URL);
        for (String zip : input.getZipCodes()) {
            links.addAll(retry(zip, this::collectLinks));
        }

        log.info("End collecting links for zipCode.length: " + input.getZipCodes().size());
        return links;
    }

    private Set<String> collectLinks(String zip) {
        Set<String> links = new HashSet<>();
        WebElement inputPostalCode = getDriver().findElement(By.id("inputPostalCode"));
        inputPostalCode.clear();
        inputPostalCode.sendKeys(zip);
        click(By.xpath("//button[contains(@class, 'ejs-find-a-store-dosearch')]"), Conf.TIMEOUT);
        links.addAll(getUrls(By.xpath("//ul[contains(@class, 'findastorebox')]/li/a[@class='strong']"), 2));
        return links;
    }

    @Override
    public List<Entity> extractEntities(String link) {
        log.info("Start extractEntities for link: " + link);

        getDriver().get(link);
        List<Entity> entities = new ArrayList<>();
        Entity entity = new Entity(getWebsiteUrl(), link);

        entity.setBusinessName("COFFEE BEANERY");

        String elementText = getElementTextByXpath("//div[@class='widget']/p");
        String email = getElementTextByXpath("//div[@class='widget']/p/a[contains(@href, 'mailto')]");
        elementText = elementText.replace(email, "").replace("Get directions", "").trim();
        String[] tokens = elementText.split("Phone");
        if (tokens.length > 0) {
            entity.setAddress(tokens[0]);
        }
        if (tokens.length > 1) {
            entity.setPhone(tokens[1].replaceAll("[^\\d]", ""));
        }
        entity.setCompanyWebsite(getBaseUrl());
        entity.setCompanyEmailAddress(email);
        entity.setFacebookURL(getCommonDataCache().get(FACEBOOK_URL));
        entity.setTwitterURL(getCommonDataCache().get(TWITTER_URL));
        entity.setGoogleplusURL(getCommonDataCache().get(GOOGLE_PLUS_URL));
        entity.setPinterestURL(getCommonDataCache().get(PINTEREST_URL));
        entity.setHasEcommerce(getCommonDataCache().get(HAS_ECOMMERCE));
        entity.setCreditCardsAccepted(getCommonDataCache().get(CREDIT_CARDS_ACCEPTED));

        log.info("Start extractEntities for link: " + link);

        entities.add(entity);
        return entities;
    }

    @Override
    protected void cacheCommonData() {
        getDriver().get(getBaseUrl());
        getCommonDataCache().put(FACEBOOK_URL, getUrl(By.xpath("//a[contains(@href, 'facebook.com')]")));
        getCommonDataCache().put(TWITTER_URL, getUrl(By.xpath("//a[contains(@href, 'twitter.com')]")));
        getCommonDataCache().put(GOOGLE_PLUS_URL, getUrl(By.xpath("//a[contains(@href, 'plus.google.com')]")));
        getCommonDataCache().put(PINTEREST_URL, getUrl(By.xpath("//a[contains(@href, 'pinterest.com')]")));

        boolean shoppingCart = isPresent(By.cssSelector("a[title = 'Shopping Cart']"), 0);
        getCommonDataCache().put(HAS_ECOMMERCE, convertBoolean(shoppingCart));

        getDriver().get("http://www.coffeebeanery.com/all-right-roasts");
        click(By.xpath("//*[@id='ListingProducts']/div[1]/div[1]/div/div[3]/a"), Conf.TIMEOUT);
        CommonUtils.sleep(1);

        WebElement qty = getDriver().findElement(By.cssSelector("ul[class*='addtocart-qte-section']>li>input"));
        qty.sendKeys("1");
        click(By.cssSelector("button[class *= 'ejs-addtocart'"), Conf.TIMEOUT);
        CommonUtils.sleep(1);

        getDriver().get("https://secure.coffeebeanery.com/place-the-order");
        CommonUtils.sleep(1);
        boolean creditCardAccepted = isPresent(By.xpath("//label[contains(., 'Credit Card')]"), 0);
        getCommonDataCache().put(CREDIT_CARDS_ACCEPTED, convertBoolean(creditCardAccepted));
    }

    @Override
    public String getBaseUrl() {
        return "http://www.coffeebeanery.com";
    }
}
