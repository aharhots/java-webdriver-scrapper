package com.workfusion.wdscrapper.ready;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import com.google.common.collect.Lists;
import com.workfusion.wdscrapper.BaseTest;
import com.workfusion.wdscrapper.Conf;
import com.workfusion.wdscrapper.api.Entity;
import com.workfusion.wdscrapper.api.Input;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 * Created by Vladimir Shvets
 * Date: 11/16/2015
 */
public class HealthGrades extends BaseTest {

    private static final String BASE_URL = "http://www.healthgrades.com";

    @Override
    public Collection<String> collectLinks(Input input) {
        final List<String> links = Lists.newArrayList();
        final WebDriver driver = getDriver();
        driver.get(getBaseUrl() + "/dentistry-general-directory");
        getLinks(input, links, driver);
        driver.get(getBaseUrl() + "/internal-medicine-directory");
        getLinks(input, links, driver);
        return links;
    }

    private void getLinks(Input input, List<String> links, WebDriver driver) {
        for (String state : input.getStates()) {
            final List<WebElement> cityLinks = driver.findElements(By.xpath("//a[@data-hgoname=\"city-link\" and contains(.,', " + state + "')]"));
            if (!cityLinks.isEmpty()) {
                final List<String> cityUrls = getUrls(By.xpath("//a[@data-hgoname=\"city-link\" and contains(.,', " + state + "')]"), Conf.TIMEOUT);
                for (int i = 0; i < cityUrls.size(); i++) {
                    //                for (int i = 0; i < 1; i++) {
                    String cityUrl = cityUrls.get(i);
                    driver.get(cityUrl);
                    Integer totalNumberOfPages = 1;
                    try {
                        totalNumberOfPages = Integer.valueOf(driver.findElement(By.id("totalNumberOfPages")).getText());
                    } catch (Exception ignored) {
                    }
                    for (int pageIndex = 1; pageIndex <= totalNumberOfPages; pageIndex++) {
                        try {
                            Thread.sleep(1000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        final List<WebElement> listOfDoctorsOnPage = driver.findElement(By.id("providerSearchResults")).findElements(By.xpath("//div[@class = 'providerInfo']/h2/a"));
                        links.addAll(listOfDoctorsOnPage.stream().map(doctorElement -> doctorElement.getAttribute("href")).collect(Collectors.toList()));
                        if (pageIndex != totalNumberOfPages) {
                            // don't click on both!
                            driver.findElements(By.xpath("//a[@title=\"Next Page\"]")).get(0).click();

                        }
                    }
                }
            }
        }
    }

    @Override
    public List<Entity> extractEntities(String link) {
        final WebDriver driver = getDriver();
        driver.get(link);
        final Entity entity = new Entity(getWebsiteUrl(), getBaseUrl());
        try {
            // overview page
            final String name = getElementTextByXpath("//span[@itemprop=\"name\"]");
            if (name.contains(",")) {
                String[] splitName = name.split(",");
                entity.setPrimaryContactTitle(splitName[splitName.length - 1]);
            }
            if (name.contains(".")) {
                entity.setProfessionalSuffix(name.split(".")[0]);
            }
            entity.setAcceptingNewPatients(getElementTextByXpath("//div[contains(.,'Accepting new patients?')]/ul[1]"));
            entity.setPrimaryContactName(name);
            String address = getElementTextByXpath("//span[@itemprop='streetAddress']");
            String city = getElementTextByXpath("//span[@itemprop='addressLocality']");
            String state = getElementTextByXpath("//span[@itemprop='addressRegion']");
            String zip = getElementTextByXpath("//span[@itemprop='postalCode']");
            entity.setAddress(getFullAddress(address, city, state, zip));
            entity.setCompanyWebsite(getElementAttrByXPath("//*a[@title=\"View Practice Website\"]", "href"));
            entity.setBusinessName(getElementTextByXpath("//*[@id=\"premiumProfileSummaryWrapper\"]/div/div[2]/div[2]/div/div/div/div/div[1]/a"));
            entity.setContactPhoneNumber(getElementTextByXpath("//*[@id=\"summaryBlockSinglePhone\"]/div/div/span[1]/span/span[2]/span"));
            entity.setBoardCertified(String.valueOf(getElementTextByXpath("//h4[span/a[text()='Board Certifications']][1]/following-sibling::ul[1]/li").isEmpty()));
            entity.setSpecialties(getElementTextByXpath("//h4[text()='Specialties'][1]/following-sibling::ul[1]/li"));
            //        entity.setTollFreePhone();
            //        entity.setContactEmailAddress();
            //        entity.setOperationHours();
            entity.setBoardCertifications(getElementTextByXpath("//h4[span/a[text()='Board Certifications']][1]/following-sibling::ul[1]/li"));
            // Experience link data-hgoname="background-tab"
            String experienceTab = getElementByXpath("//a[@data-hgoname=\"background-tab\"]").getAttribute("href");
            driver.get(experienceTab);
            entity.setMedicalSchoolAttended(getElementTextByXpath("//*[@id=\"backgroundEducationAndTraining2\"]//div[contains(.,'Medical School') and dl]/dl/dt"));
            entity.setGraduationYearYYYY(getElementTextByXpath("//*[@id=\"backgroundEducationAndTraining2\"]//div[contains(.,'Medical School') and dl]/dl/dd/span"));
            entity.setResidencyHospital(getElementTextByXpath("//*[@id=\"backgroundEducationAndTraining2\"]//div[contains(.,'Residency Hospital') and dl]/dl/dt"));
            entity.setResidencyYearYYYY(getElementTextByXpath("//*[@id=\"backgroundEducationAndTraining2\"]//div[contains(.,'Residency Hospital') and dl]/dl/dd/span"));
            entity.setLanguagesSpokes(getElementsTextByXpath("//div[@id=\"backgroundLanguagesSpoken2\"]//ul/li", ";"));
            // Appointments & Offices link data-hgoname="appointments-tab"
            String appointmentsTab = getElementByXpath("//a[@data-hgoname=\"appointments-tab\"]").getAttribute("href");
            driver.get(appointmentsTab);
            String phoneAndOrFax = getElementsTextByXpath("//div[@class='officeItem'][1]//dd[@class='phoneNumber']", "|");
            if (phoneAndOrFax.contains("Fax")) {
                String[] split = phoneAndOrFax.split("|");
                for (String string : split) {
                    if (string.contains("Fax")) {
                        entity.setFax(string);
                    } else {
                        entity.setPhone(string);
                    }
                }
            } else {
                entity.setPhone(phoneAndOrFax);
            }
            entity.setAcceptedInsurances(getElementsTextByXpath("//*[@class=\"insurancePlanList\"]/li", ";"));


            entity.setAgeOfProfessional(getElementTextByXpath("//h2[span[@class='category']]"));
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return Lists.newArrayList(entity);
    }


    @Override
    public String getBaseUrl() {
        return BASE_URL;
    }
}
