package com.workfusion.wdscrapper.ready;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.workfusion.wdscrapper.BaseTest;
import com.workfusion.wdscrapper.Conf;
import com.workfusion.wdscrapper.api.Entity;
import com.workfusion.wdscrapper.api.Input;
import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class BaptistInfo extends BaseTest {

    protected List<Input> generateTaskInputs() {
        List<Input> inputs;
        try {
            inputs = new ArrayList<>();
            getDriver().get("http://www.believe.com/church-directory/");
            List<String> stateLnks = getUrls(By.xpath("//article/ul/li/a[contains(@href, '/church-directory/')]"), Conf.TIMEOUT);
            for (String stateLnk : stateLnks) {
                Input input = new Input();
                input.setLinks(Collections.singletonList(stateLnk));
                inputs.add(input);
            }
            log.info("retrieve data for states: " + stateLnks);
        } finally {
            tearDown();
        }
        return inputs;
    }

    @Override
    public Collection<String> collectLinks(Input input) {
        Set<String> links = new HashSet<>();
        for (String link : input.getLinks()) {
            getDriver().get(link + "/?group=A-Z");

            List<String> cities = getUrls(By.xpath("//article[@class='drop-shadow-curved-top']/ul[2]/li/a"), Conf.TIMEOUT);
            for (String city : cities) {
                getDriver().get(city);
                links.addAll(getUrls(By.xpath("//li[@class='church-list']/a[1]"), Conf.TIMEOUT));
            }
        }
        return links;
    }

    @Override
    public List<Entity> extractEntities(String link) {
        List<Entity> entities = new ArrayList<>();
        getDriver().get(link);

        WebDriverWait wait = new WebDriverWait(getDriver(), 5);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("article[role='church']")));

        Entity entity = new Entity(getWebsiteUrl(), link);
        entity.setBusinessName(getElementText(By.xpath("//header[@class='cf']/div/h1")));
        entity.setAddress(getElementText(By.xpath("//header[@class='cf']/div/span")));
        String phone = getElementText(By.xpath("//ul[@role='metadata']/li/h6[text()='Phone']/.."));
        if (StringUtils.isNoneBlank(phone)) {
            entity.setPhone(phone.replace("Phone", "").trim());
        }
        String musicMinistry = getElementText(By.xpath("//ul[@role='metadata']/li/h6[contains(.,'Music')]/.."));
        if (StringUtils.isNotBlank(musicMinistry)) {
            entity.setMusicMinistry(convertBoolean(StringUtils.isNotBlank(musicMinistry.replace("Music", "").trim())));
        }
        String email = getUrl(By.xpath("//ul[@role='metadata']/li/a[contains(@href, 'mailto:')]"));
        entity.setCompanyEmailAddress(email.replace("mailto:", ""));
        entity.setCompanyWebsite(getUrl(By.xpath("//ul[@role='metadata']/li/h6[contains(.,'Website')]/../a")));

        setCommonData(entity);

        entities.add(entity);
        return entities;
    }

    @Override
    protected void cacheCommonData() {
        getDriver().get(getBaseUrl());
        getCommonDataCache().put(GOOGLE_PLUS_URL, getUrl(By.xpath("//a[contains(@href, 'plus.google.com')]")));
        getCommonDataCache().put(FACEBOOK_URL, getUrl(By.xpath("//a[contains(@href, 'facebook.com')]")));
        getCommonDataCache().put(TWITTER_URL, getUrl(By.xpath("//a[contains(@href, 'twitter.com')]")));
        getCommonDataCache().put(PINTEREST_URL, getUrl(By.xpath("//a[contains(@href, 'pinterest.com')]")));
    }

    public String getBaseUrl() {
        return "http://www.baptistinfo.com";
    }
}