package com.workfusion.wdscrapper.ready;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.workfusion.wdscrapper.BaseTest;
import com.workfusion.wdscrapper.Conf;
import com.workfusion.wdscrapper.api.Entity;
import com.workfusion.wdscrapper.api.Input;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.By;

public class Maidpro extends BaseTest {

    @Override
    protected List<Input> generateTaskInputs() {
        try {
            List<Input> inputs = new ArrayList<>();
            getDriver().get("http://www.maidpro.com/locations/");
            List<String> stateLnks = getUrls(By.xpath("//a[contains(@href, '/local/state')]"), Conf.TIMEOUT);
            for (String stateLnk : stateLnks) {
                Input input = new Input();
                input.setLinks(Collections.singletonList(stateLnk));
                inputs.add(input);
            }
            return inputs;
        } finally {
            tearDown();
        }
    }

    @Override
    public Collection<String> collectLinks(Input input) {
        Set<String> links = new HashSet<>();
        for (String stateLnk : input.getLinks()) {
            links.addAll(retry(stateLnk, this::collectLinks));
        }
        return links;
    }

    private Set<String> collectLinks(String stateLnk) {
        Set<String> links = new HashSet<>();
        getDriver().get(stateLnk);
        if (!getDriver().getCurrentUrl().contains("no-service")) {
            links.addAll(getUrls(By.xpath("//div[contains(@class, 'city-list')]/ul/li/a[@href != '#']"), Conf.TIMEOUT));
        }
        return links;
    }

    @Override
    public List<Entity> extractEntities(String link) {
        List<Entity> entities = new ArrayList<>();

        getDriver().get(link);
        List<String> lnks = getUrls(By.linkText("Go to Local Office"), 0);
        if (CollectionUtils.isNotEmpty(lnks)) {
            for (String lnk : lnks) {
                getDriver().get(lnk);
                Entity entity = extractEntity(lnk);
                if (entity != null) {
                    entities.add(entity);
                }
            }
        } else {
            Entity entity = extractEntity(link);
            if (entity != null) {
                entities.add(entity);
            }
        }
        return entities;
    }

    private Entity extractEntity(String link) {
        String businessName = getElementText(By.xpath("//div[contains(@class, 'grid-66')]/h1"));
        if (StringUtils.isNotBlank(businessName)) {
            Entity entity = new Entity(getWebsiteUrl(), link);
            entity.setBusinessName(businessName);
            entity.setAddress(getElementText(By.xpath("//div[@class = 'address']")));
            entity.setPhone(getElementText(By.xpath("//p[@class = 'phone']")));
            entity.setCompanyEmailAddress(getElementText(By.xpath("//a[contains(@href, 'mailto')]")));
            setCommonData(entity);
            return entity;
        }
        return null;
    }

    @Override
    protected void cacheCommonData() {
        getDriver().get(getBaseUrl());
        getCommonDataCache().put(LINKEDIN_URL, getUrl(By.xpath("//a[contains(@href, 'linkedin.com')]")));
        getCommonDataCache().put(YOUTUBE_URL, getUrl(By.xpath("//a[contains(@href, 'youtube.com')]")));
        getCommonDataCache().put(FACEBOOK_URL, getUrl(By.xpath("//a[contains(@href, 'facebook.com')]")));
        getCommonDataCache().put(TWITTER_URL, getUrl(By.xpath("//a[contains(@href, 'twitter.com')]")));
    }

    @Override
    public String getBaseUrl() {
        return "http://www.maidpro.com";
    }
}
