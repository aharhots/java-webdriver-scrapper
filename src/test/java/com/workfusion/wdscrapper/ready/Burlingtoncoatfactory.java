package com.workfusion.wdscrapper.ready;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.google.common.base.Joiner;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.workfusion.wdscrapper.BaseTest;
import com.workfusion.wdscrapper.Conf;
import com.workfusion.wdscrapper.api.Entity;
import com.workfusion.wdscrapper.api.Input;
import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;


public class Burlingtoncoatfactory extends BaseTest {

    private static final String BASE_URL = "http://www.burlingtoncoatfactory.com";

    @Override
    public Collection<String> collectLinks(Input input) {
        final Set<String> links = Sets.newLinkedHashSet();
        for (String zipCode : input.getZipCodes()) {
            links.addAll(retry(zipCode, this::collectLinks));
        }
        return links;
    }

    private Set<String> collectLinks(String zipCode) {
        Set<String> links = new HashSet<>();
        getDriver().get("http://www.burlingtoncoatfactory.com/store-locator-results.aspx?q=" + zipCode + "&r=15");
        List<String> urlsFromWebElements = getUrlsFromWebElements(getElementsByXpath("//div[@class=\"resultBody\"]/a[1]"));
        if (!urlsFromWebElements.isEmpty()) {
            links.addAll(urlsFromWebElements);
            List<WebElement> nextPageLinks = getElementsByXpath("//a[@id=\"nextResults\"]");
            while (!nextPageLinks.isEmpty() && nextPageLinks.get(0).isDisplayed()){
                click(By.xpath("//a[@id=\"nextResults\"]/a"), Conf.TIMEOUT);
                try {
                    Thread.sleep(50);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                links.addAll(getUrlsFromWebElements(getElementsByXpath("//div[@class=\"resultBody\"]/a[1]")));
                nextPageLinks = getElementsByXpath("//a[@id=\"nextResults\"]");
            }
        }
        return links;
    }

    @Override
    public List<Entity> extractEntities(String link) {
        final WebDriver driver = getDriver();
        driver.get(link);
        final Entity entity = new Entity(getWebsiteUrl(), getBaseUrl());
        final List<Entity> result = Lists.newArrayList(entity);
        entity.setBusinessName(getElementTextByXpath("//div[1]/span[@class=\"storeDetailsSectionTitle\"]"));
        final String storeDetails = getElementTextByXpath("//div[1][@class=\"storeDetailsSection\"]");
        final String[] tokens = storeDetails.split("\n");
        String phone = tokens[tokens.length - 1];
        String onlyDigits = phone.replaceAll("[^\\d]", "").trim();
        if (onlyDigits.length() > 7) {
            entity.setPhone(phone);
            entity.setAddress(storeDetails.replace(phone, "").trim());
        } else {
            entity.setAddress(storeDetails);
        }

        final String hoursBlock = getElementTextByXpath("//div[3][@class=\"storeDetailsSection\"]");
        final String hours;
        if (hoursBlock.contains("Store hours:")) {
            final List<String> splitHoursBlockArray = Lists.newArrayList(hoursBlock.split("\n"));
            // Store hours: - first object
            splitHoursBlockArray.remove(0);
            hours = Joiner.on(StringUtils.SPACE).join(splitHoursBlockArray);
        } else {
            hours = StringUtils.EMPTY;
        }
        final String companyWebSite = driver.getCurrentUrl();
        entity.setOperationHours(hours);
        entity.setCompanyWebsite(companyWebSite);
        entity.setFacebookURL("https://www.facebook.com/BurlingtonCoatFactory");
        entity.setTwitterURL("https://twitter.com/Burlington");
        entity.setGoogleplusURL("https://plus.google.com/111968966818527904558");
        entity.setPinterestURL("https://www.pinterest.com/burlingtoncoat/");
        entity.setYoutubeURL("https://www.youtube.com/burlington");
        entity.setInstagramURL("https://www.instagram.com/burlingtonstyle/");
        return result;
    }

    @Override
    public String getBaseUrl() {
        return BASE_URL;
    }

    private List<String> getUrlsFromWebElements(List<WebElement> webElements) {
        final List<String> result = Lists.newArrayList();
        for (WebElement webElement : webElements) {
            result.add(webElement.getAttribute("href"));
        }
        return result;
    }

}