package com.workfusion.wdscrapper.ready;


import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.workfusion.wdscrapper.BaseTest;
import com.workfusion.wdscrapper.api.Entity;
import com.workfusion.wdscrapper.api.Input;
import com.workfusion.wdscrapper.utils.WebDriverHelper;
import org.openqa.selenium.By;

public class Fridays extends BaseTest {
    final String BASE_URL = "http://www.tgifridays.com";

    @Override
    public Collection<String> collectLinks(Input input) {
        return null;
    }

    @Override
    public List<Entity> extractEntities(String link) {
        return null;
    }

    @Override
    public String getBaseUrl() {
        return BASE_URL;
    }

    @Override
    public List<Entity> startTask(Input input) {
        try {
            log.info("Starting extraction for " + input);
            String range = "50"; // Available 25 or 50.
            List<String> inputZips = input.getZipCodes();
            List<Entity> entities = new ArrayList<>();
            for (int i = 0; i < inputZips.size(); i++) {
                String entityLink = BASE_URL + "/find?utf8=%E2%9C%93&q=" + inputZips.get(i) + "&r=" + range;
                getDriver().get(entityLink);
                By showAllToggleLink = By.xpath("//a[@class='show-all']");
                if (isPresent(showAllToggleLink, 0))
                    click(showAllToggleLink, 0);
                expandHours();
                entities.addAll(getPageEntities(entityLink));
                log.info("Entities found: " + entities.size() + "\tZip: " + (i + 1) + "/" + inputZips.size());
            }
            return entities;
        } finally {
            tearDown();
        }
    }

    private List<Entity> getPageEntities(String entityLink) {
        List<String> entityTexts = getElementTexts(By.xpath("//*[@class='store-nav-item-wrapper']"), 0);
        List<Entity> entities = new ArrayList<>();
        for (String entityText : entityTexts) {
            Entity entity = new Entity(getWebsiteUrl(), entityLink);
            entity.setBusinessName(getBusinessName(entityText));
            entity.setAddress(getFullAddress(getAddress(entityText), getState(entityText), getCity(entityText), getZip(entityText)));
            entity.setPhone(getPhone(entityText));
            entity.setOperationHours(getHours(entityText));
            entity.setInstagramURL("https://www.instagram.com/officialtgifridays");
            entity.setTwitterURL("https://twitter.com/TGIFridays");
            entity.setFacebookURL("https://www.facebook.com/TGIFridays");
            entity.setYoutubeURL("https://www.youtube.com/user/tgifridays");
            entities.add(entity);
        }
        return entities;
    }

    private String getHours(String entityText) {
        String[] splitEntityText = entityText.split("\\n");
        String hours = "";
        if (splitEntityText.length > 6)
            hours = splitEntityText[6] + "\n";
        if (splitEntityText.length > 7)
            hours += splitEntityText[7] + "\n";
        if (splitEntityText.length > 8)
            hours += splitEntityText[8] + "\n";
        if (splitEntityText.length > 9)
            hours += splitEntityText[9] + "\n";
        if (splitEntityText.length > 10)
            hours += splitEntityText[10] + "\n";
        if (splitEntityText.length > 11)
            hours += splitEntityText[11] + "\n";
        if (splitEntityText.length > 12)
            hours += splitEntityText[12];
        return hours;
    }

    private String getPhone(String entityText) {
        String[] splitEntityText = entityText.split("\\n");
        return splitEntityText.length > 4 ? splitEntityText[4] : "";
    }

    private String getZip(String entityText) {
        String[] splitEntityText = entityText.split("\\n");
        return splitEntityText.length > 3 ? getTextByRegex(splitEntityText[3], "\\d+") : "";
    }

    private String getCity(String entityText) {
        String[] splitEntityText = entityText.split("\\n");
        return splitEntityText.length > 3 ? getTextByRegex(splitEntityText[3], "\\w+") : "";
    }

    private String getState(String entityText) {
        String[] splitEntityText = entityText.split("\\n");
        return splitEntityText.length > 3 ? getTextByRegex(splitEntityText[3], "(?<= )\\w{2}") : "";
    }

    private String getAddress(String entityText) {
        String[] splitEntityText = entityText.split("\\n");
        return splitEntityText.length > 3 ? splitEntityText[2] + ", " + splitEntityText[3] : "";
    }

    private String getBusinessName(String entityText) {
        String[] splitEntityText = entityText.split("\\n");
        return splitEntityText.length > 1 ? splitEntityText[0] : "";
    }

    private void expandHours() {
        int numberOfHourContainers = getElements(By.xpath("//*[@class='store-hours']"), 0).size();
        for (int i = 0; i < numberOfHourContainers; i++)
            WebDriverHelper.makeElementVisible(getDriver(), By.xpath("//li[" + (i + 1) + "]//*[@class='store-hours']"));
    }
}
