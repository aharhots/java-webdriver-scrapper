package com.workfusion.wdscrapper.ready;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import com.google.common.base.Joiner;
import com.google.common.collect.Lists;
import com.workfusion.wdscrapper.BaseTest;
import com.workfusion.wdscrapper.api.Entity;
import com.workfusion.wdscrapper.api.Input;
import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.WebDriver;


public class Davidsbridal extends BaseTest {

    private static final String BASE_URL = "http://www.davidsbridal.com";

    @Override
    protected List<Input> generateTaskInputs() {
        return Lists.newArrayList(new Input(Lists.newArrayList()));
    }

    @Override
    protected List<Entity> startTask(Input input) {
        final WebDriver driver = getDriver();
        final List<Entity> entities = Lists.newArrayList();
        for (int pageNumber = 1; pageNumber < 1000; pageNumber++) {
            getNextPage(driver, pageNumber);
            if (getElementsByXpath("//div[@class=\"wwcmError\"]").isEmpty()) {
                entities.add(extractEntity(pageNumber));
            }
        }
        return entities;
    }

    @Override
    public Collection<String> collectLinks(Input input) {
        return null;
    }

    public List<Entity> extractEntities(String link) {
        return null;
    }

    @Override
    public String getBaseUrl() {
        return BASE_URL;
    }

    private Entity extractEntity(Integer pageIndex) {
        final Entity entity = new Entity(getWebsiteUrl(), getBaseUrl());
        String businessName = StringUtils.EMPTY;
        String addressStreet = StringUtils.EMPTY;
        String phoneNumber = StringUtils.EMPTY;
        final String[] details = getElementsTextByXpath("//*[@id=\"searchInfo_aplus\"]/div/div[1]/div[3]/div/div[1]", "").split("\n");
        if (details.length > 3) {
            businessName = details[0];
            phoneNumber = details[details.length - 1];
            addressStreet = Joiner.on(", ").join(Arrays.copyOfRange(details, 1, details.length - 1));
        } else {
            log.error("Page index: " + pageIndex);
        }
        final String hours = getElementTextByXpath("//*[@id=\"searchInfo_aplus\"]/div/div[1]/div[3]/div/div[2]").replace("\n", ", ");
        final String fb = "https://www.facebook.com/davidsbridal";
        final String twitter = "https://twitter.com/davidsbridal";
        final String pinterest = "https://www.pinterest.com/davidsbridal/";
        final String instagram = "https://www.instagram.com/davidsbridal/";
        final String yt = "https://www.youtube.com/user/davidsbridal";
        entity.setHasEcommerce("Y");
        entity.setCreditCardsAccepted("Y");
        entity.setBusinessName(businessName);
        entity.setAddress(addressStreet);
        entity.setOperationHours(hours);
        entity.setPhone(phoneNumber);
        entity.setFacebookURL(fb);
        entity.setTwitterURL(twitter);
        entity.setPinterestURL(pinterest);
        entity.setYoutubeURL(yt);
        entity.setInstagramURL(instagram);
        entity.setBusinessName(businessName);
        return entity;
    }

    private void getNextPage(WebDriver driver, int pageNumber) {
        driver.get(getBaseUrl() + "/AjaxStoreLocatorDirectionsView?stLocId=" + pageNumber);
    }

}