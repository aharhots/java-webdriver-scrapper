package com.workfusion.wdscrapper.ready;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.workfusion.wdscrapper.BaseTest;
import com.workfusion.wdscrapper.Conf;
import com.workfusion.wdscrapper.api.Entity;
import com.workfusion.wdscrapper.api.Input;
import com.workfusion.wdscrapper.utils.CommonUtils;
import com.workfusion.wdscrapper.utils.WebDriverHelper;
import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


public class Sbc extends BaseTest {

    @Override
    public Collection<String> collectLinks(Input input) {
        Set<String> links = new HashSet<>();

        getDriver().get("http://www.sbc.net/churchsearch/results.asp");
        executeJs("window.jQuery('select[name=\"churchlist_length\"]').find('option[selected=selected]').val('1000')");
        for (String zip : input.getZipCodes()) {
            links.addAll(retry(zip, this::collectLinks));
        }
        return links;
    }

    private Set<String> collectLinks(String zip) {
        Set<String> links = new HashSet<>();
        getDriver().get("http://www.sbc.net/churchsearch/results.asp?query=" + zip);
        waitForAjaxDone();

        List<String> ids = WebDriverHelper.getAttributes(getDriver(), By.cssSelector("#churchlist>tbody>tr"), "id");
        if (!ids.isEmpty()) {
            for (String id : ids) {
                click(By.id(id), 0);
                waitForAjaxDone();
            }
            links.addAll(getUrls(By.xpath("//div[contains(@class,'churchinfo')]/div/h3/a"), Conf.TIMEOUT));
        }
        return links;
    }

    @Override
    public List<Entity> extractEntities(String link) {
        List<Entity> entities = new ArrayList<>();

        getDriver().get(link);

        WebDriverWait wait = new WebDriverWait(getDriver(), 10);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@class='page-header']/h1")));
        CommonUtils.sleep(1);

        Entity entity = new Entity(getWebsiteUrl(), link);
        entity.setBusinessName(getElementText(By.xpath("//*[@class='page-header']/h1")));
        entity.setAddress(getElementText(By.xpath("//div[@id='cs-address']")));
        entity.setPhone(getElementText(By.xpath("//*[@id='cs-phone']/a")));
        entity.setCompanyEmailAddress(getElementText(By.xpath("//*[@id='cs-email']/a")));
        entity.setCompanyWebsite(getUrl(By.xpath("//*[@id='cs-url']/a")));

        String churchStuff = getElementText(By.xpath("//div/h4[contains(.,'Church Staff:')]/.."));
        if (StringUtils.isNotBlank(churchStuff)) {
            int beginStuffIndex = churchStuff.indexOf("Church Staff:") + "Church Staff:".length();
            int endStuffIndex = churchStuff.indexOf("\n", beginStuffIndex + 1);
            churchStuff = churchStuff.substring(beginStuffIndex, endStuffIndex);

            String contactName = getElementText(By.xpath("//div/h4[contains(.,'Church Staff:')]/../a[1]"));
            String contactTitle = getToken(CHURCH_TITLE_REGEX, contactName);
            entity.setPrimaryContactTitle(contactTitle);
            contactName = removeToken(contactName, CHURCH_TITLE_REGEX, contactTitle);

            String suffix = getToken(CHURCH_SUFFIX_REGEX, contactName);
            entity.setProfessionalSuffix(suffix);
            contactName = removeToken(contactName, CHURCH_SUFFIX_REGEX, suffix);
            entity.setPrimaryContactName(contactName);

            String email = getUrl(By.xpath("//div/h4[contains(.,'Church Staff:')]/../a[1]"));
            entity.setContactEmailAddress(email.replace("mailto:", "").trim());

            if (churchStuff.contains(",")) {
                String phone = churchStuff.substring(churchStuff.indexOf(","));
                entity.setContactPhoneNumber(phone.replaceAll("[^\\d]", ""));
            }
        }

        String details = getElementText(By.xpath("//*[@class='col-md-10 col-md-offset-1 col-xs-12']/p[2]"));
        if (details.contains("Members")) {
            Pattern pattern = Pattern.compile("Members:\\s*(\\d+,?\\.?\\s?\\d+)");
            Matcher matcher = pattern.matcher(details);
            if (matcher.find()) {
                entity.setChurchSize(matcher.group(1));
            }
        }

        if (details.contains("Average Attendance")) {
            Pattern pattern = Pattern.compile("Average Attendance:\\s*(\\d+,?\\.?\\s?\\d+)");
            Matcher matcher = pattern.matcher(details);
            if (matcher.find()) {
                entity.setAverageAttendance(matcher.group(1));
            }
        }

        entities.add(entity);
        return entities;
    }

    @Override
    public String getBaseUrl() {
        return "http://www.sbc.net";
    }
}
