package com.workfusion.wdscrapper.ready;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.workfusion.wdscrapper.BaseTest;
import com.workfusion.wdscrapper.api.Entity;
import com.workfusion.wdscrapper.api.Input;
import com.workfusion.wdscrapper.utils.CommonUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

/**
 * Created by Vladimir Shvets on 11/16/2015.
 */

public class EmbassySuites3Hilton extends BaseTest {

    private static String BASE_URL = "http://embassysuites3.hilton.com";

    @Override
    public Collection<String> collectLinks(Input input) {
        final List<String> links = new ArrayList<>();
        for (String state : input.getStates()) {
            getDriver().get("http://embassysuites3.hilton.com/en_US/es/search/findhotels/results.htm");
            getDriver().findElement(By.id("hotelSearchOneBox")).sendKeys(state);
            getDriver().findElement(By.xpath("//a[@class = 'linkBtn']")).click();

            String text = getDriver().findElement(By.xpath("//p[@class = 'showingTotal']")).getText();
            int number = Integer.parseInt(text.replaceAll("[^0-9]", ""));
            int pages = (number / 20);
            int i = 0;
            while (i < pages) {
                CommonUtils.jse(getDriver()).executeScript("scroll(0, 500000);");
                try {
                    Thread.sleep(5000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                i++;
            }
            List<WebElement> listOfDivs = getDriver().findElement(By.id("hotelsEndlessScrolling")).findElements(By.xpath("//div[@class = 'hotelDescription']/h2/a"));

            for (WebElement div : listOfDivs) {

                links.add(div.getAttribute("href"));
            }
        }
        return links;
    }

    @Override
    public List<Entity> extractEntities(String link) {
        final List<Entity> entities = new ArrayList<>();
        getDriver().navigate().to(link);
        final Entity entity = new Entity(getWebsiteUrl(), link);
        String address = getElementText(By.xpath("//span[@itemprop= 'addressLocality']"), 1) + getElementText(By
                .xpath("//span[@itemprop= 'addressRegion']"), 1) + getElementText(By.xpath("//span[@itemprop= 'postalCode']")
                , 1);
        entity.setAddress(address);
        entity.setHasEcommerce("Y");
        if (getDriver().findElements(By.xpath("//*[contains(text(), 'WiFi')]")).size() != 0) {
            entity.setWifi("Y");
        } else {
            entity.setWifi("N");
        }
        if (getDriver().findElements(By.xpath("//*[contains(text(), 'Valet Parking')]")).size() != 0) {
            entity.setHotelsValetParking("Y");
        } else if (getDriver().findElements(By.xpath("//*[contains(text(), 'Valet: Not Available')]")).size() != 0) {
            entity.setHotelsValetParking("N");
        } else if (getDriver().findElements(By.xpath("//*[contains(text(), 'Valet/unload')]")).size() != 0) {
            entity.setHotelsValetParking("Y");
        } else if (getDriver().findElements(By.xpath("//*[contains(text(), 'Valet:') and contains(text(), '$')]")).size() != 0) {
            entity.setHotelsValetParking("Y");
        }
        if (getDriver().findElements(By.xpath("//*[contains(text(), 'Pets allowed: No')]")).size() != 0) {
            entity.setHotelsPetFriendly("N");
        } else if (getDriver().findElements(By.xpath("//*[contains(text(), 'Pets allowed: Yes')]")).size() != 0) {
            entity.setHotelsPetFriendly("Y");
        }
        if (getDriver().findElements(By.xpath("//*[contains(text(), 'fitness')]")).size() != 0) {
            entity.setHotelsExerciseFacility("Y");
        } else {
            entity.setHotelsExerciseFacility("N");
        }
        entity.setCompanyWebsite(link);
        entity.setBusinessName(getElementText(By.xpath("//div[@class='property_details_container']/h1/a"), 1));
        entity.setAddress(getElementText(By.xpath("//span[@class='address']/span[@class='visualGroup']/span"), 1));
        entity.setPhone(getElementText(By.xpath("//span[@itemprop='telephone']"), 1));
        entity.setFax(getElementText(By.xpath("//span[@itemprop='fax']"), 1));
        entities.add(entity);
        return entities;
    }

    @Override
    public String getBaseUrl() {
        return BASE_URL;
    }
}
