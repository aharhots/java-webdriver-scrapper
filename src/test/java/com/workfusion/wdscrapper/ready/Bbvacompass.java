package com.workfusion.wdscrapper.ready;

import java.util.Collection;
import java.util.List;
import java.util.Set;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.workfusion.wdscrapper.BaseTest;
import com.workfusion.wdscrapper.api.Entity;
import com.workfusion.wdscrapper.api.Input;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;


public class Bbvacompass extends BaseTest {

    private static final String BASE_URL = "https://www.bbvacompass.com";

    @Override
    public Collection<String> collectLinks(Input input) {
        // BBVA Compass operates in Alabama, Arizona, California, Colorado, Florida, New Mexico and Texas.
        final WebDriver driver = getDriver();
        final Set<String> links = Sets.newLinkedHashSet();
        driver.get("https://www.bbvacompass.com/USA/");
        for (String state : input.getStates()) {
            final List<WebElement> stateLinks = getElementsByXpath("//a[@href=\"/USA/" + state + "/\"]");
            if (!stateLinks.isEmpty()){
                driver.get(getUrlsFromWebElements(stateLinks).get(0));
                final List<WebElement> cityLinks = getElementsByXpath("//ul/li/a[starts-with(@href, \"/USA/"+state+"/\")]");
                if (!cityLinks.isEmpty()){
                    final List<String> cityUrls = getUrlsFromWebElements(cityLinks);
                    for (String cityUrl : cityUrls) {
                        driver.get(cityUrl);
                        links.addAll(getUrlsFromWebElements(getElementsByXpath("//ul/li/a[starts-with(@href, \"/USA/" + state + "/\")]")));
                    }
                }
            }
        }
        return links;
    }

    @Override
    public List<Entity> extractEntities(String link) {
        final WebDriver driver = getDriver();
        driver.get(link);
        final Entity entity = new Entity(getWebsiteUrl(), getBaseUrl());
        final List<Entity> result = Lists.newArrayList(entity);
        final String businessName = getElementTextByXpath("//div[@class=\"block-branch-info\"]/h2");
        final String addressStreet = getElementTextByXpath("//span[@id='branchAddressStreet']");
        final String addressCityStateZip = getElementTextByXpath("//span[@id='branchAddressCityStateZip']");
        final String hours = getElementTextByXpath("//span[@id='branch-hours']");
        final String phoneNumber = getElementTextByXpath("//span[@class=\"number\"]");
        final String fb = "https://www.facebook.com/bbvacompass";
        final String twitter = "https://twitter.com/BBVACompass";
        final String googlePlus = "https://plus.google.com/+bbvacompass";
        final String pinterest = "https://www.pinterest.com/bbvacompass/";
        final String yt = "https://www.youtube.com/user/bbvacompass";
        entity.setBusinessName(businessName);
        entity.setAddress(addressStreet + ", " + addressCityStateZip);
        entity.setOperationHours(hours);
        entity.setPhone(phoneNumber);
        entity.setFacebookURL(fb);
        entity.setTwitterURL(twitter);
        entity.setGoogleplusURL(googlePlus);
        entity.setPinterestURL(pinterest);
        entity.setYoutubeURL(yt);
        entity.setBusinessName(businessName);
        return result;
    }

    @Override
    public String getBaseUrl() {
        return BASE_URL;
    }

    private List<String> getUrlsFromWebElements(List<WebElement> webElements){
        final List<String> result = Lists.newArrayList();
        for (WebElement webElement : webElements) {
            result.add(webElement.getAttribute("href"));
        }
        return result;
    }

}