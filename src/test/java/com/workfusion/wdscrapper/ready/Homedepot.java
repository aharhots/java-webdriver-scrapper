package com.workfusion.wdscrapper.ready;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.google.common.base.Joiner;
import com.google.common.collect.Lists;
import com.workfusion.wdscrapper.BaseTest;
import com.workfusion.wdscrapper.Conf;
import com.workfusion.wdscrapper.api.Entity;
import com.workfusion.wdscrapper.api.Input;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;


public class Homedepot extends BaseTest {

    private static final String BASE_URL = "http://www.homedepot.com";

    @Override
    protected List<Input> generateTaskInputs() {
        return Lists.newArrayList(new Input(Lists.newArrayList()));
    }

    @Override
    public Collection<String> collectLinks(Input input) {
        final WebDriver driver = getDriver();
        driver.get(getBaseUrl() + "/StoreFinder/storeDirectory");
        final List<String> links = Lists.newArrayList();
        final List<String> stateUrls = getUrls(By.xpath("//div[@id=\"stateListing\"]/div/ul/li/a"), Conf.TIMEOUT);
        for (String stateUrl : stateUrls) {
            driver.get(stateUrl);
            links.addAll(getUrls(By.xpath("//div[@id=\"localListings\"]/div/ul/li/a"), Conf.TIMEOUT));
        }
        return links;
    }

    @Override
    public List<Entity> extractEntities(String link) {
        final WebDriver driver = getDriver();
        driver.get(link);
        final Entity entity = new Entity(getWebsiteUrl(), getBaseUrl());
        final String businessName = getElementTextByXpath("//h1[@class=\"page-title\"]");
        final String phoneNumber = getElementTextByXpath("//span[@itemprop=\"telephone\"]");
        final String streetAddress = getElementTextByXpath("//span[@itemprop=\"streetAddress\"]");
        final String addressLocality = getElementTextByXpath("//span[@itemprop=\"addressLocality\"]");
        final String addressRegion = getElementTextByXpath("//span[@itemprop=\"addressRegion\"]");
        final String postalCode = getElementTextByXpath("//span[@itemprop=\"postalCode\"]");
        final ArrayList<String> addressList = Lists.newArrayList(streetAddress, addressLocality, addressRegion, postalCode);
        final String hours = getElementsTextByXpath("//li[@itemprop=\"openingHours\"]", ", ").replace("\n", ", ");
        final String fb = "https://www.facebook.com/homedepot";
        final String twitter = "https://twitter.com/HomeDepot";
        final String pinterest = "https://www.pinterest.com/homedepot/";
        final String yt = "https://www.youtube.com/user/homedepot";
        entity.setHasEcommerce("Y");
        entity.setCreditCardsAccepted("Y");
        entity.setBusinessName(businessName);
        entity.setCompanyWebsite(driver.getCurrentUrl());
        entity.setAddress(Joiner.on(", ").join(addressList));
        entity.setOperationHours(hours);
        entity.setPhone(phoneNumber);
        entity.setFacebookURL(fb);
        entity.setTwitterURL(twitter);
        entity.setPinterestURL(pinterest);
        entity.setYoutubeURL(yt);
        return Lists.newArrayList(entity);
    }

    @Override
    public String getBaseUrl() {
        return BASE_URL;
    }


}