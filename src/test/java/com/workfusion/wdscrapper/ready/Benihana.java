package com.workfusion.wdscrapper.ready;

import java.util.Collection;
import java.util.List;
import java.util.Set;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.workfusion.wdscrapper.BaseTest;
import com.workfusion.wdscrapper.api.Entity;
import com.workfusion.wdscrapper.api.Input;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;


public class Benihana extends BaseTest {

    private static final String BASE_URL = "http://www.benihana.com";

    @Override
    public Collection<String> collectLinks(Input input) {
        final Set<String> links = Sets.newLinkedHashSet();
        final WebDriver driver = getDriver();
        driver.get("http://www.benihana.com/locations");
        links.addAll(getUrlsFromWebElements(getElementsByXpath("//div[@class=\"locations-entry\"]/a")));
        return links;
    }

    @Override
    public List<Entity> extractEntities(String link) {
        final WebDriver driver = getDriver();
        driver.get(link);
        final Entity entity = new Entity(getWebsiteUrl(), getBaseUrl());
        final List<Entity> result = Lists.newArrayList(entity);
        final String businessName = getElementTextByXpath("//div[@data-fields=\"page_name\"]");
        final String hours = getElementTextByXpath("//div[@data-fields=\"contents, hours\"]").replace("\n", ", ");
        final String addressStreet = getElementTextByXpath("//div[@class='dynamic-content-row locations-row locations-row-top']/p[1]").replace("\n", ", ");
        final String phoneNumber = getElementTextByXpath("//div[@class='dynamic-content-row locations-row locations-row-top']/p[2]");
        final String companyWebSite = driver.getCurrentUrl();
        final String fb = "https://www.facebook.com/Benihana";
        final String twitter = "https://twitter.com/Benihana";
        final String googlePlus = "https://plus.google.com/110982853780491532857/posts";
        final String pinterest = "http://pinterest.com/benihanaus/";
        final String instagram = "http://instagram.com/benihana";
        final String yt = "http://www.youtube.com/benihana";

        entity.setBusinessName(businessName);
        entity.setAddress(addressStreet);
        entity.setOperationHours(hours);
        entity.setPhone(phoneNumber);
        entity.setCompanyWebsite(companyWebSite);
        entity.setFacebookURL(fb);
        entity.setTwitterURL(twitter);
        entity.setGoogleplusURL(googlePlus);
        entity.setPinterestURL(pinterest);
        entity.setYoutubeURL(yt);
        entity.setInstagramURL(instagram);
        entity.setBusinessName(businessName);
        return result;
    }

    @Override
    public String getBaseUrl() {
        return BASE_URL;
    }

    @Override
    protected List<Input> generateTaskInputs() {
        return Lists.newArrayList(new Input(Lists.newArrayList("FAKE_STATE")));
    }

    private List<String> getUrlsFromWebElements(List<WebElement> webElements) {
        final List<String> result = Lists.newArrayList();
        for (WebElement webElement : webElements) {
            result.add(webElement.getAttribute("href"));
        }
        return result;
    }

}