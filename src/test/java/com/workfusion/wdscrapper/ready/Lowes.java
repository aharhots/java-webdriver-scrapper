package com.workfusion.wdscrapper.ready;

import java.util.Collection;
import java.util.List;
import java.util.Set;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.workfusion.wdscrapper.BaseTest;
import com.workfusion.wdscrapper.Conf;
import com.workfusion.wdscrapper.api.Entity;
import com.workfusion.wdscrapper.api.Input;
import com.workfusion.wdscrapper.utils.CommonUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;


public class Lowes extends BaseTest {

    private static final String BASE_URL = "http://www.lowes.com";

    @Override
    protected List<Entity> startTask(Input input) {
        final WebDriver driver = getDriver();
        final Set<Entity> entities = Sets.newLinkedHashSet();
        for (String zipCode : input.getZipCodes()) {
            log.info("Current zipCode:" + zipCode);
            driver.get(getBaseUrl() + "/StoreLocatorDisplayView?storeId=10151&langId=-1&catalogId=10051");
            getElementByXpath("//*[@id=\"SL-map-search\"]").sendKeys(zipCode);
            click(By.xpath("//*[@id='SL-map-search-submit']"), Conf.TIMEOUT);
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            final List<WebElement> detailsBlocks = getElementsByXpath("//ol[@class=\"ui-datalist\"]/li");
            for (int i = 0; i < detailsBlocks.size(); i++) {
                entities.add(extractEntity(i + 1));
            }
        }
        log.info("entities: " + entities);
        System.out.println("entities: " + entities);
        return Lists.newArrayList(entities);
    }

    @Override
    public Collection<String> collectLinks(Input input) {
        return null;
    }

    public Entity extractEntity(Integer blockIndex) {
        final Entity entity = new Entity(getWebsiteUrl(), getBaseUrl());
        final String currentBlock = "//ol[@class=\"ui-datalist\"]/li[" + blockIndex + "]";
        final String businessName = getElementTextByXpath(currentBlock + "/strong[@class=\"store-name\"]");
        final String address = getElementsTextByXpath(currentBlock + "/span[@class=\"store-location\" or @class=\"store-location togglable\"]", ", ");
        final String phoneNumber = getElementTextContentByXpath(currentBlock +"/span[4]/text()").replace("Phone:", "").trim();
        final String faxNumber = getElementTextContentByXpath(currentBlock + "//span[4]/span/text()").replace("Fax:", "").trim();
        final String hours = getElementsTextByXpath(currentBlock + "/strong[@class=\"store-hours\" or @class=\"store-hours togglable\"]", ", ");
        final String fb = "https://www.facebook.com/lowes";
        final String twitter = "https://twitter.com/lowes";
        final String pinterest = "https://www.pinterest.com/lowes/?auto_follow=true";
        final String yt = "https://www.youtube.com/user/Lowes";
        final String instagram = "https://www.instagram.com/loweshomeimprovement/";
        final String gp = "https://plus.google.com/+lowes";
        entity.setBusinessName(businessName);
        entity.setHasEcommerce("Y");
        entity.setCreditCardsAccepted("Y");
        entity.setAddress(address);
        entity.setOperationHours(hours);
        entity.setPhone(phoneNumber);
        entity.setFax(faxNumber);
        entity.setFacebookURL(fb);
        entity.setTwitterURL(twitter);
        entity.setPinterestURL(pinterest);
        entity.setYoutubeURL(yt);
        entity.setGoogleplusURL(gp);
        entity.setInstagramURL(instagram);
        entity.setBusinessName(businessName);
        return entity;
    }

    protected String getElementTextContentByXpath(String xpathExpression) {
        return (String) CommonUtils.jse(getDriver()).executeScript("return document.evaluate(arguments[0], document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.textContent;",xpathExpression);
    }


    public List<Entity> extractEntities(String link) {
        return null;
    }

    @Override
    public String getBaseUrl() {
        return BASE_URL;
    }
}