package com.workfusion.wdscrapper.ready;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import com.workfusion.wdscrapper.BaseTest;
import com.workfusion.wdscrapper.Conf;
import com.workfusion.wdscrapper.api.Entity;
import com.workfusion.wdscrapper.api.Input;
import com.workfusion.wdscrapper.utils.CommonUtils;
import com.workfusion.wdscrapper.utils.WebDriverHelper;
import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.UnhandledAlertException;
import org.openqa.selenium.WebElement;

public class Havoline extends BaseTest {

    @Override
    protected List<Entity> startEntitiesExtractionsByLinks(List<Input> inputsByStates) {
        List<Entity> entities;
        try {
            entities = new ArrayList<>();
            Set<String> addresses = new HashSet<>();

            openStoreLocatorPage();
            for (String zip : Arrays.asList("89801", "80728", "54455", "75212", "10001", "32839", "99518")) {
                entities.addAll(retry(zip, (zipCode) -> searchAndExtract(zipCode, addresses)));
            }
        } finally {
            tearDown();
        }
        return entities;
    }

    @Override
    protected void openStoreLocatorPage() {
        getDriver().get("http://www.havoline.com/services/locations.aspx");
        CommonUtils.sleep(2);
        executeJs("window.jQuery('select.searchDistanceBox').find('option[selected=\"selected\"]').val('999')");
    }

    private List<Entity> searchAndExtract(String zip, Set<String> addresses) {
        List<Entity> entities = new ArrayList<>();
        if (isPresent(By.xpath("//input[@class='sftext']"), 5)) {
            WebElement inputPostalCode = getDriver().findElement(By.xpath("//input[@class='sftext']"));
            inputPostalCode.clear();
            inputPostalCode.sendKeys(zip);
            click(By.xpath("//input[contains(@class, 'btnXpressSearch')]"), Conf.TIMEOUT);
            CommonUtils.sleep(2);
            WebDriverHelper.closeAlertIfPresent(getDriver());

            try {
                entities.addAll(extractEntities(addresses));
            } catch (UnhandledAlertException e) {
                WebDriverHelper.closeAlertIfPresent(getDriver());
            }
        }
        return entities;
    }

    private List<Entity> extractEntities(Set<String> addresses) {
        List<Entity> entities = new ArrayList<>();

        String resultMsg = getElementText(By.xpath("//*[@id='stationList']/div[@class='xpressLubeListHdr']"));
        if (resultMsg.contains("No xpress lube locations found")) {
            return entities;
        }

        int itemSize = getElements(By.cssSelector("div[id='stationList']>p"), 5).size();
        for (int i = 1; i <= itemSize; i++) {
            String name = getElementText(By.xpath("//*[@id='stationList']/p[" + i + "]/a[1]"));
            String direction = getElementText(By.xpath("//*[@id='stationList']/p[" + i + "]/a[2]"));
            String elementText = getElementText(By.xpath("//*[@id='stationList']/p[" + i + "]")).replace(name, "").replace(direction, "");

            List<String> tokens = Arrays.asList(elementText.split("\\n"));
            List<String> addressTokens = tokens.stream().filter(t -> !t.contains("tel:") && !t.contains("distance:") && !t.isEmpty())
                    .collect(Collectors.toList());
            String address = StringUtils.join(addressTokens, " ").trim();
            if (!addresses.contains(address)) {
                Entity entity = new Entity(getWebsiteUrl(), getBaseUrl());
                entity.setBusinessName("HAVOLINE XPRESS LUBE");
                entity.setAddress(address);

                List<String> phoneTokens = tokens.stream().filter(t -> t.contains("tel:"))
                        .collect(Collectors.toList());
                entity.setPhone(StringUtils.join(phoneTokens, " ").replace("tel:", "").trim());

                entity.setCompanyWebsite(getBaseUrl());
                setCommonData(entity);

                addresses.add(address);
                entities.add(entity);
            }
        }
        return entities;
    }

    @Override
    protected void cacheCommonData() {
        getDriver().get(getBaseUrl());

        getCommonDataCache().put(YOUTUBE_URL, getUrl(By.xpath("//a[contains(@href, 'youtube.com')]")));
        getCommonDataCache().put(FACEBOOK_URL, getUrl(By.xpath("//a[contains(@href, 'facebook.com')]")));
        getCommonDataCache().put(TWITTER_URL, getUrl(By.xpath("//a[contains(@href, 'twitter.com')]")));
    }

    @Override
    public Collection<String> collectLinks(Input input) {
        return new ArrayList<>();
    }

    @Override
    public List<Entity> extractEntities(String link) {
        return new ArrayList<>();
    }

    @Override
    public String getBaseUrl() {
        return "http://www.havoline.com";
    }
}
