package com.workfusion.wdscrapper.ready;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.workfusion.wdscrapper.BaseTest;
import com.workfusion.wdscrapper.Conf;
import com.workfusion.wdscrapper.api.Entity;
import com.workfusion.wdscrapper.api.Input;
import com.workfusion.wdscrapper.utils.CommonUtils;
import org.openqa.selenium.By;

import static java.util.Collections.singletonList;

public class Baptist411 extends BaseTest {

    @Override
    public Collection<String> collectLinks(Input input) {
        Set<String> links = new HashSet<>();

        for (String state : input.getStates()) {
            links.addAll(retry(state, this::collectLinks));
        }
        return links;
    }

    private Set<String> collectLinks(String state) {
        Set<String> links = new HashSet<>();
        int pageNumber = 1;
        do {
            String url = String.format("http://www.baptist411.com//churches/display.php?search=%s&searchID=1&page=%s", state, pageNumber++);
            getDriver().get(url);
            CommonUtils.sleep(1);
            links.addAll(getUrls(By.cssSelector("a[href ^= '/churches/details/']"), Conf.TIMEOUT));
        } while (isPresent(By.linkText("Next 100"), 0));
        return links;
    }

    @Override
    public List<Entity> extractEntities(String link) {
        getDriver().get(link);

        Entity entity = new Entity(getWebsiteUrl(), link);
        entity.setBusinessName(getElementTextByXpath("//td[@class='largetitle']/table/tbody/tr/td/b"));
        String address1 = getElementTextByXpath("/html/body/center/table/tbody/tr[2]/td/table/tbody/tr/td[3]/table/tbody/tr/td/table/tbody/tr[4]/td/table/tbody/tr[1]/td[1]/table/tbody/tr[2]/td");
        String address2 = getElementTextByXpath("/html/body/center/table/tbody/tr[2]/td/table/tbody/tr/td[3]/table/tbody/tr/td/table/tbody/tr[4]/td/table/tbody/tr[1]/td[1]/table/tbody/tr[3]/td");
        entity.setAddress(address1 + " " + address2);
        String phone = getElementTextByXpath("/html/body/center/table/tbody/tr[2]/td/table/tbody/tr/td[3]/table/tbody/tr/td/table/tbody/tr[4]/td/table/tbody/tr[1]/td[1]/table/tbody/tr[4]/td");
        entity.setPhone(filterUnknown(phone));
        String email = getElementAttrByXPath("/html/body/center/table/tbody/tr[2]/td/table/tbody/tr/td[3]/table/tbody/tr/td/table/tbody/tr[4]/td/table/tbody/tr[1]/td[1]/table/tbody/tr[5]/td/img", "src");
        entity.setCompanyEmailAddress(filterUnknown(email).replace("http://www.baptist411.com/emailimg.php?email=", ""));

        String websiteText = getElementTextByXpath("/html/body/center/table/tbody/tr[2]/td/table/tbody/tr/td[3]/table/tbody/tr/td/table/tbody/tr[4]/td/table/tbody/tr[1]/td[1]/table/tbody/tr[6]/td");
        if (!websiteText.toLowerCase().contains("unknown")) {
            String website = getElementTextByXpath("/html/body/center/table/tbody/tr[2]/td/table/tbody/tr/td[3]/table/tbody/tr/td/table/tbody/tr[4]/td/table/tbody/tr[1]/td[1]/table/tbody/tr[6]/td/a");
            entity.setCompanyWebsite(website);
        }

        String contactName = getElementTextByXpath("/html/body/center/table/tbody/tr[2]/td/table/tbody/tr/td[3]/table/tbody/tr/td/table/tbody/tr[4]/td/table/tbody/tr[1]/td[1]/table/tbody/tr[8]/td");
        if (!contactName.toLowerCase().contains("unknown")) {
            String contactTitle = getToken(CHURCH_TITLE_REGEX, contactName);
            entity.setPrimaryContactTitle(contactTitle);
            contactName = removeToken(contactName, CHURCH_TITLE_REGEX, contactTitle);

            String suffix = getToken(CHURCH_SUFFIX_REGEX, contactName);
            entity.setProfessionalSuffix(suffix);
            contactName = removeToken(contactName, CHURCH_SUFFIX_REGEX, suffix);
            entity.setPrimaryContactName(contactName);
        }

        return singletonList(entity);
    }

    private String filterUnknown(String text) {
        return !text.toLowerCase().contains("unknown") ? text : "";
    }

    @Override
    public String getBaseUrl() {
        return "http://www.baptist411.com";
    }
}