package com.workfusion.wdscrapper.ready;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import com.workfusion.wdscrapper.BaseTest;
import com.workfusion.wdscrapper.api.Entity;
import com.workfusion.wdscrapper.api.Input;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

/**
 * Created by Vladimir Shvets on 12/10/2015.
 */
public class Olivegarden extends BaseTest {

    final String BASE_URL = "http://olivegarden.com";

    @Override
    public Collection<String> collectLinks(Input input) {
        Set<String> links = new HashSet<>();

        getDriver().get("http://www.olivegarden.com/locations/location-search");
        for (String state : input.getStates()) {
            links.addAll(retry( state, this::collectLinks));
        }
        return links;
    }

    private List<String> collectLinks(String state) {
        getDriver().findElement(By.id("searchText")).sendKeys(state);
        getDriver().findElement(By.id("locSearchIcon")).click();
        waitForAjaxDone();
        return getUrls(By.xpath("//a[contains(@id, 'directionsLink')]"), 10);
    }

    @Override
    public List<Entity> extractEntities(String link) {
        Entity entity = new Entity(getWebsiteUrl(), BASE_URL);
        List<Entity> entities = new ArrayList<>();
        getDriver().get(link);
        entity.setBusinessName("OLIVE GARDEN ITALIAN RSTRNT");
        String fullAddress = getElementText(By.id("info-link-webhead"), 1);
        String phone = getElementText(By.id("headRestPhone"), 1);
        entity.setAddress(fullAddress);
        entity.setPhone(phone);

        entity.setCreditCardsAccepted("Y");
        entity.setHasEcommerce("Y");
        List<String> hourList = getDriver().findElements(By.xpath("//li[@class = 'time']")).stream().map(WebElement::getText).collect(Collectors.toList());
        String operationHours = hourList.toString();
        entity.setOperationHours(operationHours);
        entity.setCompanyWebsite(link);
        entity.setFacebookURL("https://www.facebook.com/OliveGarden");
        entity.setInstagramURL("https://www.instagram.com/OliveGarden");
        entity.setTwitterURL("https://twitter.com/olivegarden");
        entity.setYoutubeURL("https://www.youtube.com/user/olivegarden");
        entities.add(entity);
        return entities;
    }

    @Override
    public String getBaseUrl() {
        return BASE_URL;
    }
}


