package com.workfusion.wdscrapper.ready;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import com.google.common.collect.Sets;
import com.workfusion.wdscrapper.BaseTest;
import com.workfusion.wdscrapper.Conf;
import com.workfusion.wdscrapper.api.Entity;
import com.workfusion.wdscrapper.api.Input;
import com.workfusion.wdscrapper.utils.CommonUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;


public class Autozone extends BaseTest {

    private static final String BASE_URL = "http://www.autozone.com";

    @Override
    public Collection<String> collectLinks(Input input) {
        final Set<String> links = Sets.newLinkedHashSet();
        openStoreLocatorPage();
        for (String zip : input.getZipCodes()) {
            links.addAll(retry(zip, this::collectLinks));
        }
        return links;
    }

    private Set<String> collectLinks(String zip) {
        Set<String> links = new HashSet<>();
        if (isPresent(By.id("zip"), Conf.TIMEOUT)) {
            WebElement inputZipCode = getDriver().findElement(By.id("zip"));
            inputZipCode.clear();
            inputZipCode.sendKeys(zip);
            click(By.xpath("//*[@id=\"storeLocatorForm\"]/fieldset[5]/input[1]"), Conf.TIMEOUT);

            CommonUtils.sleepMillis(500);
            links.addAll(getUrls(By.xpath("//a[text()=\"Get Map and Hours\"]"), Conf.TIMEOUT));
        }
        return links;
    }

    @Override
    protected void openStoreLocatorPage() {
        getDriver().get(getBaseUrl() + "/storelocator/storeLocatorMain.jsp");
    }

    @Override
    public Set<Entity> extractEntities(String link) {
        getDriver().get(link);
        final Entity entity = new Entity(getWebsiteUrl(), getBaseUrl());
        final Set<Entity> result = Sets.newHashSet(entity);
        final String elementTextByXpath = getElementTextByXpath("//*[@id=\"storelocator-results\"]/div[1]/div");
        final String[] split = elementTextByXpath.split("\n");
        entity.setAddress(split[1] + ", " + split[2]);
        if (split.length > 2) {
            entity.setPhone(split[3]);
        }
        entity.setOperationHours(getElementText(By.xpath("//table[@class='hours-of-operation']/tbody")));

        setCommonData(entity);

        return result;
    }

    @Override
    protected void cacheCommonData() {
        getDriver().get(getBaseUrl());
        getCommonDataCache().put(HAS_ECOMMERCE, convertBoolean(isPresent(By.id("shopping-cart"), 0)));
        getCommonDataCache().put(CREDIT_CARDS_ACCEPTED, convertBoolean(true));
        getCommonDataCache().put(PAYMENT_METHODS, "3|A|D|M|S");

        getCommonDataCache().put(YOUTUBE_URL, getUrl(By.xpath("//a[contains(@href, 'youtube.com')]")));
        getCommonDataCache().put(FACEBOOK_URL, getUrl(By.xpath("//a[contains(@href, 'facebook.com')]")));
        getCommonDataCache().put(TWITTER_URL, getUrl(By.xpath("//a[contains(@href, 'twitter.com')]")));
    }

    @Override
    public String getBaseUrl() {
        return BASE_URL;
    }

}