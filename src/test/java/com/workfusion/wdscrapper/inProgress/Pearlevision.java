package com.workfusion.wdscrapper.inProgress;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.workfusion.wdscrapper.BaseTest;
import com.workfusion.wdscrapper.api.Entity;
import com.workfusion.wdscrapper.api.Input;
import org.openqa.selenium.By;

public class Pearlevision extends BaseTest {

    @Override
    public Collection<String> collectLinks(Input input) {
        Set<String> links = new HashSet<>();

        String urlPattern = getUrlPattern(input.getCountries().get(0));
        for (String zip : input.getZipCodes()) {
            links.addAll(retry(zip, (zipCode) -> collectLinks(zipCode, urlPattern)));
        }
        return links;
    }

    private Set<String> collectLinks(String zip, String urlPattern) {
        Set<String> links = new HashSet<>();
        getDriver().get(String.format(urlPattern, zip));
        if (isVisible(By.xpath("//*[@id='ErrorDivSearch']"), 0)) {
            links.addAll(getUrls(By.linkText("VIEW LOCATION DETAILS"), 2));
        }
        return links;
    }

    private String getUrlPattern(String country) {
        String urlPattern = "";
        if ("us".equalsIgnoreCase(country)) {
            urlPattern = "http://www.pearlevision.com/pv-us/eye-care-center-locator?location=%s";
        } else {
            urlPattern = "http://www.pearlevision.ca/pv-ca/schedule-eye-exam/eye-exams?location=%s";
        }
        return urlPattern;
    }

    @Override
    public List<Entity> extractEntities(String link) {
        getDriver().get(link);
        List<Entity> entities = new ArrayList<>();

        Entity entity = new Entity(getWebsiteUrl(), link);
        entity.setBusinessName("PEARLE VISION");
        String phone = getElementText(By.xpath("//div[@class='storeInfo']/p/span[@class='tel']"));
        entity.setPhone(phone);
        String emailText = getElementText(By.xpath("//div[@class='storeInfo']/p/a[contains(., 'Email Us')]"));
        entity.setCompanyEmailAddress(getUrl(By.xpath("//div[@class='storeInfo']/p/a[contains(., 'Email Us')]")));
        entity.setCompanyWebsite(getBaseUrl());
        String storeInfo = getElementText(By.xpath("//div[@class='storeInfo']/p"));
        entity.setAddress(storeInfo.replace(emailText, "").replace(phone, "").trim());
        String hours = getElementText(By.xpath("//div[@class='hours']"));
        entity.setOperationHours(hours.replace("Store Hours", "").trim());

        setCommonData(entity);

        entities.add(entity);
        return entities;
    }

    @Override
    protected void cacheCommonData() {
        getDriver().get(getBaseUrl());
        getCommonDataCache().put(FACEBOOK_URL, getUrl(By.xpath("//a[contains(@href, 'facebook.com')]")));
        getCommonDataCache().put(TWITTER_URL, getUrl(By.xpath("//a[contains(@href, 'twitter.com')]")));
        getCommonDataCache().put(YOUTUBE_URL, getUrl(By.xpath("//a[contains(@href, 'youtube.com')]")));
    }

    @Override
    public String getBaseUrl() {
        return "http://www.pearlevision.com";
    }
}
