package com.workfusion.wdscrapper.inProgress;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.workfusion.wdscrapper.BaseTest;
import com.workfusion.wdscrapper.Conf;
import com.workfusion.wdscrapper.api.Entity;
import com.workfusion.wdscrapper.api.Input;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class Only99 extends BaseTest {

    @Override
    public Collection<String> collectLinks(Input input) {
        Set<String> links = new HashSet<>();

        getDriver().get(getBaseUrl());
        for (String zip : input.getZipCodes()) {
            //links.addAll(retry("93710", this::collectLinks));
            links.addAll(retry(zip, this::collectLinks));
        }
        return links;
    }

    private Set<String> collectLinks(String zip) {
        Set<String> links = new HashSet<>();
        WebElement inputPostalCode = getDriver().findElement(By.id("address"));
        inputPostalCode.clear();
        inputPostalCode.sendKeys(zip);
        click(By.id("addressLink"), Conf.TIMEOUT);
        waitForAjaxDone();

        links.addAll(getUrls(By.linkText("See details"), 2));
        return links;
    }

    @Override
    public List<Entity> extractEntities(String link) {
        getDriver().get(link);

        Entity entity = new Entity(getWebsiteUrl(), link);
        entity.setBusinessName("99 CENTS ONLY STORES");
        entity.setAddress(getElementText(By.xpath("//div[@class='address']")));
        entity.setOperationHours(getElementText(By.xpath("//span[contains(@class, 'openingHours')]")));
        setCommonData(entity);
        return Collections.singletonList(entity);
    }

    @Override
    protected void cacheCommonData() {
        getDriver().get(getBaseUrl());
        getCommonDataCache().put(INSTAGRAM_URL, getUrl(By.xpath("//a[contains(@href, 'instagram.com')]")));
        getCommonDataCache().put(YOUTUBE_URL, getUrl(By.xpath("//a[contains(@href, 'youtube.com')]")));
        getCommonDataCache().put(FACEBOOK_URL, getUrl(By.xpath("//a[contains(@href, 'facebook.com')]")));
        getCommonDataCache().put(PINTEREST_URL, getUrl(By.xpath("//a[contains(@href, 'pinterest.com')]")));
        getCommonDataCache().put(TWITTER_URL, getUrl(By.xpath("//a[contains(@href, 'twitter.com')]")));
    }

    @Override
    public String getBaseUrl() {
        return "http://99only.com";
    }
}
