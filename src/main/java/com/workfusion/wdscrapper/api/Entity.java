package com.workfusion.wdscrapper.api;

/**
 * User: Artyom Strok
 * Date: 11/16/15
 * Time: 12:39 PM
 */
public class Entity {
    /**
     *  Url of website
     */
    private String websiteUrl;
    /**
     * Url of web page that is used to populate current object
     */
    private final String originalURL;
    /**
     * The name under which a sole trader, partnership, limited liability partnership, or company carries on business.
     */
    private String businessName;
    /**
     * Address of company
     */
    private String address;
    /**
     * Phone of company
     */
    private String phone;
    /**
     * Fax of company
     */
    private String fax;
    /**
     * Website of company
     */
    private String companyWebsite;
    /**
     * Email address of company
     */
    private String companyEmailAddress;
    /**
     * The name of person to contact with in order to ask any questions
     */
    private String primaryContactName;
    /**
     * The title of primary contact e.g. "Sr.", "Fr." "Rev" and etc
     */
    private String primaryContactTitle;
    /**
     * The professional suffix of primary contact e.g. "Associate", "Director", "Coordinator", "Executive Director" and etc.
     */
    private String professionalSuffix;
    /**
     * The phone number of primary contact
     */
    private String contactPhoneNumber;
    /**
     * The email address of primary contact
     */
    private String contactEmailAddress;
    /**
     * Set of credit cards that accepted for payment
     */
    private String creditCardsAccepted;
    /**
     * The ability to make online order
     */
    private String hasEcommerce;
    /**
     * Hours of service or working hours
     */
    private String operationHours;
    /**
     * Payment methods are the ways in which customers pay for services or goods
     */
    private String paymentMethods;
    /**
     * Telephone number that is billed for all arriving calls instead of incurring charges to the originating telephone subscriber
     */
    private String tollFreePhone;
    /**
     * Define a date of establishment or founding of company
     */
    private String yearEstablished;
    /**
     * URL of company in Facebook
     */
    private String facebookURL;
    /**
     * URL of company in Google-Plus
     */
    private String googleplusURL;
    /**
     * URL of company in Instagram
     */
    private String instagramURL;
    /**
     * URL of company in LinkedIn
     */
    private String linkedinURL;
    /**
     * URL of company in Pinterest
     */
    private String pinterestURL;
    /**
     * URL of company in Twitter
     */
    private String twitterURL;
    /**
     * URL of company in YouTube
     */
    private String youtubeURL;
    /**
     * URL of company in Yelp
     */
    private String yelpURL;
    /**
     * Tumblr URL (Attr 10517)
     */
    private String tumblrURL;

    private String atm;
    private String deliveryAvailable;
    private String hotelsExerciseFacility;
    private String hotelsGuestLaundry;
    private String hotelsPetFriendly;
    private String hotelsValetParking;
    private String languagesSpoken;
    private String licensedBonded;
    private String priceRange;
    private String reservations;
    private String restaurantsHappyHour;
    private String open24Hours;
    private String carryout;
    private String dinein;
    private String cuisineDescription;
    private String wheelchairAccessible;
    private String catering;
    private String diesel;
    private String carWash;
    private String propane;
    private String playArea;
    private String wifi;
    private String acceptedInsurances;
    private String acceptingNewPatients;
    private String medicareAccepted;
    private String medicareidAccepted;
    private String graduationYear;
    private String hospitalAffiliation;
    private String boardCertified;
    private String ageOfProfessional;
    private String languagesSpokes;
    private String residenceHospital;
    private String residencyYearYYYY;
    private String medicalSchoolAttended;
    private String specialties;
    private String boardCertifications;
    private String musicMinistry;
    private String churchSize;
    private String averageAttendance;

    public Entity(String websiteUrl, String originalURL) {
        this.websiteUrl = websiteUrl;
        this.originalURL = originalURL;
    }

    public String getOriginalURL() {
        return originalURL;
    }

    public String getWebsiteUrl() {
        return websiteUrl;
    }

    public void setWebsiteUrl(String websiteUrl) {
        this.websiteUrl = websiteUrl;
    }

    public String getBusinessName() {
        return businessName;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getCompanyWebsite() {
        return companyWebsite;
    }

    public void setCompanyWebsite(String companyWebsite) {
        this.companyWebsite = companyWebsite;
    }

    public String getCompanyEmailAddress() {
        return companyEmailAddress;
    }

    public void setCompanyEmailAddress(String companyEmailAddress) {
        this.companyEmailAddress = companyEmailAddress;
    }

    public String getPrimaryContactName() {
        return primaryContactName;
    }

    public void setPrimaryContactName(String primaryContactName) {
        this.primaryContactName = primaryContactName;
    }

    public String getPrimaryContactTitle() {
        return primaryContactTitle;
    }

    public void setPrimaryContactTitle(String primaryContactTitle) {
        this.primaryContactTitle = primaryContactTitle;
    }

    public String getProfessionalSuffix() {
        return professionalSuffix;
    }

    public void setProfessionalSuffix(String professionalSuffix) {
        this.professionalSuffix = professionalSuffix;
    }

    public String getContactPhoneNumber() {
        return contactPhoneNumber;
    }

    public void setContactPhoneNumber(String contactPhoneNumber) {
        this.contactPhoneNumber = contactPhoneNumber;
    }

    public String getContactEmailAddress() {
        return contactEmailAddress;
    }

    public void setContactEmailAddress(String contactEmailAddress) {
        this.contactEmailAddress = contactEmailAddress;
    }

    public String getCreditCardsAccepted() {
        return creditCardsAccepted;
    }

    public void setCreditCardsAccepted(String creditCardsAccepted) {
        this.creditCardsAccepted = creditCardsAccepted;
    }

    public String getHasEcommerce() {
        return hasEcommerce;
    }

    public void setHasEcommerce(String hasEcommerce) {
        this.hasEcommerce = hasEcommerce;
    }

    public String getOperationHours() {
        return operationHours;
    }

    public void setOperationHours(String operationHours) {
        this.operationHours = operationHours;
    }

    public String getPaymentMethods() {
        return paymentMethods;
    }

    public void setPaymentMethods(String paymentMethods) {
        this.paymentMethods = paymentMethods;
    }

    public String getTollFreePhone() {
        return tollFreePhone;
    }

    public void setTollFreePhone(String tollFreePhone) {
        this.tollFreePhone = tollFreePhone;
    }

    public String getYearEstablished() {
        return yearEstablished;
    }

    public void setYearEstablished(String yearEstablished) {
        this.yearEstablished = yearEstablished;
    }

    public String getFacebookURL() {
        return facebookURL;
    }

    public void setFacebookURL(String facebookURL) {
        this.facebookURL = facebookURL;
    }

    public String getGoogleplusURL() {
        return googleplusURL;
    }

    public void setGoogleplusURL(String googleplusURL) {
        this.googleplusURL = googleplusURL;
    }

    public String getInstagramURL() {
        return instagramURL;
    }

    public void setInstagramURL(String instagramURL) {
        this.instagramURL = instagramURL;
    }

    public String getLinkedinURL() {
        return linkedinURL;
    }

    public void setLinkedinURL(String linkedinURL) {
        this.linkedinURL = linkedinURL;
    }

    public String getPinterestURL() {
        return pinterestURL;
    }

    public void setPinterestURL(String pinterestURL) {
        this.pinterestURL = pinterestURL;
    }

    public String getTwitterURL() {
        return twitterURL;
    }

    public void setTwitterURL(String twitterURL) {
        this.twitterURL = twitterURL;
    }

    public String getYoutubeURL() {
        return youtubeURL;
    }

    public void setYoutubeURL(String youtubeURL) {
        this.youtubeURL = youtubeURL;
    }

    public void setTumblrURL(String tumblrURL) {
        this.tumblrURL = tumblrURL;
    }

    public String getTumblrURL() {
        return tumblrURL;
    }

    public String getYelpURL() {
        return yelpURL;
    }

    public void setYelpURL(String yelpURL) {
        this.yelpURL = yelpURL;
    }

    public String getWifi() {
        return wifi;
    }

    public void setWifi(String wifi) {
        this.wifi = wifi;
    }

    public String getAtm() {
        return atm;
    }

    public void setAtm(String atm) {
        this.atm = atm;
    }

    public String getDeliveryAvailable() {
        return deliveryAvailable;
    }

    public void setDeliveryAvailable(String deliveryAvailable) {
        this.deliveryAvailable = deliveryAvailable;
    }

    public String getHotelsExerciseFacility() {
        return hotelsExerciseFacility;
    }

    public void setHotelsExerciseFacility(String hotelsExerciseFacility) {
        this.hotelsExerciseFacility = hotelsExerciseFacility;
    }

    public String getHotelsGuestLaundry() {
        return hotelsGuestLaundry;
    }

    public void setHotelsGuestLaundry(String hotelsGuestLaundry) {
        this.hotelsGuestLaundry = hotelsGuestLaundry;
    }

    public String getHotelsPetFriendly() {
        return hotelsPetFriendly;
    }

    public void setHotelsPetFriendly(String hotelsPetFriendly) {
        this.hotelsPetFriendly = hotelsPetFriendly;
    }

    public String getHotelsValetParking() {
        return hotelsValetParking;
    }

    public void setHotelsValetParking(String hotelsValetParking) {
        this.hotelsValetParking = hotelsValetParking;
    }

    public String getLanguagesSpoken() {
        return languagesSpoken;
    }

    public void setLanguagesSpoken(String languagesSpoken) {
        this.languagesSpoken = languagesSpoken;
    }

    public String getLicensedBonded() {
        return licensedBonded;
    }

    public void setLicensedBonded(String licensedBonded) {
        this.licensedBonded = licensedBonded;
    }

    public String getPriceRange() {
        return priceRange;
    }

    public void setPriceRange(String priceRange) {
        this.priceRange = priceRange;
    }

    public String getReservations() {
        return reservations;
    }

    public void setReservations(String reservations) {
        this.reservations = reservations;
    }

    public String getRestaurantsHappyHour() {
        return restaurantsHappyHour;
    }

    public void setRestaurantsHappyHour(String restaurantsHappyHour) {
        this.restaurantsHappyHour = restaurantsHappyHour;
    }

    public String getOpen24Hours() {
        return open24Hours;
    }

    public void setOpen24Hours(String open24Hours) {
        this.open24Hours = open24Hours;
    }

    public String getCarryout() {
        return carryout;
    }

    public void setCarryout(String carryout) {
        this.carryout = carryout;
    }

    public String getDinein() {
        return dinein;
    }

    public void setDinein(String dinein) {
        this.dinein = dinein;
    }

    public String getCuisineDescription() {
        return cuisineDescription;
    }

    public void setCuisineDescription(String cuisineDescription) {
        this.cuisineDescription = cuisineDescription;
    }

    public String getWheelchairAccessible() {
        return wheelchairAccessible;
    }

    public void setWheelchairAccessible(String wheelchairAccessible) {
        this.wheelchairAccessible = wheelchairAccessible;
    }

    public String getCatering() {
        return catering;
    }

    public void setCatering(String catering) {
        this.catering = catering;
    }

    public String getDiesel() {
        return diesel;
    }

    public void setDiesel(String diesel) {
        this.diesel = diesel;
    }

    public String getCarWash() {
        return carWash;
    }

    public void setCarWash(String carWash) {
        this.carWash = carWash;
    }

    public String getPropane() {
        return propane;
    }

    public void setPropane(String propane) {
        this.propane = propane;
    }

    public String getPlayArea() {
        return playArea;
    }

    public void setPlayArea(String playArea) {
        this.playArea = playArea;
    }

    public String getAcceptedInsurances() {
        return acceptedInsurances;
    }

    public void setAcceptedInsurances(String acceptedInsurances) {
        this.acceptedInsurances = acceptedInsurances;
    }

    public void setAcceptingNewPatients(String acceptingNewPatients) {
        this.acceptingNewPatients = acceptingNewPatients;
    }

    public String getAcceptingNewPatients() {
        return acceptingNewPatients;
    }

    public String getMedicareAccepted() {
        return medicareAccepted;
    }

    public void setMedicareAccepted(String medicareAccepted) {
        this.medicareAccepted = medicareAccepted;
    }

    public String getMedicareidAccepted() {
        return medicareidAccepted;
    }

    public void setMedicareidAccepted(String medicareidAccepted) {
        this.medicareidAccepted = medicareidAccepted;
    }

    public String getGraduationYear() {
        return graduationYear;
    }

    public String getHospitalAffiliation() {
        return hospitalAffiliation;
    }

    public void setHospitalAffiliation(String hospitalAffiliation) {
        this.hospitalAffiliation = hospitalAffiliation;
    }

    public void setGraduationYear(String graduationYear) {
        this.graduationYear = graduationYear;
    }

    @Override
    public String toString() {
        return "{" +
                "originalURL='" + originalURL + '\'' +
                (businessName != null ? ", businessName='" + businessName + '\'' : "") +
                (address != null ? ", address='" + address + '\'' : "") +
                (phone != null ? ", phone='" + phone + '\'' : "") +
                (fax != null ? ", fax='" + fax + '\'' : "") +
                (companyWebsite != null ? ", companyWebsite='" + companyWebsite + '\'' : "") +
                (companyEmailAddress != null ? ", companyEmailAddress='" + companyEmailAddress + '\'' : "") +
                (primaryContactName != null ? ", primaryContactName='" + primaryContactName + '\'' : "") +
                (primaryContactTitle != null ? ", primaryContactTitle='" + primaryContactTitle + '\'' : "") +
                (professionalSuffix != null ? ", professionalSuffix='" + professionalSuffix + '\'' : "") +
                (contactPhoneNumber != null ? ", contactPhoneNumber='" + contactPhoneNumber + '\'' : "") +
                (contactEmailAddress != null ? ", contactEmailAddress='" + contactEmailAddress + '\'' : "") +
                (creditCardsAccepted != null ? ", creditCardsAccepted='" + creditCardsAccepted + '\'' : "") +
                (hasEcommerce != null ? ", hasEcommerce='" + hasEcommerce + '\'' : "") +
                (operationHours != null ? ", operationHours='" + operationHours + '\'' : "") +
                (paymentMethods != null ? ", paymentMethods='" + paymentMethods + '\'' : "") +
                (tollFreePhone != null ? ", tollFreePhone='" + tollFreePhone + '\'' : "") +
                (yearEstablished != null ? ", yearEstablished='" + yearEstablished + '\'' : "") +
                (facebookURL != null ? ", facebookURL='" + facebookURL + '\'' : "") +
                (googleplusURL != null ? ", googleplusURL='" + googleplusURL + '\'' : "") +
                (instagramURL != null ? ", instagramURL='" + instagramURL + '\'' : "") +
                (linkedinURL != null ? ", linkedinURL='" + linkedinURL + '\'' : "") +
                (pinterestURL != null ? ", pinterestURL='" + pinterestURL + '\'' : "") +
                (twitterURL != null ? ", twitterURL='" + twitterURL + '\'' : "") +
                (youtubeURL != null ? ", youtubeURL='" + youtubeURL + '\'' : "") +
                (yelpURL != null ? ", yelpURL='" + yelpURL + '\'' : "") +

                (atm != null ? ", atm='" + atm + '\'' : "") +
                (deliveryAvailable != null ? ", deliveryAvailable='" + deliveryAvailable + '\'' : "") +
                (hotelsExerciseFacility != null ? ", hotelsExerciseFacility='" + hotelsExerciseFacility + '\'' : "") +
                (hotelsGuestLaundry != null ? ", hotelsGuestLaundry='" + hotelsGuestLaundry + '\'' : "") +
                (hotelsPetFriendly != null ? ", hotelsPetFriendly='" + hotelsPetFriendly + '\'' : "") +
                (hotelsValetParking != null ? ", hotelsValetParking='" + hotelsValetParking + '\'' : "") +
                (languagesSpoken != null ? ", languagesSpoken='" + languagesSpoken + '\'' : "") +
                (licensedBonded != null ? ", licensedBonded='" + licensedBonded + '\'' : "") +
                (priceRange != null ? ", priceRange='" + priceRange + '\'' : "") +
                (reservations != null ? ", reservations='" + reservations + '\'' : "") +
                (restaurantsHappyHour != null ? ", restaurantsHappyHour='" + restaurantsHappyHour + '\'' : "") +
                (open24Hours != null ? ", open24Hours='" + open24Hours + '\'' : "") +
                (carryout != null ? ", carryout='" + carryout + '\'' : "") +
                (dinein != null ? ", dinein='" + dinein + '\'' : "") +
                (cuisineDescription != null ? ", cuisineDescription='" + cuisineDescription + '\'' : "") +
                (wheelchairAccessible != null ? ", wheelchairAccessible='" + wheelchairAccessible + '\'' : "") +
                (catering != null ? ", catering='" + catering + '\'' : "") +
                (diesel != null ? ", diesel='" + diesel + '\'' : "") +
                (carWash != null ? ", carWash='" + carWash + '\'' : "") +
                (propane != null ? ", propane='" + propane + '\'' : "") +
                (playArea != null ? ", playArea='" + playArea + '\'' : "") +
                (wifi != null ? ", wifi='" + wifi + '\'' : "") +
                "}\n";
    }

    public void setBoardCertified(String boardCertified) {
        this.boardCertified = boardCertified;
    }

    public String getBoardCertified() {
        return boardCertified;
    }


    public void setAgeOfProfessional(String ageOfProfessional) {
        this.ageOfProfessional = ageOfProfessional;
    }

    public String getAgeOfProfessional() {
        return ageOfProfessional;
    }

    public void setLanguagesSpokes(String languagesSpokes) {
        this.languagesSpokes = languagesSpokes;
    }

    public String getLanguagesSpokes() {
        return languagesSpokes;
    }

    public void setResidencyHospital(String residenceHospital) {
        this.residenceHospital = residenceHospital;
    }

    public String getResidenceHospital() {
        return residenceHospital;
    }

    public void setResidencyYearYYYY(String residencyYearYYYY) {
        this.residencyYearYYYY = residencyYearYYYY;
    }

    public String getResidencyYearYYYY() {
        return residencyYearYYYY;
    }

    public void setMedicalSchoolAttended(String medicalSchoolAttended) {
        this.medicalSchoolAttended = medicalSchoolAttended;
    }

    public String getMedicalSchoolAttended() {
        return medicalSchoolAttended;
    }

    public void setGraduationYearYYYY(String graduationYearYYYY) {
        this.medicalSchoolAttended = graduationYearYYYY;
    }

    public void setSpecialties(String specialties) {
        this.specialties = specialties;
    }

    public String getSpecialties() {
        return specialties;
    }

    public void setBoardCertifications(String boardCertifications) {
        this.boardCertifications = boardCertifications;
    }

    public String getBoardCertifications() {
        return boardCertifications;
    }

    public String getMusicMinistry() {
        return musicMinistry;
    }

    public void setMusicMinistry(String musicMinistry) {
        this.musicMinistry = musicMinistry;
    }

    public String getChurchSize() {
        return churchSize;
    }

    public void setChurchSize(String churchSize) {
        this.churchSize = churchSize;
    }

    public String getAverageAttendance() {
        return averageAttendance;
    }

    public void setAverageAttendance(String averageAttendance) {
        this.averageAttendance = averageAttendance;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (!(o instanceof Entity))
            return false;

        Entity entity = (Entity) o;

        if (originalURL != null ? !originalURL.equals(entity.originalURL) : entity.originalURL != null)
            return false;
        if (businessName != null ? !businessName.equals(entity.businessName) : entity.businessName != null)
            return false;
        if (address != null ? !address.equals(entity.address) : entity.address != null)
            return false;
        if (phone != null ? !phone.equals(entity.phone) : entity.phone != null)
            return false;
        if (fax != null ? !fax.equals(entity.fax) : entity.fax != null)
            return false;
        if (companyWebsite != null ? !companyWebsite.equals(entity.companyWebsite) : entity.companyWebsite != null)
            return false;
        if (companyEmailAddress != null ? !companyEmailAddress.equals(entity.companyEmailAddress) : entity.companyEmailAddress != null)
            return false;
        if (primaryContactName != null ? !primaryContactName.equals(entity.primaryContactName) : entity.primaryContactName != null)
            return false;
        if (primaryContactTitle != null ? !primaryContactTitle.equals(entity.primaryContactTitle) : entity.primaryContactTitle != null)
            return false;
        if (professionalSuffix != null ? !professionalSuffix.equals(entity.professionalSuffix) : entity.professionalSuffix != null)
            return false;
        if (contactPhoneNumber != null ? !contactPhoneNumber.equals(entity.contactPhoneNumber) : entity.contactPhoneNumber != null)
            return false;
        if (contactEmailAddress != null ? !contactEmailAddress.equals(entity.contactEmailAddress) : entity.contactEmailAddress != null)
            return false;
        if (creditCardsAccepted != null ? !creditCardsAccepted.equals(entity.creditCardsAccepted) : entity.creditCardsAccepted != null)
            return false;
        if (hasEcommerce != null ? !hasEcommerce.equals(entity.hasEcommerce) : entity.hasEcommerce != null)
            return false;
        if (operationHours != null ? !operationHours.equals(entity.operationHours) : entity.operationHours != null)
            return false;
        if (paymentMethods != null ? !paymentMethods.equals(entity.paymentMethods) : entity.paymentMethods != null)
            return false;
        if (tollFreePhone != null ? !tollFreePhone.equals(entity.tollFreePhone) : entity.tollFreePhone != null)
            return false;
        if (yearEstablished != null ? !yearEstablished.equals(entity.yearEstablished) : entity.yearEstablished != null)
            return false;
        if (facebookURL != null ? !facebookURL.equals(entity.facebookURL) : entity.facebookURL != null)
            return false;
        if (googleplusURL != null ? !googleplusURL.equals(entity.googleplusURL) : entity.googleplusURL != null)
            return false;
        if (instagramURL != null ? !instagramURL.equals(entity.instagramURL) : entity.instagramURL != null)
            return false;
        if (linkedinURL != null ? !linkedinURL.equals(entity.linkedinURL) : entity.linkedinURL != null)
            return false;
        if (pinterestURL != null ? !pinterestURL.equals(entity.pinterestURL) : entity.pinterestURL != null)
            return false;
        if (twitterURL != null ? !twitterURL.equals(entity.twitterURL) : entity.twitterURL != null)
            return false;
        if (youtubeURL != null ? !youtubeURL.equals(entity.youtubeURL) : entity.youtubeURL != null)
            return false;
        if (yelpURL != null ? !yelpURL.equals(entity.yelpURL) : entity.yelpURL != null)
            return false;
        if (tumblrURL != null ? !tumblrURL.equals(entity.tumblrURL) : entity.tumblrURL != null)
            return false;
        if (atm != null ? !atm.equals(entity.atm) : entity.atm != null)
            return false;
        if (deliveryAvailable != null ? !deliveryAvailable.equals(entity.deliveryAvailable) : entity.deliveryAvailable != null)
            return false;
        if (hotelsExerciseFacility != null ? !hotelsExerciseFacility.equals(entity.hotelsExerciseFacility) : entity.hotelsExerciseFacility != null)
            return false;
        if (hotelsGuestLaundry != null ? !hotelsGuestLaundry.equals(entity.hotelsGuestLaundry) : entity.hotelsGuestLaundry != null)
            return false;
        if (hotelsPetFriendly != null ? !hotelsPetFriendly.equals(entity.hotelsPetFriendly) : entity.hotelsPetFriendly != null)
            return false;
        if (hotelsValetParking != null ? !hotelsValetParking.equals(entity.hotelsValetParking) : entity.hotelsValetParking != null)
            return false;
        if (languagesSpoken != null ? !languagesSpoken.equals(entity.languagesSpoken) : entity.languagesSpoken != null)
            return false;
        if (licensedBonded != null ? !licensedBonded.equals(entity.licensedBonded) : entity.licensedBonded != null)
            return false;
        if (priceRange != null ? !priceRange.equals(entity.priceRange) : entity.priceRange != null)
            return false;
        if (reservations != null ? !reservations.equals(entity.reservations) : entity.reservations != null)
            return false;
        if (restaurantsHappyHour != null ? !restaurantsHappyHour.equals(entity.restaurantsHappyHour) : entity.restaurantsHappyHour != null)
            return false;
        if (open24Hours != null ? !open24Hours.equals(entity.open24Hours) : entity.open24Hours != null)
            return false;
        if (carryout != null ? !carryout.equals(entity.carryout) : entity.carryout != null)
            return false;
        if (dinein != null ? !dinein.equals(entity.dinein) : entity.dinein != null)
            return false;
        if (cuisineDescription != null ? !cuisineDescription.equals(entity.cuisineDescription) : entity.cuisineDescription != null)
            return false;
        if (wheelchairAccessible != null ? !wheelchairAccessible.equals(entity.wheelchairAccessible) : entity.wheelchairAccessible != null)
            return false;
        if (catering != null ? !catering.equals(entity.catering) : entity.catering != null)
            return false;
        if (diesel != null ? !diesel.equals(entity.diesel) : entity.diesel != null)
            return false;
        if (carWash != null ? !carWash.equals(entity.carWash) : entity.carWash != null)
            return false;
        if (propane != null ? !propane.equals(entity.propane) : entity.propane != null)
            return false;
        if (playArea != null ? !playArea.equals(entity.playArea) : entity.playArea != null)
            return false;
        if (wifi != null ? !wifi.equals(entity.wifi) : entity.wifi != null)
            return false;
        if (acceptedInsurances != null ? !acceptedInsurances.equals(entity.acceptedInsurances) : entity.acceptedInsurances != null)
            return false;
        if (acceptingNewPatients != null ? !acceptingNewPatients.equals(entity.acceptingNewPatients) : entity.acceptingNewPatients != null)
            return false;
        if (medicareAccepted != null ? !medicareAccepted.equals(entity.medicareAccepted) : entity.medicareAccepted != null)
            return false;
        if (medicareidAccepted != null ? !medicareidAccepted.equals(entity.medicareidAccepted) : entity.medicareidAccepted != null)
            return false;
        if (graduationYear != null ? !graduationYear.equals(entity.graduationYear) : entity.graduationYear != null)
            return false;
        if (hospitalAffiliation != null ? !hospitalAffiliation.equals(entity.hospitalAffiliation) : entity.hospitalAffiliation != null)
            return false;
        if (boardCertified != null ? !boardCertified.equals(entity.boardCertified) : entity.boardCertified != null)
            return false;
        if (ageOfProfessional != null ? !ageOfProfessional.equals(entity.ageOfProfessional) : entity.ageOfProfessional != null)
            return false;
        if (languagesSpokes != null ? !languagesSpokes.equals(entity.languagesSpokes) : entity.languagesSpokes != null)
            return false;
        if (residenceHospital != null ? !residenceHospital.equals(entity.residenceHospital) : entity.residenceHospital != null)
            return false;
        if (residencyYearYYYY != null ? !residencyYearYYYY.equals(entity.residencyYearYYYY) : entity.residencyYearYYYY != null)
            return false;
        if (medicalSchoolAttended != null ? !medicalSchoolAttended.equals(entity.medicalSchoolAttended) : entity.medicalSchoolAttended != null)
            return false;
        if (specialties != null ? !specialties.equals(entity.specialties) : entity.specialties != null)
            return false;
        if (musicMinistry != null ? !musicMinistry.equals(entity.musicMinistry) : entity.musicMinistry != null)
            return false;
        if (churchSize != null ? !churchSize.equals(entity.churchSize) : entity.churchSize != null)
            return false;
        if (averageAttendance != null ? !averageAttendance.equals(entity.averageAttendance) : entity.averageAttendance != null)
            return false;
        return !(boardCertifications != null ? !boardCertifications.equals(entity.boardCertifications) : entity.boardCertifications != null);

    }

    @Override
    public int hashCode() {
        int result = originalURL != null ? originalURL.hashCode() : 0;
        result = 31 * result + (businessName != null ? businessName.hashCode() : 0);
        result = 31 * result + (address != null ? address.hashCode() : 0);
        result = 31 * result + (phone != null ? phone.hashCode() : 0);
        result = 31 * result + (fax != null ? fax.hashCode() : 0);
        result = 31 * result + (companyWebsite != null ? companyWebsite.hashCode() : 0);
        result = 31 * result + (companyEmailAddress != null ? companyEmailAddress.hashCode() : 0);
        result = 31 * result + (primaryContactName != null ? primaryContactName.hashCode() : 0);
        result = 31 * result + (primaryContactTitle != null ? primaryContactTitle.hashCode() : 0);
        result = 31 * result + (professionalSuffix != null ? professionalSuffix.hashCode() : 0);
        result = 31 * result + (contactPhoneNumber != null ? contactPhoneNumber.hashCode() : 0);
        result = 31 * result + (contactEmailAddress != null ? contactEmailAddress.hashCode() : 0);
        result = 31 * result + (creditCardsAccepted != null ? creditCardsAccepted.hashCode() : 0);
        result = 31 * result + (hasEcommerce != null ? hasEcommerce.hashCode() : 0);
        result = 31 * result + (operationHours != null ? operationHours.hashCode() : 0);
        result = 31 * result + (paymentMethods != null ? paymentMethods.hashCode() : 0);
        result = 31 * result + (tollFreePhone != null ? tollFreePhone.hashCode() : 0);
        result = 31 * result + (yearEstablished != null ? yearEstablished.hashCode() : 0);
        result = 31 * result + (facebookURL != null ? facebookURL.hashCode() : 0);
        result = 31 * result + (googleplusURL != null ? googleplusURL.hashCode() : 0);
        result = 31 * result + (instagramURL != null ? instagramURL.hashCode() : 0);
        result = 31 * result + (linkedinURL != null ? linkedinURL.hashCode() : 0);
        result = 31 * result + (pinterestURL != null ? pinterestURL.hashCode() : 0);
        result = 31 * result + (twitterURL != null ? twitterURL.hashCode() : 0);
        result = 31 * result + (youtubeURL != null ? youtubeURL.hashCode() : 0);
        result = 31 * result + (yelpURL != null ? yelpURL.hashCode() : 0);
        result = 31 * result + (tumblrURL != null ? tumblrURL.hashCode() : 0);
        result = 31 * result + (atm != null ? atm.hashCode() : 0);
        result = 31 * result + (deliveryAvailable != null ? deliveryAvailable.hashCode() : 0);
        result = 31 * result + (hotelsExerciseFacility != null ? hotelsExerciseFacility.hashCode() : 0);
        result = 31 * result + (hotelsGuestLaundry != null ? hotelsGuestLaundry.hashCode() : 0);
        result = 31 * result + (hotelsPetFriendly != null ? hotelsPetFriendly.hashCode() : 0);
        result = 31 * result + (hotelsValetParking != null ? hotelsValetParking.hashCode() : 0);
        result = 31 * result + (languagesSpoken != null ? languagesSpoken.hashCode() : 0);
        result = 31 * result + (licensedBonded != null ? licensedBonded.hashCode() : 0);
        result = 31 * result + (priceRange != null ? priceRange.hashCode() : 0);
        result = 31 * result + (reservations != null ? reservations.hashCode() : 0);
        result = 31 * result + (restaurantsHappyHour != null ? restaurantsHappyHour.hashCode() : 0);
        result = 31 * result + (open24Hours != null ? open24Hours.hashCode() : 0);
        result = 31 * result + (carryout != null ? carryout.hashCode() : 0);
        result = 31 * result + (dinein != null ? dinein.hashCode() : 0);
        result = 31 * result + (cuisineDescription != null ? cuisineDescription.hashCode() : 0);
        result = 31 * result + (wheelchairAccessible != null ? wheelchairAccessible.hashCode() : 0);
        result = 31 * result + (catering != null ? catering.hashCode() : 0);
        result = 31 * result + (diesel != null ? diesel.hashCode() : 0);
        result = 31 * result + (carWash != null ? carWash.hashCode() : 0);
        result = 31 * result + (propane != null ? propane.hashCode() : 0);
        result = 31 * result + (playArea != null ? playArea.hashCode() : 0);
        result = 31 * result + (wifi != null ? wifi.hashCode() : 0);
        result = 31 * result + (acceptedInsurances != null ? acceptedInsurances.hashCode() : 0);
        result = 31 * result + (acceptingNewPatients != null ? acceptingNewPatients.hashCode() : 0);
        result = 31 * result + (medicareAccepted != null ? medicareAccepted.hashCode() : 0);
        result = 31 * result + (medicareidAccepted != null ? medicareidAccepted.hashCode() : 0);
        result = 31 * result + (graduationYear != null ? graduationYear.hashCode() : 0);
        result = 31 * result + (hospitalAffiliation != null ? hospitalAffiliation.hashCode() : 0);
        result = 31 * result + (boardCertified != null ? boardCertified.hashCode() : 0);
        result = 31 * result + (ageOfProfessional != null ? ageOfProfessional.hashCode() : 0);
        result = 31 * result + (languagesSpokes != null ? languagesSpokes.hashCode() : 0);
        result = 31 * result + (residenceHospital != null ? residenceHospital.hashCode() : 0);
        result = 31 * result + (residencyYearYYYY != null ? residencyYearYYYY.hashCode() : 0);
        result = 31 * result + (medicalSchoolAttended != null ? medicalSchoolAttended.hashCode() : 0);
        result = 31 * result + (specialties != null ? specialties.hashCode() : 0);
        result = 31 * result + (boardCertifications != null ? boardCertifications.hashCode() : 0);
        result = 31 * result + (musicMinistry != null ? musicMinistry.hashCode() : 0);
        result = 31 * result + (churchSize != null ? churchSize.hashCode() : 0);
        result = 31 * result + (averageAttendance != null ? averageAttendance.hashCode() : 0);
        return result;
    }
}

