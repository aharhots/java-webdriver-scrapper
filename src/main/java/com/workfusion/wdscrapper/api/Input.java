package com.workfusion.wdscrapper.api;

import java.util.List;

/**
 * @author Maksim Turchyn
 */
public class Input {

    private List<String> countries;
    private List<String> states;
    private List<String> zipCodes;

    private List<String> links;

    public Input() {
    }

    public Input(List<String> states) {
        this.states = states;
    }

    public Input(List<String> countries, List<String> states, List<String> zipCodes) {
        this.zipCodes = zipCodes;
        this.countries = countries;
        this.states = states;
    }

    public List<String> getZipCodes() {
        return zipCodes;
    }

    public void setZipCodes(List<String> zipCodes) {
        this.zipCodes = zipCodes;
    }

    public List<String> getCountries() {
        return countries;
    }

    public void setCountries(List<String> countries) {
        this.countries = countries;
    }

    public List<String> getStates() {
        return states;
    }

    public void setStates(List<String> states) {
        this.states = states;
    }

    public List<String> getLinks() {
        return links;
    }

    public void setLinks(List<String> links) {
        this.links = links;
    }

    @Override
    public String toString() {
        return "Input {" +
                "\ncountries=" + countries +
                ", \nstates=" + states +
                ", \nzipCodes=" + zipCodes +
                ", \nlinks=" + links +
                "\n}";
    }
}
