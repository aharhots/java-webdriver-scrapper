package com.workfusion.wdscrapper.api;

import java.util.Collection;
import java.util.List;

/**
 * User: Artyom Strok
 * Date: 11/16/15
 * Time: 2:10 PM
 */
public interface IEntityExtractor {

    Collection<String> collectLinks(Input input);

    Collection<Entity> extractEntities(String link);

    String getBaseUrl();

}
