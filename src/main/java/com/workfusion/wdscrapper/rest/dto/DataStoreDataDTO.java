package com.workfusion.wdscrapper.rest.dto;

import java.util.Map;


public class DataStoreDataDTO {

    private Map<String, DataStoreColumnType> columns;

    private String originalCampaignUuid;

    private Long automationUseCaseId;

    public Map<String, DataStoreColumnType> getColumns() {
        return columns;
    }

    public void setColumns(Map<String, DataStoreColumnType> columns) {
        this.columns = columns;
    }

    public String getOriginalCampaignUuid() {
        return originalCampaignUuid;
    }

    public void setOriginalCampaignUuid(String originalCampaignUuid) {
        this.originalCampaignUuid = originalCampaignUuid;
    }

    public Long getAutomationUseCaseId() {
        return automationUseCaseId;
    }

    public void setAutomationUseCaseId(Long automationUseCaseId) {
        this.automationUseCaseId = automationUseCaseId;
    }


}
