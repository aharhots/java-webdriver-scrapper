package com.workfusion.wdscrapper.rest.dto;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;

public class RestResponse<T> {

    private ResponseStatus responseStatus;

    private T body;

    private List<Error> errors = new ArrayList<Error>();

    public RestResponse() {
    }

    public RestResponse(ResponseStatus responseStatus) {
        this.responseStatus = responseStatus;
    }

    public RestResponse(Error error) {
        this.responseStatus = ResponseStatus.FAILURE;
        this.errors.add(error);
    }

    public ResponseStatus getResponseStatus() {
        return responseStatus;
    }

    public void setResponseStatus(ResponseStatus responseStatus) {
        this.responseStatus = responseStatus;
    }

    public T getBody() {
        return body;
    }

    public void setBody(T body) {
        this.body = body;
    }

    public List<Error> getErrors() {
        return errors;
    }

    public void setErrors(List<Error> errors) {
        this.errors = errors;
    }

    public boolean hasErrors() {
        return CollectionUtils.isNotEmpty(errors);
    }

}
