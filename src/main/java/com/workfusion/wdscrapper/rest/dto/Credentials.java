package com.workfusion.wdscrapper.rest.dto;

import java.io.Serializable;

public class Credentials implements Serializable {

    private static final long serialVersionUID = -3951884136327930011L;

    private String username;

    private String password;
    private String host;

    public Credentials() {
    }

    public Credentials(String username, String password, String host) {
        this.username = username;
        this.password = password;
        this.host = host;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }
}
