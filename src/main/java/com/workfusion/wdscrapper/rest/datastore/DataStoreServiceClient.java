package com.workfusion.wdscrapper.rest.datastore;

import java.lang.reflect.Field;
import java.lang.reflect.Type;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.common.collect.Maps;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.workfusion.wdscrapper.api.Entity;
import com.workfusion.wdscrapper.rest.dto.Credentials;
import com.workfusion.wdscrapper.rest.dto.DataStoreColumnType;
import com.workfusion.wdscrapper.rest.dto.DataStoreDataDTO;
import com.workfusion.wdscrapper.rest.dto.Error;
import com.workfusion.wdscrapper.rest.dto.RestResponse;
import com.workfusion.wdscrapper.rest.dto.RowDataDTO;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.client.utils.URIBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

public class DataStoreServiceClient {

    private static final String CREATE_OR_UPDATE_METHOD = "/createOrUpdate";
    private static final String DATASTORE_SERVICE_URL = "/soap/datastore";
    private static final Gson GSON = new Gson();

    private static final String INSERT_METHOD = "/insert";

    // temporary properties
    private static final String username = "postgress";
    private static final String password = "";
    private static final String workfusionHost = "";
    private static final String workfusionUrl = "/mturk-web";

    private static final RestTemplate REST_TEMPLATE = new RestTemplate();
    private static final Type TYPE_MAP_STRING_STRING = new TypeToken<Map<String, String>>() {}.getType();
    private static final Logger logger = LoggerFactory.getLogger(DataStoreServiceClient.class);
    private static volatile DataStoreServiceClient instance;
    private Credentials credentials;

    private DataStoreServiceClient() {
        credentials = new Credentials(username, password, workfusionHost);
    }

    public static DataStoreServiceClient getInstance() {
        DataStoreServiceClient localInstance = instance;
        if (localInstance == null) {
            synchronized (DataStoreServiceClient.class) {
                localInstance = instance;
                if (localInstance == null) {
                    instance = localInstance = new DataStoreServiceClient();
                }
            }
        }
        return localInstance;
    }

    public void insertEntities(String dataStoreName, List<Entity> entities) {
        createOrUpdate(dataStoreName, createTextColumns(getHeaders()));
        entities.forEach((entity) -> insertEntity(dataStoreName, entity));
    }

    private Long insertEntity(String dataStoreName, Entity entity) {
        final RowDataDTO rowData = entityToRawData(entity, getHeaders());
        return insertRow(dataStoreName, rowData);
    }

    public Long insertRow(String dataStoreName, RowDataDTO rowData) {
        Long response = null;
        try {
            final HttpEntity<RowDataDTO> httpEntity = createHttpEntity(rowData, MediaType.APPLICATION_JSON, MediaType.APPLICATION_JSON);
            final URI url = buildRequestUrl(credentials, dataStoreName + INSERT_METHOD);
            final ResponseEntity<RestResponse<Long>> resp = REST_TEMPLATE.exchange(url, HttpMethod.POST, httpEntity,
                    new ParameterizedTypeReference<RestResponse<Long>>() {
                    });
            final RestResponse<Long> body = resp.getBody();
            if (!body.hasErrors()) {
                response = body.getBody();
            } else {
                logErrors(body);
            }
        } catch (URISyntaxException e) {
            logger.error("Failed to build request url", e);
            e.printStackTrace();
        }
        return response;
    }

    public boolean createOrUpdate(String dataStoreName, Map<String, DataStoreColumnType> columns) {
        try {
            final DataStoreDataDTO dataStoreData = new DataStoreDataDTO();
            dataStoreData.setColumns(columns);

            final HttpEntity<DataStoreDataDTO> httpEntity = createHttpEntity(dataStoreData, MediaType.APPLICATION_JSON,
                    MediaType.APPLICATION_JSON);

            final URI url = buildRequestUrl(credentials, dataStoreName + CREATE_OR_UPDATE_METHOD);
            final ResponseEntity<RestResponse<Boolean>> resp = REST_TEMPLATE.exchange(url, HttpMethod.POST, httpEntity,
                    new ParameterizedTypeReference<RestResponse<Boolean>>() {
                    });
            final RestResponse<Boolean> body = resp.getBody();
            if (!body.hasErrors()) {
                return true;
            } else {
                logErrors(body);
            }
        } catch (URISyntaxException e) {
            logger.error("Failed to build request url", e);
            e.printStackTrace();
        }
        return false;
    }

    private String[] getHeaders() {
        final Field[] entityFields = Entity.class.getDeclaredFields();
        final String[] headers = new String[entityFields.length];
        for (int i = 0; i < entityFields.length; i++) {
            headers[i] = underscoreHeader(entityFields[i].getName());
        }
        return headers;
    }

    private String underscoreHeader(String header) {
        return header.replaceAll("([A-Z]+)", "_$1").toLowerCase();
    }

    private Map<String, DataStoreColumnType> createTextColumns(String[] headers) {
        final Map<String, DataStoreColumnType> result = Maps.newHashMap();
        for (String header : headers) {
            result.put(header, DataStoreColumnType.TEXT);
        }
        return result;
    }

    private RowDataDTO entityToRawData(Entity entity, String[] headers) {
        final Map<String, String> headerValueMap = GSON.fromJson(GSON.toJson(entity), TYPE_MAP_STRING_STRING);
        final Map<String, String> underscoredHeaderValueMap = new HashMap<>(headerValueMap.size());
        for (String headerName : headerValueMap.keySet()) {
            underscoredHeaderValueMap.put(underscoreHeader(headerName), headerValueMap.get(headerName));
        }
        final String[] values = new String[headers.length];
        for (int i = 0; i < headers.length; i++) {
            String underscoredHeader = underscoreHeader(headers[i]);
            final String value = underscoredHeaderValueMap.get(underscoredHeader);
            values[i] = value != null ? value : StringUtils.EMPTY;
            headers[i] = underscoredHeader;
        }
        final RowDataDTO rowDataDTO = new RowDataDTO();
        rowDataDTO.setHeaders(headers);
        rowDataDTO.setValues(values);
        return rowDataDTO;
    }

    private URI buildUri(String rootUrl, String path) throws URISyntaxException {
        final URIBuilder builder = new URIBuilder(rootUrl);
        builder.setPath(path);
        return builder.build();
    }

    private <T> void logErrors(RestResponse<T> restResponse) {
        final List<Error> errors = restResponse.getErrors();
        for (Error error : errors) {
            logger.error(error.getMessage());
        }
    }

    private URI buildRequestUrl(Credentials credentials, String additionalPath)
            throws URISyntaxException {
        final StringBuilder dataStoreRequestUrl = new StringBuilder(workfusionUrl).append(DATASTORE_SERVICE_URL).append("/");
        if (StringUtils.isNotBlank(additionalPath)) {
            dataStoreRequestUrl.append(additionalPath);
        }
        return buildUri(credentials.getHost(), dataStoreRequestUrl.toString());
    }

    private <T> HttpEntity<T> createHttpEntity(T entity, MediaType contentType, MediaType acceptType) {
        final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(contentType);
        headers.setAccept(Collections.singletonList(acceptType));
        return new HttpEntity<>(entity, headers);
    }

}
