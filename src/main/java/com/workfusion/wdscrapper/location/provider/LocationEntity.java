package com.workfusion.wdscrapper.location.provider;

/**
 * Created by amorozov
 * Date: 11/23/2015
 */
public class LocationEntity {

    private String zipCode;
    private String country;
    private String state;
    private String city;

    public LocationEntity(String zipCode, String country, String state, String city) {
        this.zipCode = zipCode;
        this.country = country;
        this.state = state;
        this.city = city;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (!(o instanceof LocationEntity))
            return false;

        LocationEntity that = (LocationEntity) o;

        return zipCode.equals(that.zipCode);

    }

    @Override
    public int hashCode() {
        return zipCode.hashCode();
    }
}
