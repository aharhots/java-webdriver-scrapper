package com.workfusion.wdscrapper.utils;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import com.workfusion.wdscrapper.Conf;
import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class WebDriverHelper {
    public static void setDriver() {
        System.out.println("OS: " + System.getProperty("os.name"));
        if (System.getProperty("os.name").contains("OS X"))
            System.setProperty("webdriver.chrome.driver", "chromedriver_mac");
        else if (System.getProperty("os.name").contains("Linux"))
            System.setProperty("webdriver.chrome.driver", "chromedriver");
        else
            System.setProperty("webdriver.chrome.driver", "chromedriver.exe");
    }

    public static void setupDriver(WebDriver webdriver) {
        webdriver.get("about:blank");
        webdriver.manage().timeouts().pageLoadTimeout(Conf.PAGE_LOAD_TIMEOUT, TimeUnit.SECONDS);
        webdriver.manage().timeouts().implicitlyWait(Conf.TIMEOUT, TimeUnit.SECONDS);
        webdriver.manage().timeouts().setScriptTimeout(Conf.SCRIPT_TIMEOUT, TimeUnit.SECONDS);
    }

    /**
     * Maximizes browser. Note: the common webdriver.manage().window().maximize() doesn't work under Linux with XVFB.
     * webDriver    WebDriver instance.
     */
    public static void maximize(WebDriver webDriver) {
        java.awt.Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        webDriver.manage().window().setPosition(new org.openqa.selenium.Point(0, 0));
        webDriver.manage().window().setSize(new org.openqa.selenium.Dimension(screenSize.width, screenSize.height - 40));
    }

    /**
     * Returns list of element texts.
     *
     * @param driver    WebDriver instance.
     * @param locator   Element locator.
     * @param timeout   Timeout in seconds.
     * @return          List of texts.
     */
    public static List<String> getElementTexts(WebDriver driver, By locator, int timeout) {
        List<String> texts;
        long startTime = CommonUtils.getSeconds();
        do {
            texts = doGetElementTexts(driver, locator);
        } while (texts.size() < 1 && CommonUtils.getSeconds() - startTime < timeout);
        return texts;
    }

    private static List<String> doGetElementTexts(WebDriver driver, By locator) {
        List<String> texts;
        driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
        try {
            List<WebElement> elements = driver.findElements(locator);
            texts = elements.size() > 0 ?
                    elements.stream().map(element -> element.getText()).collect(Collectors.toList())
                    : new ArrayList<>();
        }
        catch (StaleElementReferenceException e) {
            texts = new ArrayList<>();
        }
        driver.manage().timeouts().implicitlyWait(Conf.TIMEOUT, TimeUnit.SECONDS);
        return texts;
    }

    public static void setText(WebDriver driver, By locator, String text, int timeout) {
        long startTime = CommonUtils.getSeconds();
        while (!doSetText(driver, locator, text) && CommonUtils.getSeconds() - startTime < timeout);
    }

    private static boolean doSetText(WebDriver driver, By locator, String text) {
        driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
        boolean isSet;
        try {
            CommonUtils.jse(driver).executeScript("arguments[0].setAttribute('value', '" + text + "')", driver.findElement(locator));
            isSet = true;
        } catch (StaleElementReferenceException e) {
            isSet = false;
        }
        driver.manage().timeouts().implicitlyWait(Conf.TIMEOUT, TimeUnit.SECONDS);
        return isSet;
    }

    /**
     * Finds elements and returns list of values attribute "href".
     *
     * @param driver    WebDriver instance.
     * @param locator   Element locator.
     * @param timeout   Timeout in seconds.
     * @return List of URLs.
     */
    public static List<String> getUrls(WebDriver driver, By locator, int timeout) {
        driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
        long startTime = CommonUtils.getSeconds();
        List<String> urls;
        do {
            urls = getUrls(driver, locator);
            CommonUtils.sleep(1);
        } while ((urls.size() < 1 || (urls.size() > 0 && urls.size() != getUrls(driver, locator).size())) && CommonUtils.getSeconds() - startTime < timeout);
        driver.manage().timeouts().implicitlyWait(Conf.TIMEOUT, TimeUnit.SECONDS);
        return urls;
    }

    /**
     * Finds elements and returns list of values attribute "href".
     *
     * @param driver    WebDriver instance.
     * @param locator   Element locator.
     * @return          List of URLs.
     */
    private static List<String> getUrls(WebDriver driver, By locator) {
        return getAttributes(driver, locator, "href");
    }

    public static List<String> getAttributes(WebDriver driver, By locator, String attrName) {
        try {
            return driver.findElements(locator).stream()
                    .map(element -> element.getAttribute(attrName))
                    .filter(StringUtils::isNotBlank)
                    .collect(Collectors.toList());
        } catch (StaleElementReferenceException e) {
            return new ArrayList<>();
        }
    }

    /**
     * Takes a screenshot.
     *
     * @param driver   WebDriver instance.
     * @param filePath File path.
     */
    public static void takeScreenshot(WebDriver driver, String filePath) {
        File screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
        try {
            org.apache.commons.io.FileUtils.copyFile(screenshot, new File(filePath));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Verifies if an element is present.
     *
     * @param driver        WebDriver instance.
     * @param locator       Element locator.
     * @param timeout       Timeout.
     * @return              true if element present, otherwise false.
     */
    public static boolean isPresent(WebDriver driver, By locator, int timeout) {
        driver.manage().timeouts().implicitlyWait(timeout, TimeUnit.SECONDS);
        boolean isPresent = !driver.findElements(locator).isEmpty();
        driver.manage().timeouts().implicitlyWait(Conf.TIMEOUT, TimeUnit.SECONDS);
        return  isPresent;
    }

    /**
     * Verifies if an element invisible.
     *
     * @param driver        WebDriver instance.
     * @param locator       Element locator.
     * @param timeout       Timeout.
     * @return              true if element invisible, otherwise false.
     */
    public static boolean isInvisible(WebDriver driver, By locator, int timeout) {
        return checkVisibility(driver, locator, timeout, false);
    }

    /**
     * Verifies if an element visible.
     *
     * @param driver        WebDriver instance.
     * @param locator       Element locator.
     * @param timeout       Timeout.
     * @return              true if element visible, otherwise false.
     */
    public static boolean isVisible(WebDriver driver, By locator, int timeout) {
        return checkVisibility(driver, locator, timeout, true);
    }

    /**
     * Verifies if element visible.
     *
     * @param locator           element to check.
     * @param timeout           timeout in seconds to find an element.
     * @param shouldBeVisible   expected state true or false.
     * @return                  returns true if element visibility matches expected state, otherwise false.
     */
    private static boolean checkVisibility(WebDriver driver, By locator, int timeout, boolean shouldBeVisible) {
        boolean isVisibilityMatch = false;
        try {
            driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
            long startTime = CommonUtils.getSeconds();
            do {
                int visibleElementsCount = 0;
                for (WebElement element : driver.findElements(locator))
                    if (element.isDisplayed())
                        visibleElementsCount++;
                if (visibleElementsCount > 0 == shouldBeVisible) {
                    isVisibilityMatch = true;
                    break;
                }
            } while (CommonUtils.getSeconds() - startTime < timeout);
            driver.manage().timeouts().implicitlyWait(Conf.TIMEOUT, TimeUnit.SECONDS);
        }
        catch (StaleElementReferenceException e) {
            checkVisibility(driver, locator, timeout, shouldBeVisible);
        }
        return isVisibilityMatch;
    }

    public static void makeElementVisible(WebDriver driver, By locator) {
        CommonUtils.jse(driver).executeScript("arguments[0].setAttribute('style', 'display: block;')", driver.findElement(locator));
    }

    public static void makeElementInvisible(WebDriver driver, By locator) {
        CommonUtils.jse(driver).executeScript("arguments[0].setAttribute('style', 'display: none;')", driver.findElement(locator));
    }

    /**
     * Clicks on element.
     *  @param driver   WebDriver instance
     * @param locator   locator.
     * @param timeout   Timeout for wait element visible.
     */
    public static void click(WebDriver driver, By locator, int timeout) {
        long startTime = CommonUtils.getSeconds();
        while (!doClick(driver, locator) && CommonUtils.getSeconds() - startTime <= timeout);
    }

    private static boolean doClick(WebDriver driver, By locator) {
        driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
        boolean isClicked;
        try {
            CommonUtils.jse(driver).executeScript("arguments[0].click()", driver.findElement(locator));
            isClicked = true;
        }
        catch (StaleElementReferenceException e) {
            isClicked = false;
        }
        driver.manage().timeouts().implicitlyWait(Conf.TIMEOUT, TimeUnit.SECONDS);
        return isClicked;
    }

    /**
     * Waits until an element become invisible.
     * This method throws an error if an element left visible within timeout.
     *
     * @param driver    Webdriver instance.
     * @param locator   Element locator By.
     * @param timeout   Timeout in seconds.
     */
    public static void waitInvisible(WebDriver driver, By locator, int timeout) {
        WebDriverWait wait = new WebDriverWait(driver, timeout);
        wait.until(ExpectedConditions.invisibilityOfElementLocated(locator));
    }

    /**
     * Waits until an element become visible.
     *
     * @param driver    Webdriver instance.
     * @param locator   Element locator By.
     * @param timeout   Timeout in seconds.
     */
    public static void waitVisible(WebDriver driver, By locator, int timeout) {
        try {
            WebDriverWait wait = new WebDriverWait(driver, timeout);
            wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
        } catch (Exception e) {
        }
    }

    public static List<WebElement> getElements(WebDriver driver, By locator, int timeout) {
        driver.manage().timeouts().implicitlyWait(timeout, TimeUnit.SECONDS);
        List<WebElement> elements = driver.findElements(locator);
        driver.manage().timeouts().implicitlyWait(Conf.TIMEOUT, TimeUnit.SECONDS);
        return elements;
    }

    public static void closeAlertIfPresent(WebDriver driver) {
        try {
            driver.switchTo().alert().accept();
        } catch (NoAlertPresentException e) {
        }
    }

    public static String getAlertMessage(WebDriver driver) {
        try {
            Alert alert = driver.switchTo().alert();
            String alertText = alert.getText();
            alert.accept();
            return alertText;
        } catch (NoAlertPresentException e) {
            return StringUtils.EMPTY;
        }
    }


}
