package com.workfusion.wdscrapper.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;

public class CommonUtils {

    public static long getSeconds() {
        return System.currentTimeMillis() / 1000;
    }

    /**
     * Returns postfix of URL after last slash '/'
     *
     * @param url URL
     * @return postfix.
     */
    public static String getUrlPostfix(String url) {
        String[] splitUrl = url.split("/");
        return splitUrl[splitUrl.length - 1];
    }

    public static <T> T nvl(T obj, T defaultValue) {
        return obj != null ? obj : defaultValue;
    }

    public static void sleep(int seconds) {
        sleepMillis(seconds * 1000);
    }

    public static void sleepMillis(long millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static JavascriptExecutor jse(WebDriver driver) {
        return (JavascriptExecutor) driver;
    }

    public static String insertText(String text, String textToInsert) {
        return text.replace("</head>", "</head>\n<website_url>" + textToInsert + "</website_url>");
    }

    public static String getTextByRegex(String text, String regularExpression) {
        if (text == null) return null;
        Pattern pattern = Pattern.compile(regularExpression);
        Matcher matcher = pattern.matcher(text);
        return matcher.find() ? matcher.group(0) : null;
    }
}
