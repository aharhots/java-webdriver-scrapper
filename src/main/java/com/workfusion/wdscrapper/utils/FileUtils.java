package com.workfusion.wdscrapper.utils;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import static java.io.File.separator;

public class FileUtils {

    public final static String PARENT_FOLDER = System.getProperty("user.home") + separator + "Locations" + separator;
    public final static String EXTENSION = ".html";

    /**
     * Appends text to file.
     *
     * @param filePath File path.
     * @param text     Content to add.
     */
    private static void addToFile(String filePath, String text) {
        File file = new File(filePath);
        if (!file.exists())
            file.getParentFile().mkdirs();
        PrintWriter writer = null;
        try {
            writer = new PrintWriter(new BufferedWriter(new FileWriter(filePath, true)));
        } catch (IOException e) {
            e.printStackTrace();
        }
        writer.println(text);
        writer.close();
    }

    /**
     * Appends text to file.
     *
     * @param classInstance Class instance to create folder name.
     * @param fileName      Just file name without path and extension.
     * @param text          Text to add.
     * @param url           Page URL.
     */
    public static void addToFile(Class classInstance, String fileName, String text, String url) {
        String siteFolder = classInstance.getSimpleName();
        String filePath = PARENT_FOLDER + siteFolder + separator + fileName + EXTENSION;
        text = CommonUtils.insertText(text, url);
        addToFile(filePath, text);
    }

    /**
     * Verifies if a file exists
     *
     * @param classInstance Class instance to create folder name.
     * @param fileName      Just file name without path and extension.
     * @return true if file exists, otherwise false.
     */
    public static boolean isFileExist(Class classInstance, String fileName) {
        String[] splitClassName = classInstance.getName().split("\\.");
        String siteFolder = splitClassName[splitClassName.length - 1];
        return new File(PARENT_FOLDER + siteFolder, fileName + EXTENSION).exists();
    }

    public static List<String> getLines(String filePath) {
        try {
            return Files.readAllLines(Paths.get(filePath), StandardCharsets.UTF_8);
        } catch (IOException e) {
            e.printStackTrace();
            return new ArrayList<>();
        }
    }
}
