package com.workfusion.wdscrapper.s3;

/**
 * Class that contain data that is result of executing s3 plugin
 *
 * @author Yesipov Kirill
 */
public class S3ResultItem {

    private String directUrl;
    private String signedUrl;
    private String filename;

    /**
     * Accessor method
     *
     * @return String
     */
    public String getDirectUrl() {
        return directUrl;
    }

    /**
     * Accessor method
     *
     * @param directUrl field
     */
    public void setDirectUrl(String directUrl) {
        this.directUrl = directUrl;
    }

    /**
     * Accessor method
     *
     * @return String
     */
    public String getSignedUrl() {
        return signedUrl;
    }

    /**
     * Accessor method
     *
     * @param signedUrl field
     */
    public void setSignedUrl(String signedUrl) {
        this.signedUrl = signedUrl;
    }

    /**
     * Accessor method
     *
     * @return String
     */
    public String getFilename() {
        return filename;
    }

    /**
     * Accessor method
     *
     * @param filename field
     */
    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String toString() {
        return directUrl;
    }
}
