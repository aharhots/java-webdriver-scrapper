package com.workfusion.wdscrapper;

import java.net.MalformedURLException;
import java.net.URL;

import org.apache.commons.lang3.StringUtils;

import static java.lang.System.getProperty;

/**
 * @author Dmitry Borovko
 */
public class Conf {

    public static final String PROP_IG_COUNTRIES = "ig.countries";
    public static final String PROP_IG_STATES = "ig.states";
    public static final String PROP_MAX_CONCURRENT_TESTS = "max-concurrent-tests";
    public static final String PROP_SELENIUM_HUB_URL = "selenium-hub-url";
    public static final int TIMEOUT = 30;
    public static final int SCRIPT_TIMEOUT = 40;
    public static final int PAGE_LOAD_TIMEOUT = 180;
    private static final String PROP_DATASTORE_NAME = "datastore-name";
    private static final String DEFAULT_DATASTORE_NAME = "locations_temp_1";

    public static final String DEFAULT_COUNTRY = "US";
    public static final String DEFAULT_STATE = "CA";
    public static final String COMA_CHAR = ",";

    public static final URL SELENUIM_HUB_URL;

    static {
        URL url = null;
        String urlString = getProperty(PROP_SELENIUM_HUB_URL);
        if (StringUtils.isNotBlank(urlString)) {
            try {
                url = new URL(urlString);
            } catch (MalformedURLException e) {
                throw new RuntimeException("Selenium HUB URL should be provided correctly.", e);
            }
        }
        SELENUIM_HUB_URL = url;
    }


    public static final int MAX_CONCURRENT_TESTS = Integer.parseInt(getProperty(PROP_MAX_CONCURRENT_TESTS, "1"));

    public static final int MAX_TRY = 3;

    public static String getDatastoreName() {
        return getProperty(PROP_DATASTORE_NAME, DEFAULT_DATASTORE_NAME);
    }
}
