#      |Website	       |Name	      |Type   |USA/USA+Canada
------:|---------------|--------------|-------|-----				
1      |99only.com     |99 ONLY	|Others	|U
2      |autozone.com   |AUTOZONE	|Others	|U
3      |loewshotels.com|LOEW'S HOTELS & RESORTS	|Others	|B
4      |pearlevision.com| PEARLE VISION	|Others	|B
5      |pier1.com	|PIER 1 IMPORTS	|Others	|B
6      |pizzahut.com	|PIZZA HUT	|Others	|B
7      |tacobell.com	|TACO BELL	|Others	|B
8      |zomato.com	|ZOMATO (fka UrbanSpoon)	|Others	|U				
9      |webmd.com	|Web MD	|Medical	|U
10     |healthgrades.com	|Health Grades	|Medical	|U
11     |umc.org	|UMC (United Methodist)	|Churches	|U
12     |advanceamerica.net	|ADVANCE AMERICA	|Others	|U
13     |applebees.com	|APPLEBEE'S	|Others	|B
14     |littlecaesars.com	|LITTLE CAESARS PIZZA	|Others	|B
15     |pandaexpress.com	|PANDA EXPRESS	|Others	 |B